#!/usr/bin/env python

# plot_countries - plot some SARS-CoV-2 daily count curves
# (C) 2020 Boud Roukema GPL-3+
#
# This script is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This script is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.

import sys
import os
import warnings

import numpy as np
from numpy.random import SeedSequence, default_rng
import matplotlib
import matplotlib.pyplot as plt

def generate_Noft_positive(N):

    debug = True

    seeds = SeedSequence(2039)
    seed = seeds.spawn(2) # one seed is ignored
    #rng = default_rng(seed[0])
    rng = default_rng()

    # Use a concave-down parabola that is positive on (0 < x < N) as a simple
    # test case.

    # First generate the smooth curve with only rounding noise:
    x = np.arange(N)
    Noft_smooth = N*N/2 - 0.5*(x - N/2)**2
    if(debug):
        print('Noft_smooth = ',Noft_smooth)

    Noft_noisy = rng.poisson(Noft_smooth)
    Noft_noisy = np.array(Noft_noisy, dtype=int) # convert to int
    if(debug):
        print('Noft_noisy = ',Noft_noisy)

    return Noft_noisy


def plot_countries(country,Noft_list,date_str,
                   phi_i,plot_label, join_plots=False):

    debug = True

    n = os.environ.get('N_MEDIAN_MODEL')
    if(n):
        N_median_model = np.int(n)
    else:
        N_median_model = 4

    ## main colour: data points + model
    colours = np.array(['k','r','g','b']) # allow this many colours
    ## shading for 1 sigma Poisson error
    colour_err = np.array(['#aaaaaa',
                           '#ffaaaa',
                           '#aaffaa',
                           '#aaaaff']) # allow this many colours
    ## shading for 1 sigma phi_i model error
    colour_phi = np.array(['#cccccc',
                           '#ffcccc',
                           '#ccffcc',
                           '#ccccff']) # allow this many colours
    ## If there is no good reason for colour, then stick to black and white:
    N_countries = country.size
    colours = np.array(['k'] * N_countries)
    colour_err = np.array(['#aaaaaa'] * N_countries)
    colour_phi = np.array(['#cccccc'] * N_countries)

    if(N_countries > colours.size):
        raise RuntimeError('plot_countries_mod.py: plot_countries: N_countries = ',
                           N_countries,' needs more colours.')

    sublabels = np.array(['A', 'B','C','D','E','F'])
    if(join_plots):
        # For joint plots, initialise specific subplotting options.
        plt.clf()
        plt.rcParams.update({'font.size': 8,
                             'font.family': 'serif'})
        if(N_countries == 6):
            fig, axis_2Dtuple = plt.subplots(3,2,figsize=(6.4,8.4))
            axis_tuple = (axis_2Dtuple[0][0], axis_2Dtuple[0][1],
                          axis_2Dtuple[1][0], axis_2Dtuple[1][1],
                          axis_2Dtuple[2][0], axis_2Dtuple[2][1])
        elif(N_countries == 4):
            fig, axis_2Dtuple = plt.subplots(2,2,figsize=(6.4,5.6))
            axis_tuple = (axis_2Dtuple[0][0], axis_2Dtuple[0][1],
                          axis_2Dtuple[1][0], axis_2Dtuple[1][1])
        else:
            raise RuntimeError('plot_countries_mod: N_countries = ',N_countries,' not programmed.')

        print('axis_2Dtuple=',axis_2Dtuple)

        file_label = ''
    else:
        axis_tuple = country # dummy

    for c_tuple in zip(country,Noft_list,phi_i,date_str,
                       colours,colour_err,colour_phi,
                       axis_tuple, sublabels):
        print('type(c_tuple[7])=',type(c_tuple[7]))
        print('c_tuple[7]=',c_tuple[7])
        ax = c_tuple[7] # axis object
        if(not(join_plots)):
            plt.clf()
            plt.rcParams.update({'font.size': 18,
                                 'font.family': 'serif'})
            fig, ax = plt.subplots(1,1)

        Noft = c_tuple[1]
        N_Noft = Noft.size # assumes numpy array
        if(N_Noft < 5):
            warnings.warn('Country '+c_tuple[0]+' has too few ('+N_Noft+') values to be plotted.')

        if(4 == N_median_model):
            Noft_neighbours = np.array([Noft[0:N_Noft-4],
                                        Noft[1:N_Noft-3],
                                        Noft[3:N_Noft-1],
                                        Noft[4:N_Noft]
            ])
        else:
            raise RuntimeError('plot_countries_mod.py: plot_countries: N_median_model = ',
                               N_median_model,' is not programmed.')
        if(debug):
            print(Noft_neighbours)
        # simple model of the middle value as the median of the neighbours
        Noft_model_middle = np.median(Noft_neighbours, axis=0)
        if(debug):
            print('Noft_model_middle = \n',Noft_model_middle)
        Noft_model = np.zeros(N_Noft)
        ## Copy the middle model into the middle of Noft_model
        Noft_model[2:N_Noft-2] = Noft_model_middle
        ## Pad the ends with constants
        Noft_model[0:2] = Noft_model[2]
        Noft_model[N_Noft-2:N_Noft] = Noft_model[N_Noft-3]

        # ordinary Poissonian error bars
        Noft_plus = Noft_model + np.sqrt(Noft_model)
        Noft_minus = Noft_model - np.sqrt(Noft_model)
        times = np.arange(N_Noft)

        # phi error bars
        Noft_plus_phi_i = Noft_model + np.sqrt(c_tuple[2])* np.sqrt(Noft_model)
        Noft_minus_phi_i = Noft_model -  np.sqrt(c_tuple[2])* np.sqrt(Noft_model)


        # labels
        country_label = (c_tuple[0]+' '+c_tuple[3])
        model_label = (c_tuple[0]+' model')

        # plot phi_i limits if phi_i > 1, in faint shading
        print('plot_countries: country, c_tuple[2] == phi_i = ',c_tuple[0],c_tuple[2])
        if(c_tuple[2] > 1.0):
            ax.fill_between(times, Noft_minus_phi_i, Noft_plus_phi_i,
                             edgecolor=c_tuple[4],
                             facecolor=c_tuple[6],
                             label='$\phi_i$')

        # plot 1 sigma Poisson limits, in brighter shading
        ax.fill_between(times, Noft_minus, Noft_plus,
                         edgecolor=c_tuple[4],
                         facecolor=c_tuple[5],
                         label='Poisson')

        # plot observational data
        if(not(join_plots)):
            ax.plot(times, Noft,
                    'o', linewidth=2,
                    c=c_tuple[4],
                    label=country_label)
        else:
            ax.plot(times, Noft,
                    'o', linewidth=2,markersize=2.5,
                    c=c_tuple[4],
                    label=country_label)

        # plot median-from-neighbours model
        ax.plot(times, Noft_model,
                 '-', linewidth=2,
                 c=c_tuple[4],
                 label=model_label)


        # calculate reasonable y limits
        ymax = np.amax([np.amax(Noft),np.amax(Noft_model)])
        ymax += np.amax([4.0*np.sqrt(ymax), 2.0*np.sqrt(c_tuple[2])*np.sqrt(ymax)])
        ymin = np.amin([np.amin(Noft),np.amin(Noft_model)])
        ymin -= np.amax([4.0*np.sqrt(ymin), 2.0*np.sqrt(c_tuple[2])*np.sqrt(ymin)]) # 3.0*np.sqrt(ymin)
        ax.set_ylim(ymin, ymax)

        if(join_plots):
            (xmin,xmax) = ax.get_xlim()
            x_text = xmin+0.9*(xmax-xmin)
            y_text = ymin+0.05*(ymax-ymin)
            ax.text(x_text,y_text,c_tuple[8],fontsize=8)

        if(not(join_plots)):
            ax.legend(prop={'size': 12})
        else:
            ax.legend(prop={'size': 6})
        #plt.yscale('log')
        #plt.ticklabel_format(style='sci', axis='y')
        ax.set_xlabel(r'$j$-th day')
        ax.set_ylabel(r'$n_{i}(\,j )$')


        # inside loop - close plot if the plots are not to be joined
        if(not(join_plots)):
            fig.tight_layout()
            fig.savefig('countries_daily_'+c_tuple[0]+plot_label+'.eps')
            plt.close()
        else:
            file_label += c_tuple[0] + '_'

    # outside loop - close plot
    if(join_plots):
        fig.tight_layout()
        #fig.savefig('countries_daily_'+file_label + plot_label+'.eps')
        fig.savefig('countries_daily_' + plot_label + '.eps')
        plt.close()


if __name__ == '__main__':

    pass_val = 0 # test parameter 0 = pass, !0 = fail

    N = 28 #50

    Noft0 = generate_Noft_positive(N)
    Noft1 = 1.5 * generate_Noft_positive(N)
    Noft2 = 2 * generate_Noft_positive(N)
    Noft3 = 2.5 * generate_Noft_positive(N)
    Noft4 = 10 * generate_Noft_positive(N)
    Noft5 = 100 * generate_Noft_positive(N)
    countries = np.array(['AA','BB','CC','DD','EE','FF'])
    Noft = np.array([Noft0, Noft1, Noft2, Noft3, Noft4, Noft5])
    date_str = np.array(['2020-04-55','2020-07-99',
                         '2020-06-55','2020-09-99',
                         '2020-08-55','2020-11-99'])
    phi_i = np.array([1.0,1.5,2,2.5,10.0,100.0])
    plot_label = '_dummy'

    plot_countries(countries[0:4],Noft,date_str,phi_i,plot_label,
                   join_plots=True)

    print('pass_val = ',pass_val)
    sys.exit(pass_val)

#end __main__
