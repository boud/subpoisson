#!/usr/bin/env python3

# (C) 2021 Boud Roukema GPL-3+
#
# This script is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This script is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.

import sys
import numpy as np
from scipy import stats, fft

def generate_Noft_weekly(N,dip_fraction1,dip_fraction2):

    # simulate a simple noiseless counts curve
    x = np.arange(N)
    Noft_smooth = N*N/2 - 0.5*(x - N/2)**2

    # add noise
    Noft_smooth = 1.0 + np.random.poisson(Noft_smooth)

    # indices for weekends
    i_weekends_0 = np.arange(0,N,7) # first day of weekend
    i_weekends_1 = np.arange(1,N,7) # second day of weekend

    Noft_smooth[i_weekends_0] *= dip_fraction1 # e.g. fraction = 0.9
    Noft_smooth[i_weekends_1] *= dip_fraction2 # e.g. fraction = 0.8

    return Noft_smooth


def detect_weekly_dip(Noft):

    # pad to multiple of 7
    N = np.array(Noft).size
    n_padding = np.mod(N,7)
    if(n_padding > 0):
        Noft_local = np.append(np.array(Noft,dtype=np.float), Noft[N-1]*np.ones(n_padding))
    else:
        Noft_local = np.array(Noft,dtype=np.float)

    # Make the forward fourier transform; don't worry about normalisation.
    f = fft.fft(Noft_local)

    # mean around expected dip
    med = (f.real[6] + f.real[8])/2.0
    
    norm = f.real[0]/np.pi
    # subtract the median, normalise, and add 1 to convert to a fraction
    dip = 1.0 + (f.real[7] - med) /norm

    return dip



if __name__ == '__main__':

    frac1 = np.linspace(0.95,0.5,num=10)
    dip = np.array([])
    for f1 in frac1:
        Noft = generate_Noft_weekly(50, f1, f1*f1)
        d = detect_weekly_dip(Noft)
        dip = np.append(dip, d)
        print(f1, d)

    rho, p_spearman = stats.spearmanr(frac1,dip)
    print('spearman: rho = ',rho,' p=',p_spearman) # non-parametric
    r, p_pearson = stats.pearsonr(frac1,dip)
    print('pearson: r = ',r,' p=',p_pearson) # parametric

    if(rho > 0.99999999 and p_spearman < 1e-30 and
       r > 0.99 and p_pearson < 1e-7):
        pass_val = 0
    else:
        pass_val = 1

    print('pass_val = ',pass_val)
    sys.exit(pass_val)

#end __main__
