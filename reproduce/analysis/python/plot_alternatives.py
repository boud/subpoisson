#!/usr/bin/env python

# plot_countries - plot some SARS-CoV-2 daily count curves
# (C) 2020 Boud Roukema GPL-3+
#
# This script is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This script is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.

import sys
import os
import warnings

import numpy as np
from numpy.random import SeedSequence, default_rng
import matplotlib
import matplotlib.pyplot as plt


# The 'ax' axis must be created before calling this routine, and the
# figure object must be finished after the routine is called.
def do_alternative_plot(country_total_count,
                        country_cluster,
                        country_cluster_stderrlog10,
                        country_cluster_alt,
                        country_cluster_alt_stderrlog10,
                        alt_y_label,
                        alt_y_axis_label,
                        fig,
                        ax,
                        sublabel=''):

                        
    ## === psi_i vs alternative psi_i relations ====

    # log10 error bars
    country_cluster_psi = country_cluster / np.sqrt(country_total_count)
    c_cluster_plus = 10.0**(np.log10(country_cluster_psi) + country_cluster_stderrlog10) - country_cluster_psi
    c_cluster_minus = country_cluster_psi - 10.0**(np.log10(country_cluster_psi) - country_cluster_stderrlog10)

    country_cluster_alt_psi = country_cluster_alt / np.sqrt(country_total_count)    
    c_cluster_alt_plus = 10.0**(np.log10(country_cluster_alt_psi) + country_cluster_alt_stderrlog10) - country_cluster_alt_psi
    c_cluster_alt_minus = country_cluster_alt_psi - 10.0**(np.log10(country_cluster_alt_psi) - country_cluster_alt_stderrlog10)
    
    ax.errorbar(country_cluster_psi,
                 country_cluster_alt_psi,
                 xerr=[c_cluster_minus,c_cluster_plus],
                 yerr=[c_cluster_alt_minus,c_cluster_alt_plus],
                 fmt=',k', markersize=16,
                 elinewidth=0.5, capsize=1.5, capthick=0.5)
    #xlim1=3.e-3
    xlim1=1.e-3
    xlim2=3.0e0
    ylim1=xlim1
    ylim2=xlim2

    ax.plot([xlim1, xlim2], [ylim1, ylim2], '-k',linewidth=0.5) # equality line
    ax.set_xscale('log')
    ax.set_yscale('log')
    ax.text(2e-3,1.0,sublabel)

    ax.axis('square')
    ax.set_xlim(xlim1,xlim2)
    ax.set_ylim(ylim1,ylim2)


    #No effect for log: https://matplotlib.org/3.2.2/api/ticker_api.html#matplotlib.ticker.LogFormatterSciNotation
    #ax.ticklabel_format(style='sci', axis='x', scilimits=(-1,4))
    #ax.ticklabel_format(style='plain', axis='y')
    ax.set_xlabel(r'$\psi_i$')
    ax.set_ylabel(alt_y_axis_label)

    #    fig.tight_layout()
    #    fig.savefig('psi_N_'+alt_y_label+'.eps')
    #    fig.close()

    return


if __name__ == '__main__':

    pass_val = 0 # test parameter 0 = pass, !0 = fail

    phi_N_file = "phi_N_full.dat"


    alt_y_label = "log_median"
    alt_y_axis_label = "$\psi_i^{\mathrm{log\,median}}$"
    (country_total_count,
     country_cluster,
     country_cluster_stderrlog10,
     country_cluster_alt,
     country_cluster_alt_stderrlog10) = np.loadtxt(phi_N_file,
                                                   usecols=(1, 6,7, 9,10),
                                                   dtype=np.float,
                                                   unpack=True)

    plt.rcParams.update({'font.size': 18,
                         'font.family': 'serif'})
    
    fig, ax = plt.subplots(1,1)
    do_alternative_plot(country_total_count,
                        country_cluster,
                        country_cluster_stderrlog10,
                        country_cluster_alt,
                        country_cluster_alt_stderrlog10,
                        alt_y_label,
                        alt_y_axis_label,
                        fig,ax)
    fig.tight_layout()
    fig.savefig('psi_N_'+alt_y_label+'.eps')
    plt.close()



    alt_y_label = "neg_binomial"
    alt_y_axis_label = "$\psi_i^{\mathrm{neg.\,binomial}}$"
    (country_total_count,
     country_cluster,
     country_cluster_stderrlog10,
     country_cluster_alt,
     country_cluster_alt_stderrlog10) = np.loadtxt(phi_N_file,
                                                   usecols=(1, 6,7, 12,13),
                                                   dtype=np.float,
                                                   unpack=True)

    fig, ax = plt.subplots(1,1)
    do_alternative_plot(country_total_count,
                        country_cluster,
                        country_cluster_stderrlog10,
                        country_cluster_alt,
                        country_cluster_alt_stderrlog10,
                        alt_y_label,
                        alt_y_axis_label,
                        fig,ax)
    fig.tight_layout()
    fig.savefig('psi_N_'+alt_y_label+'.eps')
    plt.close()


    alt_y_label = "bidays"
    alt_y_axis_label = "$\psi_i^{\mathrm{2\,day}}$"
    (country_total_count,
     country_cluster,
     country_cluster_stderrlog10,
     country_cluster_alt,
     country_cluster_alt_stderrlog10) = np.loadtxt(phi_N_file,
                                                   usecols=(1, 6,7, 17,18),
                                                   dtype=np.float,
                                                   unpack=True)
    fig, ax = plt.subplots(1,1)
    do_alternative_plot(country_total_count,
                        country_cluster,
                        country_cluster_stderrlog10,
                        country_cluster_alt,
                        country_cluster_alt_stderrlog10,
                        alt_y_label,
                        alt_y_axis_label,
                        fig,ax)
    fig.tight_layout()
    fig.savefig('psi_N_'+alt_y_label+'.eps')
    plt.close()



    alt_y_label = "tridays"
    alt_y_axis_label = "$\psi_i^{\mathrm{3\,day}}$"
    (country_total_count,
     country_cluster,
     country_cluster_stderrlog10,
     country_cluster_alt,
     country_cluster_alt_stderrlog10) = np.loadtxt(phi_N_file,
                                                   usecols=(1, 6,7, 20,21),
                                                   dtype=np.float,
                                                   unpack=True)
    
    fig, ax = plt.subplots(1,1)
    do_alternative_plot(country_total_count,
                        country_cluster,
                        country_cluster_stderrlog10,
                        country_cluster_alt,
                        country_cluster_alt_stderrlog10,
                        alt_y_label,
                        alt_y_axis_label,
                        fig,ax)
    fig.tight_layout()
    fig.savefig('psi_N_'+alt_y_label+'.eps')
    plt.close()


    ########################################################################
    ### alternative version with plots made as subplots ####################
    ########################################################################

    plt.rcParams.update({'font.size': 12,
                         'font.family': 'serif'})
    alt_y_axis_label = "$\psi_i^{\mathrm{log\,median}}$"

    (country_total_count,
     country_cluster,
     country_cluster_stderrlog10,
     country_cluster_alt,
     country_cluster_alt_stderrlog10) = np.loadtxt(phi_N_file,
                                                   usecols=(1, 6,7, 9,10),
                                                   dtype=np.float,
                                                   unpack=True)

    fig, (ax1, ax2) = plt.subplots(1,2)
    do_alternative_plot(country_total_count,
                        country_cluster,
                        country_cluster_stderrlog10,
                        country_cluster_alt,
                        country_cluster_alt_stderrlog10,
                        alt_y_label,
                        alt_y_axis_label,
                        fig,ax1,'A')


    alt_y_axis_label = "$\psi_i^{\mathrm{neg.\,binomial}}$"
    (country_total_count,
     country_cluster,
     country_cluster_stderrlog10,
     country_cluster_alt,
     country_cluster_alt_stderrlog10) = np.loadtxt(phi_N_file,
                                                   usecols=(1, 6,7, 12,13),
                                                   dtype=np.float,
                                                   unpack=True)

    do_alternative_plot(country_total_count,
                        country_cluster,
                        country_cluster_stderrlog10,
                        country_cluster_alt,
                        country_cluster_alt_stderrlog10,
                        alt_y_label,
                        alt_y_axis_label,
                        fig,ax2,'B')

    alt_y_label="log_median_neg_binomial"
    fig.tight_layout()
    fig.savefig('psi_N_'+alt_y_label+'.eps')
    plt.close()



    alt_y_axis_label = "$\psi_i^{\mathrm{2\,day}}$"
    (country_total_count,
     country_cluster,
     country_cluster_stderrlog10,
     country_cluster_alt,
     country_cluster_alt_stderrlog10) = np.loadtxt(phi_N_file,
                                                   usecols=(1, 6,7, 17,18),
                                                   dtype=np.float,
                                                   unpack=True)
    fig, (ax1, ax2) = plt.subplots(1,2)
    do_alternative_plot(country_total_count,
                        country_cluster,
                        country_cluster_stderrlog10,
                        country_cluster_alt,
                        country_cluster_alt_stderrlog10,
                        alt_y_label,
                        alt_y_axis_label,
                        fig,ax1,'A')

    alt_y_axis_label = "$\psi_i^{\mathrm{3\,day}}$"
    (country_total_count,
     country_cluster,
     country_cluster_stderrlog10,
     country_cluster_alt,
     country_cluster_alt_stderrlog10) = np.loadtxt(phi_N_file,
                                                   usecols=(1, 6,7, 20,21),
                                                   dtype=np.float,
                                                   unpack=True)

    do_alternative_plot(country_total_count,
                        country_cluster,
                        country_cluster_stderrlog10,
                        country_cluster_alt,
                        country_cluster_alt_stderrlog10,
                        alt_y_label,
                        alt_y_axis_label,
                        fig,ax2,'B')

    alt_y_label="bidays_tridays"
    fig.tight_layout()
    fig.savefig('psi_N_'+alt_y_label+'.eps')
    plt.close()

    
    print('pass_val = ',pass_val)
    sys.exit(pass_val)

#end __main__
            
