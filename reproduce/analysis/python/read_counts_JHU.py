#!/usr/bin/env python

# read_counts_JHU - read SARS-CoV-2 daily counts from JHU CSSEGIS database
# (C) 2020 Boud Roukema GPL-3+
#
# This script is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This script is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.

import sys
import os
import numpy as np
#from numpy.random import default_rng

import re
import datetime

debug = False

def initialise_ISO31661_codes():
    # For convenience, this list may include non-standard extensions to the formal ISO-3166-1 codes.
    iso_3166_1_codes = np.array([
        ['DZ', 'Algeria'],
        ['AO', 'Angola'],
        ['BF', 'Burkina_Faso'],
        ['BI', 'Burundi'],
        ['BJ', 'Benin'],
        ['BW', 'Botswana'],
        ['CM', 'Cameroon'],
        ['CV', 'Cabo_Verde'],
        ['CF', 'Central_African_Republic'],
        ['TD', 'Chad'],
        ['KM', 'Comoros'],
        ['CG', 'Congo__Brazzaville_'],
        ['CD', 'Congo__Kinshasa_'],
        ['DJ', 'Djibouti'],
        ['EG', 'Egypt'],
        ['GQ', 'Equatorial_Guinea'],
        ['ER', 'Eritrea'],
        ['SZ', 'Eswatini'],
        ['ET', 'Ethiopia'],
        ['GA', 'Gabon'],
        ['GM', 'Gambia'],
        ['GH', 'Ghana'],
        ['GN', 'Guinea'],
        ['GW', 'Guinea-Bissau'],
        ['GY', 'Guyana'],
        ['CI', 'Ivory_Coast'],
        ['KE', 'Kenya'],
        ['LS', 'Lesotho'],
        ['LR', 'Liberia'],
        ['LY', 'Libya'],
        ['MG', 'Madagascar'],
        ['MW', 'Malawi'],
        ['ML', 'Mali'],
        ['MR', 'Mauritania'],
        ['MU', 'Mauritius'],
        ['MA', 'Morocco'],
        ['MZ', 'Mozambique'],
        ['NA', 'Namibia'],
        ['NE', 'Niger'],
        ['NG', 'Nigeria'],
        ['RW', 'Rwanda'],
        ['ST', 'Sao_Tome_and_Principe'],
        ['SN', 'Senegal'],
        ['SC', 'Seychelles'],
        ['SL', 'Sierra_Leone'],
        ['SO', 'Somalia'],
        ['SD', 'Sudan'],
        ['SS', 'South_Sudan'],
        ['ZA', 'South_Africa'],
        ['TZ', 'Tanzania'],
        ['TG', 'Togo'],
        ['TN', 'Tunisia'],
        ['UG', 'Uganda'],
        ['EH', 'Western_Sahara'],
        ['ZM', 'Zambia'],
        ['ZW', 'Zimbabwe'],
        ['AG', 'Antigua_and_Barbuda'],
        ['AR', 'Argentina'],
        ['BS', 'Bahamas'],
        ['BB', 'Barbados'],
        ['BO', 'Bolivia'],
        ['BR', 'Brazil'],
        ['BZ', 'Belize'],
        ['CA', 'Canada'],
        ['CL', 'Chile'],
        ['CO', 'Colombia'],
        ['CR', 'Costa_Rica'],
        ['CU', 'Cuba'],
        ['DM', 'Dominica'],
        ['DO', 'Dominican_Republic'],
        ['EC', 'Ecuador'],
        ['SV', 'El_Salvador'],
        ['WG', 'Grenada'],
        ['GT', 'Guatemala'],
        ['HT', 'Haiti'],
        ['HN', 'Honduras'],
        ['JM', 'Jamaica'],
        ['MX', 'Mexico'],
        ['NI', 'Nicaragua'],
        ['PA', 'Panama'],
        ['PY', 'Paraguay'],
        ['PE', 'Peru'],
        ['KN', 'Saint_Kitts_and_Nevis'],
        ['LC', 'Saint_Lucia'],
        ['VC', 'Saint_Vincent_and_the_Grenadines'],
        ['SR', 'Suriname'],
        ['TT', 'Trinidad_and_Tobago'],
        ['US', 'United_States'],
        ['UY', 'Uruguay'],
        ['VE', 'Venezuela'],
        ['AF', 'Afghanistan'],
        ['AM', 'Armenia'],
        ['AZ', 'Azerbaijan'],
        ['BH', 'Bahrain'],
        ['BD', 'Bangladesh'],
        ['BT', 'Bhutan'],
        ['BN', 'Brunei'],
        ['KH', 'Cambodia'],
        ['CN', 'Mainland_China'],
        ['GE', 'Georgia'],
        ['HK', 'Hong_Kong'],
        ['IN', 'India'],
        ['ID', 'Indonesia'],
        ['IR', 'Iran'],
        ['IQ', 'Iraq'],
        ['IL', 'Israel'],
        ['JP', 'Japan'],
        ['JO', 'Jordan'],
        ['KZ', 'Kazakhstan'],
        ['KR', 'South_Korea'],
        ['KW', 'Kuwait'],
        ['KG', 'Kyrgyzstan'],
        ['LA', 'Laos'],
        ['LB', 'Lebanon'],
        ['MY', 'Malaysia'],
        ['MV', 'Maldives'],
        ['MN', 'Mongolia'],
        ['MM', 'Myanmar'],
        ['NP', 'Nepal'],
        ['OM', 'Oman'],
        ['PK', 'Pakistan'],
        ['PS', 'Palestine'],
        ['PH', 'Philippines'],
        ['QA', 'Qatar'],
        ['SA', 'Saudi_Arabia'],
        ['SG', 'Singapore'],
        ['LK', 'Sri_Lanka'],
        ['SY', 'Syria'],
        ['TW', 'Taiwan'],
        ['TJ', 'Tajikistan'],
        ['TH', 'Thailand'],
        ['TL', 'Timor-Leste'],
        ['AE', 'United_Arab_Emirates'],
        ['UZ', 'Uzbekistan'],
        ['VN', 'Vietnam'],
        ['YE', 'Yemen'],
        ['AD', 'Andorra'],
        ['AL', 'Albania'],
        ['AT', 'Austria'],
        ['BY', 'Belarus'],
        ['BE', 'Belgium'],
        ['BA', 'Bosnia_and_Herzegovina'],
        ['BG', 'Bulgaria'],
        ['HR', 'Croatia'],
        ['CY', 'Cyprus'],
        ['CZ', 'Czech_Republic'],
        ['DK', 'Denmark'],
        ['EE', 'Estonia'],
        ['FI', 'Finland'],
        ['FR', 'France'],
        ['DE', 'Germany'],
        ['GR', 'Greece'],
        ['HU', 'Hungary'],
        ['IS', 'Iceland'],
        ['IE', 'Republic_of_Ireland'],
        ['IT', 'Italy'],
        ['XK', 'Kosovo'],
        ['LV', 'Latvia'],
        ['LI', 'Liechtenstein'],
        ['LT', 'Lithuania'],
        ['LU', 'Luxembourg'],
        ['MT', 'Malta'],
        ['MD', 'Moldova'],
        ['MC', 'Monaco'],
        ['ME', 'Montenegro'],
        ['NE', 'Netherlands'],
        ['MK', 'North_Macedonia'],
        ['NO', 'Norway'],
        ['PL', 'Poland'],
        ['PT', 'Portugal'],
        ['RO', 'Romania'],
        ['RU', 'Russia'],
        ['SM', 'San_Marino'],
        ['RS', 'Serbia'],
        ['SK', 'Slovakia'],
        ['SI', 'Slovenia'],
        ['ES', 'Spain'],
        ['SE', 'Sweden'],
        ['CH', 'Switzerland'],
        ['TR', 'Turkey'],
        ['UA', 'Ukraine'],
        ['GB', 'United_Kingdom'],
        ['VA', 'Vatican_City'],
        ['AU', 'Australia'],
        ['FJ', 'Fiji'],
        ['PF', 'French_Polynesia'],
        ['NC', 'New_Caledonia'],
        ['NZ', 'New_Zealand'],
        ['PG', 'Papua_New_Guinea'],
        ['SH', 'Saint_Helena_Ascension_and_Tristan_da_Cunha'],
        ['MH', 'Marshall_Islands'],
        ['FM', 'Micronesia'],
        ['WS', 'Samoa'],
        ['SB', 'Solomon_Islands'],
        ['VU', 'Vanuatu']
    ])

    iso_3166_1_alternatives = np.array([
        ['CN', 'China'],
        ['CI', 'Cote_d_Ivoire'],
        ['MM', 'Burma'],
        ['CZ', 'Czechia'],
        ['XX', 'Diamond_Princess'],
        ['XX', 'MS_Zaandam'],
        ['VA', 'Holy_See'],
        ['IE', 'Ireland'],
        ['TW', 'Taiwan_'],
        ['US', 'US'],
        ['PS', 'West_Bank_and_Gaza']
    ])

    return iso_3166_1_codes, iso_3166_1_alternatives


def read_counts_JHU_CSSEGIS(input_filename):
    if('JHU_CSSEGIS_SARSCoV2.dat' in input_filename):

        ## Read dates:
        dates_raw = np.loadtxt(input_filename,
                           delimiter=',',
                           skiprows=0,
                           max_rows=1,
                           dtype=np.str)
        if(debug):
            print('dates_raw = ',dates_raw)
        date0split = np.fromstring(dates_raw[4], dtype=int, sep='/', count=3)
        date0 = datetime.date(2000+date0split[2], date0split[0], date0split[1]) # assumes m/d/yy US style
        print('first date = ',date0.isoformat())

        N_dates = dates_raw.size - 4
        print('N_dates = ',N_dates)
        dates = np.array([])
        for date in dates_raw[4:4+N_dates]:
            d_split = np.fromstring(date, dtype=int, sep='/', count=3)
            # Assume m/d/yy US style in XXIst century;
            # convert to ISO format.
            d = (datetime.date(2000+d_split[2], d_split[0], d_split[1])).isoformat()
            dates = np.append(dates, d)


        ## Create country list
        countries_repeats = np.loadtxt(input_filename,
                                       delimiter=',',
                                       skiprows=1,
                                       dtype=np.str,
                                       usecols=1)

        country_list = np.unique(countries_repeats)
        if(debug):
            print('country_list = ',country_list)
            print('N country_list = ',country_list.size)

        iso_3166_1_codes, iso_3166_1_alternatives = initialise_ISO31661_codes()
        all_countries_found = True

        countries_iso = np.array([])

        for country in country_list:
            country_tidied = re.sub("[ ()'*]",'_',country)
            index = np.array(np.where(iso_3166_1_codes[:,1] == country_tidied))

            if index.size > 0:
                country_iso = iso_3166_1_codes[index,0]
                countries_iso = np.append(countries_iso, country_iso)
                if(debug):
                    print('check country: ',country_iso, country_tidied)
            else:
                index = np.array(np.where(iso_3166_1_alternatives[:,1] == country_tidied))
                if index.size > 0:
                    country_iso = iso_3166_1_alternatives[index,0]
                    countries_iso = np.append(countries_iso, country_iso)
                    if(debug):
                        print('check country (alt): ',country_iso, country_tidied)
                else:
                    print('read_counts_JHU_CSSEGIS: Warning: country not found: ',country_tidied)
                    all_countries_found = False

        print('countries_iso = ',countries_iso)

        if(not all_countries_found):
            print('read_counts_JHU_CSSEGIS: Warning: some countries not found. See above.')

        ## Read cumulative count data

        cum_counts_all = np.loadtxt(input_filename,
                                    delimiter=',',
                                    skiprows=1,
                                    usecols=tuple(range(4,4+N_dates)),
                                    dtype=np.int)

        ## Sum over provinces/states of any given country;
        ## prepare output arrays in same format as for other input files.
        out_countries = np.array([])
        out_dates = np.array([])
        out_counts = np.array([],dtype=np.int)

        counts_this_country = np.zeros(shape=(N_dates,), dtype=np.int)

        for country in zip(country_list,countries_iso):
            indices = (np.array(np.where(countries_repeats == country[0])))[0]
            if(indices.size < 1):
                raise RuntimeError('read_counts_JHU_CSSEGIS: Error: country inconsistency: country, indices = ',
                                   country[0],indices)

            cc = cum_counts_all[indices,:]
            if(debug):
                print('country=',country[0])
            if((cc.shape)[0] > 1):
                cum_counts = np.sum(cc, 0)
            else:
                cum_counts = cc[0] # drop an array dimension
            if(debug):
                print('after sum:',cc)
                print('cc.shape = ',cc.shape)

            daily_counts_this_country = (cum_counts[1:N_dates] -
                                         cum_counts[0:N_dates-1])
            counts_this_country[0] = cum_counts[0] # initial day
            # append the daily counts on all non-initial days
            counts_this_country[1:N_dates] = daily_counts_this_country


            # Append the country name N_dates times
            out_countries = np.append(out_countries, np.repeat(country[1], N_dates))
            # Append the dates in iso format
            out_dates = np.append(out_dates, dates)
            # Append the daily counts
            out_counts = np.append(out_counts, counts_this_country)

    else:
        raise RuntimeError('Format of file ',input_filename,' unknown')


    return (out_countries, out_counts, out_dates)




if __name__ == '__main__':

    f = os.environ.get('INPUT_FILE')
    if(f):
        input_filename = f
    else:
        # default
        input_filename = 'JHU_CSSEGIS_SARSCoV2.dat'


    countries, counts, dates = read_counts_JHU_CSSEGIS(input_filename)

    for triplet in zip(countries,counts,dates):
        print(triplet)

    pass_val = 0 # pass_val value of test: 0 = OK, 1 = fail


    print('pass_val = ',pass_val)
    sys.exit(pass_val)

#end: __main__
