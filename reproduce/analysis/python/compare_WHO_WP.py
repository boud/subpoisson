#!/usr/bin/env python

# compare_WHO_WP - compare WHO to Wikipedia medical cases chart data
# (C) 2020 Boud Roukema GPL-3+
#
# This script is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This script is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.

import sys
import os
import numpy as np

from subpoisson import read_counts
from count_jumps_mod import count_jumps

import matplotlib
import matplotlib.pyplot as plt

if __name__ == '__main__':

    n = os.environ.get('N_THRESHOLD_START')
    if(n):
        N_thresh_start = np.int(n)
    else:
        N_thresh_start = 30

    print('compare_WHO_WP: N_thresh_start = ',N_thresh_start)

    input_filenames = []

    f = os.environ.get('INPUT_FILE_0')
    if(f):
        input_filenames.append(f)
    else:
        input_filenames.append('outputs/WHO-COVID-19-global-data.csv.cleaned')

    f = os.environ.get('INPUT_FILE_1')
    if(f):
        input_filenames.append(f)
    else:
        input_filenames.append('inputs/Wikipedia_SARSCoV2_charts.dat')

    input_files_short = np.array(['WHO-COVID-19csv',
                                  'WPmedcaseschart'])
    print('compare_WHO_WP: Please check that the input_filenames are correct: ')
    print(input_files_short[0],' = ',input_filenames[0])
    print(input_files_short[1],' = ',input_filenames[1], '\n')


    N_jumps_dict = [{},{}] # initialise two dictionaries
    files_indices = [0, 1]
    N_countries = np.array([])

    for input_file in zip(input_filenames, input_files_short, files_indices):
        countries_repeated, counts, dates = read_counts(input_file[0])

        country_list = np.unique(countries_repeated)
        N_countries = np.append(N_countries, country_list.size)
        N_jumps_array = np.array([])
        for country in country_list:
            index_country = (np.array(np.where(countries_repeated == country)))[0]
            N_jumps = count_jumps(counts[index_country], N_thresh_start)

            N_jumps_array = np.append(N_jumps_array, N_jumps)
            (N_jumps_dict[input_file[2]])[country] = N_jumps

    # Find the countries in common as a set; convert to a list;
    # convert to a numpy array; sort it:
    in_common = np.sort(np.array(list(N_jumps_dict[0].keys() & N_jumps_dict[1].keys())))
    print('in_common =',in_common,'\n')

    ## There are very likely more elegant pythonic ways of doing the following:
    N_common_0_jumps = np.array([])
    N_common_1_jumps = np.array([])
    for key in in_common:
        N_common_0_jumps = np.append(N_common_0_jumps, (N_jumps_dict[0])[key])
        N_common_1_jumps = np.append(N_common_1_jumps, (N_jumps_dict[1])[key])
    for c_tuple in zip(in_common, N_common_0_jumps, N_common_1_jumps):
        print('jumps: ',c_tuple[0],c_tuple[1],c_tuple[2])

    plt.clf()
    fig = plt.figure()
    ax = fig.add_subplot(111)
    plt.rcParams.update({'font.size': 16,
                         'font.family': 'serif'})

    xylims_tuple = (0.0,50.0)
    xylims = np.array(xylims_tuple)

    plt.plot(xylims,xylims,'-r',
             linewidth=0.5,
             label='equal quality')

    plt.scatter(N_common_0_jumps,
                N_common_1_jumps,
                marker='o',
                c='k',
                s=3**2,
                label='WHO vs WP')

    plt.xlim(xylims_tuple)
    plt.ylim(xylims_tuple)

    #plt.legend() # the reader might suspect that points are hidden
    plt.xlabel(r'$N_{\mathrm{jump}}$ (WHO)',fontsize=16)
    plt.ylabel(r'$N_{\mathrm{jump}}$ (WP med. cases charts)',fontsize=16)
    ax.tick_params(axis='both', which='major', labelsize=14)
    ax.set_aspect('equal')

    plt.tight_layout()
    plt.savefig('WHO_vs_WP.eps')
    plt.savefig('WHO_vs_WP.svg')
    plt.close()

    ## Countries counts
    with open('WHO_vs_WP_countries.dat','w+') as who_vs_wp_c_file:
        who_vs_wp_c_file.write("N_WHO_countries = %d\n" % N_countries[0])
        who_vs_wp_c_file.write("N_WPCCTF_countries = %d\n" % N_countries[1])
        who_vs_wp_c_file.write("N_common_countries = %d\n" % in_common.size)
    who_vs_wp_c_file.close()


    ## data table
    with open('WHO_vs_WP.dat','w+') as who_vs_wp_file:
        who_vs_wp_file.write(
            "# Numbers of jumps/drops in adjacent days of SARS-CoV-2 daily infection counts\n")
        who_vs_wp_file.write(
            "#   in countries common to WHO data and Wikipedia WikiProject COVID-19\n")
        who_vs_wp_file.write(
            "#   Case Count Task Force (C19CCTF) medical cases chart data.\n")
        who_vs_wp_file.write(
            "# Column 1: XX   [%2s]  country ISO 3166-1 alpha-2 code (no jurisdictional claims\n")
        who_vs_wp_file.write(
            "# Column 2: N_jump  [%d]  WHO\n")
        who_vs_wp_file.write(
            "# Column 3: N_jump  [%d]  C19CCTF\n")
        for c_tuple in zip(in_common, N_common_0_jumps, N_common_1_jumps):
            who_vs_wp_file.write('%2s %3d %3d\n' % c_tuple)
    who_vs_wp_file.close()

#end __main__
