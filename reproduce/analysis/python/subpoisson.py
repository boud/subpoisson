#!/usr/bin/env python

# subpoisson - analyse noise in SARS-CoV-2 daily counts
# (C) 2020 Boud Roukema GPL-3+
#
# This script is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This script is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.

import sys
import os
import numpy as np
from scipy import stats
#from numpy.random import default_rng

from find_range import find_high_enough_subrange
from get_noise import get_noise_statistics, get_subseq_noise_statistics
from theil_sen_mod import theil_sen, theil_sen_robust_stderr
from plot_countries_mod import plot_countries
from read_counts_JHU import read_counts_JHU_CSSEGIS
from detect_weekly_dip import detect_weekly_dip

import matplotlib
import matplotlib.pyplot as plt

import datetime

import multiprocessing

def read_counts(input_filename):
    print('==========================================================')
    print('read_counts: Will read: ',input_filename)
    print('----------------------------------------------------------')
    if('WHO-COVID-19-global-data.csv.cleaned' in input_filename):
        countries = np.loadtxt(input_filename,
                               delimiter=',',
                               usecols=1,
                               skiprows=1,
                               dtype=np.str)
        counts = np.loadtxt(input_filename,
                            delimiter=',',
                            usecols=4,
                            skiprows=1,
                            dtype=np.int)
        dates = np.loadtxt(input_filename,
                           delimiter=',',
                           usecols=0,
                           skiprows=1,
                           dtype=np.str)

        country_list = np.unique(countries)
        print('country_list = ',country_list)

    elif('Wikipedia_SARSCoV2_charts.dat' in input_filename):
        countries = np.loadtxt(input_filename,
                               delimiter=',',
                               usecols=1,
                               skiprows=0,
                               dtype=np.str)
        cum_counts_all = np.loadtxt(input_filename,
                            delimiter=',',
                            usecols=2,
                            skiprows=0,
                            dtype=np.int)
        dates = np.loadtxt(input_filename,
                           delimiter=',',
                           usecols=0,
                           skiprows=0,
                           dtype=np.str)

        country_list = np.unique(countries)
        print('country_list = ',country_list)
        N_all = countries.size
        counts = np.zeros(N_all)
        for country in country_list:
            index_country = (np.array(np.where(countries == country)))[0]
            #print('index_country = ',index_country)
            cum_counts = cum_counts_all[index_country]
            #print('cum_counts = ',cum_counts)
            N_this_country = cum_counts.size
            counts_this_country = (cum_counts[1:N_this_country] -
                                   cum_counts[0:N_this_country-1])
            #print('counts_this_country =',counts_this_country)
            counts[index_country[0]] = cum_counts[0] # add the initial day
            counts[index_country[1:N_this_country]] = counts_this_country # add the increments

    elif('JHU_CSSEGIS_SARSCoV2.dat' in input_filename):
        (countries, counts, dates) = (
            read_counts_JHU_CSSEGIS(input_filename) )
    else:
        raise RuntimeError('Format of file ',input_filename,' unknown')


    return (countries, counts, dates)


def analyse_country(country,
                    countries_repeated,
                    counts,
                    dates,
                    N_thresh_start,
                    N_thresh_stop,
                    min_days,
                    stop_days,
                    N_full,
                    sequence_type,
                    N_subseq_length,
                    fixed_rng_seed,
                    use_phiprime_model,
                    use_arithmetic_median,
                    multidays):

    verbose = True
    # use_phiprime_model = False
    
    index_country = (np.where(countries_repeated == country))[0]
    Noft = counts[index_country]
    if(verbose):
        print('Noft = ',Noft)

    ## Data curation method for WHO data only - WHO data appears to be
    ## messed up either because of time zone confusion or confusion in
    ## terms of datestamping - some sequences of approximately
    ## {n_i(j), 0, 2 n_i(j)}
    ## or even
    ## {n_i(j), 2 n_i(j), n_i(j), n_i(j), 0, n_i(j)}
    ## appear for some countries (the underlying curve is not really
    ## constant, of course).
    #
    # Noft, N_replaced, mean_replaced = replace_pairs(Noft_initial)
    # print('country, N_replaced, mean_replaced = %2s %8d %.1f' %
    #       (country,N_replaced,mean_replaced))

    ## Wikipedia medical cases chart template data does not need the WHO
    ## data adjustment
    print('::: ',country, 'Noft = ',Noft)
    OK, i_valid = find_high_enough_subrange(
        Noft, N_thresh_start, N_thresh_stop,
        min_days, min_days, stop_days)

    if(verbose):
        print(country," OK=",OK)
    if(OK):
        if(verbose):
            print("Noft(i_valid) = ",Noft[i_valid])
            print("")
        Noft_valid = Noft[i_valid] # forget the original indexing

        if(N_subseq_length == N_full):
            # full sequence
            (valid, p_KS_zeroparam,
             cluster, p_cluster,
             stderr_cluster, stderrlog10_cluster) = (
                 get_noise_statistics(Noft_valid,fixed_rng_seed, use_phiprime_model, use_arithmetic_median, multidays) )
        else:
            # subsequence
            if(verbose):
                print('Checking subsequence of ',country)
            (valid, p_KS_zeroparam,
             cluster, p_cluster,
             stderrlog10_cluster,
             i_start_best_subseq) = (
                 get_subseq_noise_statistics(Noft_valid, N_subseq_length,
                                             fixed_rng_seed, use_phiprime_model, use_arithmetic_median, multidays) )

        if(valid):
            Noft_total = np.sum(Noft)
            if(verbose):
                print(country,':: ',sequence_type,':: total_count = ',Noft_total)
                print(country,':: ',sequence_type,':: cluster = ',cluster)
                print(country,':: ',sequence_type,':: p_cluster = ',p_cluster)

            country_valid = country

            if(N_subseq_length == N_full):
                if(verbose):
                    print('i_valid[0:2]= ',i_valid[0:2])
                    print('(dates[index_country])[0:5]= ',(dates[index_country])[0:5])
                start_date = (dates[index_country])[i_valid[0]]
                if(verbose):
                    print('start_date= ',(dates[index_country])[i_valid[0]])
                Noft_valid_subseq = Noft_valid

            else:
                # i_valid[0] should be the first index of valid days, within counts[index_country]
                # i_start_best_subseq is from get_subseq_noise_statistics, starting at 0
                start_date = (dates[index_country])[i_valid[0]+i_start_best_subseq]
                if(verbose):
                    print('i_valid[0:2]= ',i_valid[0:2])
                    print('(dates[index_country])[0:5]= ',(dates[index_country])[0:5])
                    print('i_start_best_subseq= ',i_start_best_subseq)
                    print('start_date= ',(dates[index_country])[i_valid[0]])
                Noft_valid_subseq = Noft_valid[i_start_best_subseq:i_start_best_subseq+seq_tuple[1]]

            Noft_total_subseq = np.sum(Noft_valid_subseq)
        else:
            print(country,': ',seq_tuple[0],':: too few values')
            country_valid = '__' # by default, the country is invalid

    return (country_valid,
            valid,
            p_KS_zeroparam,
            Noft_total,
            Noft_total_subseq,
            cluster,
            p_cluster,
            stderrlog10_cluster,
            start_date,
            Noft_valid_subseq)



def cumulate_country_analyses( analysis_tuple ):
    ## Warning: this operates on global variables beyond the scope of this function.

    global country_valid_generic
    global country_p_KS_zeroparam_generic
    global country_total_count_generic
    global country_total_count_subseq_generic
    global country_cluster_generic
    global country_p_cluster_generic
    global country_cluster_stderrlog10_generic
    global country_start_date_generic
    global country_Noft_generic

    country_valid_0 = analysis_tuple[0]
    valid_0 = analysis_tuple[1]
    p_KS_zeroparam_0 = analysis_tuple[2]
    total_count_0 = analysis_tuple[3]
    total_count_subseq_0 = analysis_tuple[4]
    cluster_0 = analysis_tuple[5]
    p_cluster_0 = analysis_tuple[6]
    stderrlog10_cluster_0 = analysis_tuple[7]
    start_date_0 = analysis_tuple[8]
    Noft_valid_subseq_0 = analysis_tuple[9]

    if(valid_0):
        country_valid_generic = np.append(country_valid_generic, country_valid_0)
        country_p_KS_zeroparam_generic = np.append(country_p_KS_zeroparam_generic, p_KS_zeroparam_0)
        country_total_count_generic = np.append(country_total_count_generic, total_count_0)
        country_total_count_subseq_generic = np.append(country_total_count_subseq_generic,
                                               total_count_subseq_0)
        country_cluster_generic = np.append(country_cluster_generic, cluster_0)
        country_p_cluster_generic = np.append(country_p_cluster_generic, p_cluster_0)
        country_cluster_stderrlog10_generic = np.append(country_cluster_stderrlog10_generic,
                                                stderrlog10_cluster_0)
        country_start_date_generic = np.append(country_start_date_generic, start_date_0)
        country_Noft_generic.append(Noft_valid_subseq_0)



if __name__ == '__main__':

    f = os.environ.get('INPUT_FILE')
    if(f):
        input_filename = f
    else:
        # default
        input_filename = 'Wikipedia_SARSCoV2_charts.dat'

    n = os.environ.get('N_THRESHOLD_START')
    if(n):
        N_thresh_start = np.int(n)
    else:
        N_thresh_start = 30

    n = os.environ.get('N_THRESHOLD_STOP')
    if(n):
        N_thresh_stop = np.int(n)
    else:
        N_thresh_stop = 30

    n = os.environ.get('MIN_DAYS')
    if(n):
        min_days = np.int(n)
    else:
        min_days = 7

    n = os.environ.get('STOP_DAYS')
    if(n):
        stop_days = np.int(n)
    else:
        stop_days = 2

    n = os.environ.get('N_LOWEST_PSI')
    if(n):
        N_lowest_psi = np.int(n)
    else:
        N_lowest_psi = 5

    n = os.environ.get('N_LOWEST_PHI')
    if(n):
        N_lowest_phi = np.int(n)
    else:
        N_lowest_phi = 5

    n = os.environ.get('N_PLOT_LOWEST_PHI')
    if(n):
        N_plot_lowest_phi = np.int(n)
    else:
        N_plot_lowest_phi = 5

    n = os.environ.get('N_PLOT_MEDIAN_PHI')
    if(n):
        N_plot_median_phi = np.int(n)
    else:
        N_plot_median_phi = 2

    n = os.environ.get('N_PLOT_N_MIN_LOWEST_PHI')
    if(n):
        N_plot_N_min_lowest_phi = np.int(n)
    else:
        N_plot_N_min_lowest_phi = 10000


    n = os.environ.get('N_SUBSEQ_LENGTH_A')
    if(n):
        N_subseq_length_a = np.int(n)
    else:
        N_subseq_length_a = 28

    n = os.environ.get('N_SUBSEQ_LENGTH_B')
    if(n):
        N_subseq_length_b = np.int(n)
    else:
        N_subseq_length_b = 0

    n = os.environ.get('N_SUBSEQ_LENGTH_C')
    if(n):
        N_subseq_length_c = np.int(n)
    else:
        N_subseq_length_c = 0

    n = os.environ.get('FIXED_RNG_SEED')
    if(n):
        fixed_rng_seed = np.bool(n)
    else:
        fixed_rng_seed = False

    n = os.environ.get('FIXED_RNG_SEED_VALUE')
    if(n):
        fixed_rng_seed_value = np.int(n)
    else:
        fixed_rng_seed_value = 8999

    n = os.environ.get('N_CPUS')
    if(n):
        n_cpus = np.int(n)
    else:
        n_cpus = 4

    ## This is a development option for fast, inaccurate calculations,
    ## for checking the software validity.
    n = os.environ.get('ENABLE_DEV_OVERRIDE')
    if(n):
        print('\n\nsubpoisson: ENABLE_DEV_OVERRIDE is enabled.\n\n')
        enable_dev_override = True
    else:
        enable_dev_override = False

    global country_valid_generic
    global country_p_KS_zeroparam_generic
    global country_total_count_generic
    global country_total_count_subseq_generic
    global country_cluster_generic
    global country_p_cluster_generic
    global country_cluster_stderrlog10_generic
    global country_start_date_generic
    global country_Noft_generic

    output_countries_filename_base = "countries_out_"
    output_phi_N_filename_base = "phi_N_"
    output_AIC_BIC_filename_base = "AIC_BIC_"
    output_weeklydip_filename_base = "weeklydip_"

    pass_val = 0 # pass_val value of test: 0 = OK, 1 = fail

    print("N_thresh_start = ",N_thresh_start)
    print("N_thresh_stop = ",N_thresh_stop)
    print("pass_val value = ",pass_val)

    countries_repeated, counts, dates = read_counts(input_filename)

    # sanity check
    print('countries_repeated[124:128]= ',countries_repeated[124:128])
    print('counts[124:128]= ',counts[124:128])
    print('dates[124:128]= ',dates[124:128])

    country_list = np.unique(countries_repeated)
    N_countries = country_list.size
    print("Found ",N_countries," countries: ",country_list)

    ## Count and then override any negative values
    i_neg = (np.array(np.where(counts < 0)))[0]
    N_neg = (i_neg.shape)[0]
    N_counts_all = counts.size

    # Replace the negatives with zeros
    if(N_neg > 0):
        counts[i_neg] = 0


    ## Loop over full sequence and subsequence searches.

    sequence_types = np.array(["full", "a", "b", "c"])
    N_full = 999999 # indicates that the full sequence should be used (about 3000 years)
    N_subseq_lengths = np.array([N_full,
                                N_subseq_length_a, N_subseq_length_b, N_subseq_length_c])

    # Initiate storage for sequences that have median phi_i values
    ## plots for countries with the median phi values
    # arrays or lists to feed to plotter:
    c_med_plot_valid = np.array([],dtype=np.str)
    c_med_plot_Noft = []
    c_med_plot_start_date = []
    c_med_plot_cluster = []

    for seq_tuple in zip(sequence_types, N_subseq_lengths):
        if(seq_tuple[1] > 0):
            output_countries_filename = (output_countries_filename_base +
                                         seq_tuple[0] + '.dat')
            output_phi_N_filename = (output_phi_N_filename_base +
                                     seq_tuple[0] + '.dat')
            output_AIC_BIC_filename = (output_AIC_BIC_filename_base +
                                       seq_tuple[0] + '.dat')
            output_weeklydip_filename = (output_weeklydip_filename_base +
                                         seq_tuple[0] + '.dat')

            if(seq_tuple[0] == "full"):
                with open(output_countries_filename,"w+") as countries_file:
                    countries_file.write("N_countries_all = %d\n" % N_countries)
                    countries_file.write("N_neg = %d\n" % N_neg)
                    countries_file.write("N_counts_all = %d\n" % N_counts_all)
                countries_file.close

            # (re-)initialise empty global arrays for countries with high enough subranges
            country_valid_generic = np.array([],dtype=np.str)
            country_p_KS_zeroparam_generic = np.array([],dtype=np.float)
            country_total_count_generic = np.array([],dtype=np.int)
            country_total_count_subseq_generic = np.array([],dtype=np.int)
            country_cluster_generic = np.array([],dtype=np.float)
            country_cluster_stderrlog10_generic = np.array([],dtype=np.float)
            country_p_cluster_generic = np.array([],dtype=np.float)
            country_start_date_generic = np.array([],dtype=np.str)
            country_Noft_generic = [] # ordinary python list of np.arrays, initially empty

            pool = multiprocessing.Pool(n_cpus)

            # The adjective 'heavy' refers to python calculations (C would be much faster)
            country_list_heavy_calculations = country_list
            if(enable_dev_override):
                ## For development purposes, an arbitrary sublist of
                ## countries is given here.  This list must include at
                ## least six countries (due to automatic outputs below).
                ##
                country_list_heavy_calculations = ['DZ', 'BY', 'MX', 'BD', 'US', 'DE']

            # Fiducial model
            use_phiprime_model = True
            use_arithmetic_median = True
            multidays = 1

            for country in country_list_heavy_calculations:
                pool.apply_async(analyse_country,
                                 args = (country,
                                         countries_repeated,
                                         counts,
                                         dates,
                                         N_thresh_start,
                                         N_thresh_stop,
                                         min_days,
                                         stop_days,
                                         N_full,
                                         seq_tuple[0],
                                         seq_tuple[1],
                                         fixed_rng_seed,
                                         use_phiprime_model,
                                         use_arithmetic_median,
                                         multidays),
                                 callback = cumulate_country_analyses
                )

            pool.close()
            pool.join()

            # Copy the generic results to specific arrays; use np.array() or '0.0 +' or [:] to allocate independent memory
            country_valid = np.array(country_valid_generic)
            country_p_KS_zeroparam = 0.0 + country_p_KS_zeroparam_generic
            country_total_count = 0 + country_total_count_generic
            country_total_count_subseq = 0 + country_total_count_subseq_generic
            country_cluster = 0.0 + country_cluster_generic
            country_cluster_stderrlog10 = 0.0 + country_cluster_stderrlog10_generic
            country_p_cluster = 0.0 + country_p_cluster_generic
            country_start_date = np.array(country_start_date_generic)
            country_Noft = country_Noft_generic[:]
            countries_total_days = 0
            for a in country_Noft:
                countries_total_days += a.size

            N_countries_OK = country_valid.size
            print('N_countries_OK=',N_countries_OK," sequence type = ",seq_tuple[0])
            print('country_valid_generic = ',country_valid_generic) # debug only

            if(seq_tuple[0] != "full"):
                with open(output_countries_filename,"w+") as countries_file:
                    countries_file.write("N_countries_OK = %d\n" % N_countries)
                countries_file.close

            ## Get indices for sorting the countries:
            index_alphabetic = np.argsort(country_valid)  # (fiducial) alphabetical order
            index_lowest_phi = np.argsort(country_cluster[index_alphabetic])  # order by phi
            index_lowest_phi_alph = index_alphabetic[index_lowest_phi] # combined index

            #### Alternative models ####

            # Logarithmic median model instead of arithmetic median model (differ if number of neighbours is even)
            if(seq_tuple[0] == "full"):
                use_arithmetic_median = False
                use_phiprime_model = True
                multidays = 1

                # (re-)initialise empty array for countries with high enough subranges
                country_valid_generic = np.array([],dtype=np.str)
                country_p_KS_zeroparam_generic = np.array([],dtype=np.float)
                country_total_count_generic = np.array([],dtype=np.int)
                country_total_count_subseq_generic = np.array([],dtype=np.int)
                country_cluster_generic = np.array([],dtype=np.float)
                country_cluster_stderrlog10_generic = np.array([],dtype=np.float)
                country_p_cluster_generic = np.array([],dtype=np.float)
                country_start_date_generic = np.array([],dtype=np.str)
                country_Noft_generic = [] # ordinary python list of np.arrays, initially empty

                pool = multiprocessing.Pool(n_cpus)

                for country in country_list_heavy_calculations:
                    pool.apply_async(analyse_country,
                                     args = (country,
                                             countries_repeated,
                                             counts,
                                             dates,
                                             N_thresh_start,
                                             N_thresh_stop,
                                             min_days,
                                             stop_days,
                                             N_full,
                                             seq_tuple[0],
                                             seq_tuple[1],
                                             fixed_rng_seed,
                                             use_phiprime_model,
                                             use_arithmetic_median,
                                             multidays),
                                     callback = cumulate_country_analyses
                    )

                pool.close()
                pool.join()

                # Prepare indices to match the ordering of the fiducial model
                index_alphabetic_new = np.argsort(country_valid_generic)
                index_alphabetic_inv = np.argsort(index_alphabetic) # inverse order of the fiducial model
                index_alpha_new2old = index_alphabetic_new[index_alphabetic_inv]

                # Copy the generic results to specific arrays and sort them
                country_cluster_logmedian = country_cluster_generic[index_alpha_new2old]
                country_cluster_stderrlog10_logmedian = country_cluster_stderrlog10_generic[index_alpha_new2old]
                country_p_cluster_logmedian = country_p_cluster_generic[index_alpha_new2old]

            # Negative binomial model
            if(seq_tuple[0] == "full"):
                use_arithmetic_median = True
                use_phiprime_model = False
                multidays = 1

                # (re-)initialise empty array for countries with high enough subranges
                country_valid_generic = np.array([],dtype=np.str)
                country_p_KS_zeroparam_generic = np.array([],dtype=np.float)
                country_total_count_generic = np.array([],dtype=np.int)
                country_total_count_subseq_generic = np.array([],dtype=np.int)
                country_cluster_generic = np.array([],dtype=np.float)
                country_cluster_stderrlog10_generic = np.array([],dtype=np.float)
                country_p_cluster_generic = np.array([],dtype=np.float)
                country_start_date_generic = np.array([],dtype=np.str)
                country_Noft_generic = [] # ordinary python list of np.arrays, initially empty

                pool = multiprocessing.Pool(n_cpus)

                for country in country_list_heavy_calculations:
                    pool.apply_async(analyse_country,
                                     args = (country,
                                             countries_repeated,
                                             counts,
                                             dates,
                                             N_thresh_start,
                                             N_thresh_stop,
                                             min_days,
                                             stop_days,
                                             N_full,
                                             seq_tuple[0],
                                             seq_tuple[1],
                                             fixed_rng_seed,
                                             use_phiprime_model,
                                             use_arithmetic_median,
                                             multidays),
                                     callback = cumulate_country_analyses
                    )

                pool.close()
                pool.join()

                # Prepare indices to match the ordering of the fiducial model
                index_alphabetic_new = np.argsort(country_valid_generic)
                index_alphabetic_inv = np.argsort(index_alphabetic) # inverse order of the fiducial model
                index_alpha_new2old = index_alphabetic_new[index_alphabetic_inv]

                # Copy the generic results to specific arrays and sort them
                country_cluster_negbin = country_cluster_generic[index_alpha_new2old]
                country_cluster_stderrlog10_negbin = country_cluster_stderrlog10_generic[index_alpha_new2old]
                country_p_cluster_negbin = country_p_cluster_generic[index_alpha_new2old]


            # Bidays model - try grouping pairs of days' data together
            if(seq_tuple[0] == "full"):
                use_arithmetic_median = True
                use_phiprime_model = False
                multidays = 2

                # (re-)initialise empty array for countries with high enough subranges
                country_valid_generic = np.array([],dtype=np.str)
                country_p_KS_zeroparam_generic = np.array([],dtype=np.float)
                country_total_count_generic = np.array([],dtype=np.int)
                country_total_count_subseq_generic = np.array([],dtype=np.int)
                country_cluster_generic = np.array([],dtype=np.float)
                country_cluster_stderrlog10_generic = np.array([],dtype=np.float)
                country_p_cluster_generic = np.array([],dtype=np.float)
                country_start_date_generic = np.array([],dtype=np.str)
                country_Noft_generic = [] # ordinary python list of np.arrays, initially empty

                pool = multiprocessing.Pool(n_cpus)

                for country in country_list_heavy_calculations:
                    pool.apply_async(analyse_country,
                                     args = (country,
                                             countries_repeated,
                                             counts,
                                             dates,
                                             N_thresh_start,
                                             N_thresh_stop,
                                             min_days,
                                             stop_days,
                                             N_full,
                                             seq_tuple[0],
                                             seq_tuple[1],
                                             fixed_rng_seed,
                                             use_phiprime_model,
                                             use_arithmetic_median,
                                             multidays),
                                     callback = cumulate_country_analyses
                    )

                pool.close()
                pool.join()

                # Prepare indices to match the ordering of the fiducial model
                # (TODO: modularise this or use more elegant python ordering and indexing functions.)
                dummy_value = -99.9
                country_cluster_bidays = np.array([],dtype=np.float)
                country_cluster_stderrlog10_bidays = np.array([],dtype=np.float)
                country_p_cluster_bidays = np.array([],dtype=np.float)
                # Iterate over the fiducial country ordering
                for c in country_valid:
                    tmp1 = dummy_value
                    tmp2 = dummy_value
                    tmp3 = dummy_value
                    # Iterate over the newly calculated values
                    for d in zip(country_valid_generic,
                                 country_cluster_generic,
                                 country_cluster_stderrlog10_generic,
                                 country_p_cluster_generic):
                        if(c == d[0]):
                            tmp1=d[1]
                            tmp2=d[2]
                            tmp3=d[3]
                    country_cluster_bidays = np.append(country_cluster_bidays, tmp1)
                    country_cluster_stderrlog10_bidays = np.append(country_cluster_stderrlog10_bidays, tmp2)
                    country_p_cluster_bidays = np.append(country_p_cluster_bidays, tmp3)

                countries_total_bidays = 0
                for a in country_Noft_generic:
                    countries_total_bidays += a.size

                N_countries_OK_bidays = country_valid_generic.size


            # Tridays model - try grouping triples of days' data together
            if(seq_tuple[0] == "full"):
                use_arithmetic_median = True
                use_phiprime_model = False
                multidays = 3

                # (re-)initialise empty array for countries with high enough subranges
                country_valid_generic = np.array([],dtype=np.str)
                country_p_KS_zeroparam_generic = np.array([],dtype=np.float)
                country_total_count_generic = np.array([],dtype=np.int)
                country_total_count_subseq_generic = np.array([],dtype=np.int)
                country_cluster_generic = np.array([],dtype=np.float)
                country_cluster_stderrlog10_generic = np.array([],dtype=np.float)
                country_p_cluster_generic = np.array([],dtype=np.float)
                country_start_date_generic = np.array([],dtype=np.str)
                country_Noft_generic = [] # ordinary python list of np.arrays, initially empty

                pool = multiprocessing.Pool(n_cpus)

                for country in country_list_heavy_calculations:
                    pool.apply_async(analyse_country,
                                     args = (country,
                                             countries_repeated,
                                             counts,
                                             dates,
                                             N_thresh_start,
                                             N_thresh_stop,
                                             min_days,
                                             stop_days,
                                             N_full,
                                             seq_tuple[0],
                                             seq_tuple[1],
                                             fixed_rng_seed,
                                             use_phiprime_model,
                                             use_arithmetic_median,
                                             multidays),
                                     callback = cumulate_country_analyses
                    )

                pool.close()
                pool.join()


                # Prepare indices to match the ordering of the fiducial model
                # (TODO: modularise this or use more elegant python ordering and indexing functions.)
                dummy_value = -99.9
                country_cluster_tridays = np.array([],dtype=np.float)
                country_cluster_stderrlog10_tridays = np.array([],dtype=np.float)
                country_p_cluster_tridays = np.array([],dtype=np.float)
                # Iterate over the fiducial country ordering
                for c in country_valid:
                    tmp1 = dummy_value
                    tmp2 = dummy_value
                    tmp3 = dummy_value
                    # Iterate over the newly calculated values
                    for d in zip(country_valid_generic,
                                 country_cluster_generic,
                                 country_cluster_stderrlog10_generic,
                                 country_p_cluster_generic):
                        if(c == d[0]):
                            tmp1=d[1]
                            tmp2=d[2]
                            tmp3=d[3]
                    country_cluster_tridays = np.append(country_cluster_tridays, tmp1)
                    country_cluster_stderrlog10_tridays = np.append(country_cluster_stderrlog10_tridays, tmp2)
                    country_p_cluster_tridays = np.append(country_p_cluster_tridays, tmp3)

                countries_total_tridays = 0
                for a in country_Noft_generic:
                    countries_total_tridays += a.size

                N_countries_OK_tridays = country_valid_generic.size


            ## Fit \phi_i vs N_i relation

            ## Use the indices to get a reproducible order bootstrap for the
            ## Theil-Sen uncertainty estimated by bootstrap:
            c_count_log = np.log10(country_total_count[index_lowest_phi_alph])
            c_cluster_log = np.log10(country_cluster[index_lowest_phi_alph])
            ## Warning: remember that c_count_log and c_cluster_log do
            ## not match the order of country_total_count and country_cluster!

            pslope, pzeropoint = theil_sen(c_count_log, c_cluster_log)
            sig_pslope, sig_pzeropoint = (
                theil_sen_robust_stderr(c_count_log, c_cluster_log,
                                        ths_fixed_rng_seed_value=(fixed_rng_seed_value+
                                                                  seq_tuple[1])) )
            print('::',seq_tuple[0],':: ',
                  'total_count vs cluster log10 slope = %g pm %g, zeropoint = %g pm %g'
                  % (pslope, sig_pslope, pzeropoint, sig_pzeropoint))
            with open(output_countries_filename,"a") as countries_file:
                countries_file.write('phi_N_'+seq_tuple[0]+'_slope = %.6f\n' % pslope)
                countries_file.write('phi_N_'+seq_tuple[0]+'_sig_slope = %.6f\n' % sig_pslope)
                countries_file.write('phi_N_'+seq_tuple[0]+'_zeropoint = %.6f\n' % pzeropoint)
                countries_file.write('phi_N_'+seq_tuple[0]+'_sig_zeropoint = %.6f\n' % sig_pzeropoint)
            countries_file.close()


            ## Weekly dip analysis
            if(seq_tuple[0] == "full"):
                country_dip = np.array([],dtype=np.float)
                for c_Noft in country_Noft:
                    # Warning: as of scipy-1.5.2, scipy fft had at least
                    # one bug with multiprocessing; fixes are in the
                    # pipeline: https://github.com/scipy/scipy/pull/11534
                    dip = detect_weekly_dip(c_Noft)
                    country_dip = np.append(country_dip, dip)

            ## Plots
            max_N_tot_plot = 3.0e7
            # 1.3e3
            max_cluster_plot = 2.0e3

            if(seq_tuple[0] == "full"):
                plot_label = ""
            else:
                plot_label = ("%2d-day seq." % seq_tuple[1])

            ## === probabilities vs N_i  ====
            plt.figure()
            plt.rcParams.update({'font.size': 18,
                                 'font.family': 'serif'})
            # Plot the probabilities of the zero-parameter Poisson model
            # (zero parameters in the sense that the data provide the mean
            # for the Poisson distribution):
            plt.scatter(country_total_count,country_p_KS_zeroparam,
                        marker='o', edgecolors='r', s=10**2, facecolors='none')
            # Plot the probabilities of the best phi_i models:
            plt.scatter(country_total_count,country_p_cluster,
                        marker='x', facecolors='g', edgecolors='g', s=16**2)
            (xlim1,xlim2) = plt.xlim(1.e3,max_N_tot_plot)
            plt.ticklabel_format(style='sci', axis='y')
            plt.xscale('log')
            plt.xlabel(r'$N_i$')
            plt.ylabel(r'$P$')
            plt.tight_layout()
            plt.savefig('probability_vs_N_'+seq_tuple[0]+'.eps')
            plt.close()

            ## === phi_i vs N_i  ====
            plt.figure()
            #plt.plot(country_total_count,country_cluster,marker='x',c='k')
            # log10 error bars
            c_cluster_plus = 10.0**(np.log10(country_cluster) + country_cluster_stderrlog10) - country_cluster
            c_cluster_minus = country_cluster - 10.0**(np.log10(country_cluster) - country_cluster_stderrlog10)
            plt.rcParams.update({'font.size': 18,
                                 'font.family': 'serif'})
            plt.errorbar(country_total_count,country_cluster,
                         yerr=[c_cluster_minus,c_cluster_plus],
                         fmt='xk', markersize=16,
                         elinewidth=1.0, capsize=3.0
                         )
            (xlim1,xlim2) = plt.xlim(1.e3,max_N_tot_plot)
            if(seq_tuple[0] == "full"):
                plt.ylim(0.1,max_cluster_plot)
            else:
                plt.ylim(0.01,1.e3)
            plt.text(1.5e3,3e2, plot_label)
            # Theil-Sen fit
            x_fit = np.array([xlim1,xlim2])
            y_fit_log10 = pzeropoint + pslope * np.log10(x_fit)
            y_fit = 10.0**y_fit_log10
            print('x_fit ... = ',x_fit, y_fit_log10, y_fit)
            plt.plot(x_fit,y_fit,'-',c='g',linewidth=1.0)

            matplotlib.ticker.LogFormatterExponent()
            #plt.ticklabel_format(style='sci', axis='x', scilimits=(-1,4))
            #plt.ticklabel_format(style='plain', axis='y')
            plt.xscale('log')
            plt.yscale('log')
            plt.xlabel(r'$N_i$')
            phi_label='$\phi_i'
            if(seq_tuple[0] == "full"):
                phi_label += '$'
            else:
                phi_label += '^{'+np.str(seq_tuple[1])+'}$'
            plt.ylabel(phi_label)
            plt.tight_layout()
            plt.savefig('phi_N_'+seq_tuple[0]+'.eps')
            plt.close()


            ## === psi_i vs N_i (normed relation) ====
            plt.figure()
            # log10 error bars
            country_cluster_psi = country_cluster / np.sqrt(country_total_count)
            c_cluster_plus = 10.0**(np.log10(country_cluster_psi) + country_cluster_stderrlog10) - country_cluster_psi
            c_cluster_minus = country_cluster_psi - 10.0**(np.log10(country_cluster_psi) - country_cluster_stderrlog10)
            plt.rcParams.update({'font.size': 18,
                                 'font.family': 'serif'})
            plt.errorbar(country_total_count,country_cluster_psi,
                         yerr=[c_cluster_minus,c_cluster_plus],
                         fmt='xk', markersize=16,
                         elinewidth=1.0, capsize=3.0
                         )
            (xlim1,xlim2) = plt.xlim(1.e3,max_N_tot_plot)
            #plt.ylim(0.1,1.3e3)
            if(seq_tuple[0] == "full"):
                plt.ylim(1.e-3,1.0e1)
            else:
                plt.ylim(1.e-4,1.0e1)

            plt.text(1.5e3,3.0, plot_label)

            plt.xscale('log')
            plt.yscale('log')
            #No effect for log: https://matplotlib.org/3.2.2/api/ticker_api.html#matplotlib.ticker.LogFormatterSciNotation
            #plt.ticklabel_format(style='sci', axis='x', scilimits=(-1,4))
            #plt.ticklabel_format(style='plain', axis='y')
            plt.xlabel(r'$N_i$')
            plt.ylabel(r'$\psi_i$')
            plt.tight_layout()
            plt.savefig('psi_N_'+seq_tuple[0]+'.eps')
            plt.close()

            n_i_subseq_mean = np.array([],dtype=int)
            for Noft_valid_subseq in country_Noft:
                n_i_subseq_mean = np.append(n_i_subseq_mean,
                                            np.mean(Noft_valid_subseq))


            with open(output_phi_N_filename,"w+") as phi_N_file:
                if(seq_tuple[0] == "full"):
                    phi_N_file.write(
                        "# Analysis of full SARS-CoV-2 daily infection count sequences\n")
                else:
                    phi_N_file.write(
                        "# Analysis of "+np.str(seq_tuple[1])+"-day SARS-CoV-2 daily infection count sequences\n")
                phi_N_file.write(
                    "# Column 1: XX   [%2s]  country ISO 3166-1 alpha-2 code (no territorial jurisdiction claims)\n")
                phi_N_file.write(
                    "# Column 2: N_total  [%d]  total number of infections ignoring threshold\n")
                if(seq_tuple[0] == "full"):
                    phi_N_file.write(
                        "# Column 3: N_total  [%d]  total number of infections of full subsequence\n")
                else:
                    phi_N_file.write(
                        "# Column 3: N_total  [%d]  total number of infections of "+np.str(seq_tuple[1])+"-day subsequence\n")
                phi_N_file.write(
                    "# Column 4: n_i_mean  [%f]  mean number of daily infections\n")
                phi_N_file.write(
                    "# Column 5: p_KS_0    [%f]  KS probability of consistency with Poisson distribution\n")
                phi_N_file.write(
                    "# Column 6: p_cluster [%f]  given the optimal phi_i value, KS probability of consistency\n")
                phi_N_file.write(
                    "# Column 7: cluster   [%f]  optimal phi_i value\n")
                phi_N_file.write(
                    "# Column 8: stderrlog10cluster   [%f]  log10 standard error in phi_i (bootstrap)\n")
                if(seq_tuple[0] == "full"):                
                    phi_N_file.write(
                    "# Column 9: p_cluster_logmedian [%f]  same as col 6, but log median model\n")
                    phi_N_file.write(
                        "# Column 10: cluster_logmedian   [%f]  same as col 7, but log median model\n")
                    phi_N_file.write(
                        "# Column 11: stderrlog10cluster_logmedian  [%f]  same as col 8, but log median model\n")
                    phi_N_file.write(
                        "# Column 12: p_cluster_negbin [%f]  given the optimal omega value, KS probability of consistency\n")
                    phi_N_file.write(
                        "# Column 13: cluster_negbin   [%f]  optimal omega value\n")
                    phi_N_file.write(
                        "# Column 14: stderrlog10cluster_negbin  [%f]  log10 standard error in omega (bootstrap)\n")
                    phi_N_file.write(
                        "# Column 15: start_date  [yyyy-mm-dd]  date at which sequence starts\n")
                    phi_N_file.write(
                        "# Column 16: dip  [%f] dip (fraction on a 7-day time scale\n")
                    phi_N_file.write(
                        "# Column 17: p_cluster_bidays [%f]  given the optimal omega value, KS probability of consistency\n")
                    phi_N_file.write(
                        "# Column 18: cluster_bidays   [%f]  optimal omega value\n")
                    phi_N_file.write(
                        "# Column 19: stderrlog10cluster_bidays  [%f]  log10 standard error in omega (bootstrap)\n")
                    phi_N_file.write(
                        "# Column 20: p_cluster_tridays [%f]  given the optimal omega value, KS probability of consistency\n")
                    phi_N_file.write(
                        "# Column 21: cluster_tridays   [%f]  optimal omega value\n")
                    phi_N_file.write(
                        "# Column 22: stderrlog10cluster_tridays  [%f]  log10 standard error in omega (bootstrap)\n")
                else:
                    phi_N_file.write(
                        "# Column 9: start_date  [yyyy-mm-dd]  date at which sequence starts\n")

                if(seq_tuple[0] == "full"):                    
                    for ith_tuple in zip(country_valid[index_lowest_phi_alph],
                                         country_total_count[index_lowest_phi_alph],
                                         country_total_count_subseq[index_lowest_phi_alph],
                                         n_i_subseq_mean[index_lowest_phi_alph],
                                         country_p_KS_zeroparam[index_lowest_phi_alph],
                                         country_p_cluster[index_lowest_phi_alph],
                                         country_cluster[index_lowest_phi_alph],
                                         country_cluster_stderrlog10[index_lowest_phi_alph],
                                         country_p_cluster_logmedian[index_lowest_phi_alph],
                                         country_cluster_logmedian[index_lowest_phi_alph],
                                         country_cluster_stderrlog10_logmedian[index_lowest_phi_alph],
                                         country_p_cluster_negbin[index_lowest_phi_alph],
                                         country_cluster_negbin[index_lowest_phi_alph],
                                         country_cluster_stderrlog10_negbin[index_lowest_phi_alph],
                                         country_start_date[index_lowest_phi_alph],
                                         country_dip[index_lowest_phi_alph],
                                         country_p_cluster_bidays[index_lowest_phi_alph],
                                         country_cluster_bidays[index_lowest_phi_alph],
                                         country_cluster_stderrlog10_bidays[index_lowest_phi_alph],
                                         country_p_cluster_tridays[index_lowest_phi_alph],
                                         country_cluster_tridays[index_lowest_phi_alph],
                                         country_cluster_stderrlog10_tridays[index_lowest_phi_alph]):
                        phi_N_file.write('%2s %10d %10d %9.1f  %7.3f %7.3f %8.3f %7.3f  %7.3f %8.3f %7.3f   %7.3f %9.5f %7.3f %12s %6.2f   %7.3f %8.3f %7.3f  %7.3f %8.3f %7.3f\n' % ith_tuple)
                        print('ith_tuple = ',ith_tuple)
                else:
                    for ith_tuple in zip(country_valid[index_lowest_phi_alph],
                                         country_total_count[index_lowest_phi_alph],
                                         country_total_count_subseq[index_lowest_phi_alph],
                                         n_i_subseq_mean[index_lowest_phi_alph],
                                         country_p_KS_zeroparam[index_lowest_phi_alph],
                                         country_p_cluster[index_lowest_phi_alph],
                                         country_cluster[index_lowest_phi_alph],
                                         country_cluster_stderrlog10[index_lowest_phi_alph],
                                         country_start_date[index_lowest_phi_alph]):
                        phi_N_file.write('%2s %10d %10d %9.1f  %7.3f %7.3f %8.3f %7.3f  %12s\n' % ith_tuple)
                        print('ith_tuple = ',ith_tuple)
                        
                phi_N_file.close()


            # Find and output countries with the 10 lowest phi values
            #index_lowest_phi_alph = np.argsort(country_cluster) # get indices for sorting
            print('index_lowest_phi_alph[0:10] =',index_lowest_phi_alph[0:10])
            str_lowest_phi_IDs = ('N_lowest_phi_countries = ')
            str_lowest_phi_below_pointone_IDs = ('N_lowest_phi_below_pointone_countries = ')
            str_lowest_phi_below_half_IDs = ('N_lowest_phi_below_half_countries = ')
            str_lowest_phi_below_three_IDs = ('N_lowest_phi_below_three_countries = ')

            below_pointone_string_started = False
            below_half_string_started = False
            below_three_string_started = False
            str_lowest_phi_IDs += country_valid[index_lowest_phi_alph[0]]
            if(country_cluster[index_lowest_phi_alph[0]] < 0.1):
                str_lowest_phi_below_pointone_IDs += country_valid[index_lowest_phi_alph[0]]
                below_pointone_string_started = True
            elif(country_cluster[index_lowest_phi_alph[0]] < 0.5):
                str_lowest_phi_below_half_IDs += country_valid[index_lowest_phi_alph[0]]
                below_half_string_started = True
            elif(country_cluster[index_lowest_phi_alph[0]] < 3.0):
                str_lowest_phi_below_three_IDs += country_valid[index_lowest_phi_alph[0]]
                below_three_string_started = True

            for i_country in np.arange(1,N_lowest_phi):
                if(i_country < N_countries_OK):
                    # temporarily use semicolons to store spaces safely
                    str_lowest_phi_IDs += ',;' + country_valid[index_lowest_phi_alph[i_country]]
                    if(country_cluster[index_lowest_phi_alph[i_country]] < 0.1):
                        if(below_pointone_string_started):
                            str_lowest_phi_below_pointone_IDs += ',;' + country_valid[index_lowest_phi_alph[i_country]]
                        else:
                            str_lowest_phi_below_pointone_IDs += country_valid[index_lowest_phi_alph[i_country]]
                            below_pointone_string_started = True
                    elif(country_cluster[index_lowest_phi_alph[i_country]] < 0.5):
                        if(below_half_string_started):
                            str_lowest_phi_below_half_IDs += ',;' + country_valid[index_lowest_phi_alph[i_country]]
                        else:
                            str_lowest_phi_below_half_IDs += country_valid[index_lowest_phi_alph[i_country]]
                            below_half_string_started = True
                    elif(country_cluster[index_lowest_phi_alph[i_country]] < 3.0):
                        if(below_three_string_started):
                            str_lowest_phi_below_three_IDs += ',;' + country_valid[index_lowest_phi_alph[i_country]]
                        else:
                            str_lowest_phi_below_three_IDs += country_valid[index_lowest_phi_alph[i_country]]
                            below_three_string_started = True

            # Find and output countries with the 10 lowest psi values
            index_lowest_psi = np.argsort(country_cluster_psi) # get indices for sorting
            print('index_lowest_psi[0:10] =',index_lowest_psi[0:10])
            str_lowest_psi_IDs = ('N_lowest_psi_countries =')

            str_lowest_psi_IDs += country_valid[index_lowest_psi[0]]
            for i_country in np.arange(1,N_lowest_psi):
                if(i_country < N_countries_OK):
                    # temporarily use semicolons to store spaces safely
                    str_lowest_psi_IDs += ',;' + country_valid[index_lowest_psi[i_country]]

            #Noft_valid[i_start_best_substr+28]
            print('country_Noft[1]= ',country_Noft[1])
            print('country_Noft[0]= ',country_Noft[0])
            print(type(index_lowest_phi_alph))
            print(type(index_lowest_phi_alph[0:10]))
            print(type([country_Noft[index_lowest_phi_alph[0]],country_Noft[index_lowest_phi_alph[0]]]))
            #print('country_Noft[index_lowest_phi_alph[0:10]]= ',country_Noft[np.array(index_lowest_phi_alph[0:10],dtype=int)])

            ## plots for countries with the lowest phi values
            # arrays or lists to feed to plotter:
            c_plot_valid = np.array([],np.str)
            c_plot_Noft = []
            c_plot_start_date = []
            c_plot_cluster = []

            j_found = 0
            j_tried = 0
            while(j_found < N_plot_lowest_phi and j_tried < N_countries_OK):
                if(country_total_count[index_lowest_phi_alph[j_tried]] > N_plot_N_min_lowest_phi):
                    c_plot_valid = np.append(c_plot_valid, country_valid[index_lowest_phi_alph[j_tried]])
                    c_plot_Noft.append(country_Noft[index_lowest_phi_alph[j_tried]])
                    c_plot_start_date.append(country_start_date[index_lowest_phi_alph[j_tried]])
                    c_plot_cluster.append(country_cluster[index_lowest_phi_alph[j_tried]])
                    j_found += 1
                j_tried += 1

            plot_countries(c_plot_valid,
                           c_plot_Noft,
                           c_plot_start_date,
                           c_plot_cluster,
                           seq_tuple[0])

            # N_plot_phi (by default 6) plots joined together
            plot_countries(c_plot_valid,
                           c_plot_Noft,
                           c_plot_start_date,
                           c_plot_cluster,
                           'multi_'+seq_tuple[0],
                           join_plots=True)

            with open(output_countries_filename,"a") as countries_file:
                for c_tuple in zip(c_plot_valid,'abcdefghij'):
                    countries_file.write('countries_daily_low_'+c_tuple[1]+'_'+seq_tuple[0]+' = countries_daily_'+c_tuple[0]+seq_tuple[0]+'\n')
            countries_file.close()

            ## plots for countries with the median phi values
            # arrays or lists to feed to plotter:
            c_plot_valid = np.array([],dtype=np.str)
            c_plot_Noft = []
            c_plot_start_date = []
            c_plot_cluster = []

            if(N_countries_OK == 2*np.int(np.floor(N_countries_OK/2))):
                # even case
                j0 = np.int(N_countries_OK/2) - (np.int(np.floor(N_plot_median_phi/2))-1)
                j1 = j0 + np.amin([N_plot_median_phi, N_countries_OK])
            else:
                # odd case (in principle could make this different to the even case)
                j0 = np.int(N_countries_OK/2) - (np.int(np.floor(N_plot_median_phi/2))-1)
                j1 = j0 + np.amin([N_plot_median_phi, N_countries_OK])

            for j in np.arange(j0,j1):
                c_plot_valid = np.append(c_plot_valid, country_valid[index_lowest_phi_alph[j]])
                c_plot_Noft.append(country_Noft[index_lowest_phi_alph[j]])
                c_plot_start_date.append(country_start_date[index_lowest_phi_alph[j]])
                c_plot_cluster.append(country_cluster[index_lowest_phi_alph[j]])

                # Add to the joint plot if this is the 28- or 7-day subsequence
                if(seq_tuple[0] == "a" or seq_tuple[0] == "c"):
                    c_med_plot_valid = np.append(c_med_plot_valid, country_valid[index_lowest_phi_alph[j]])
                    c_med_plot_Noft.append(country_Noft[index_lowest_phi_alph[j]])
                    c_med_plot_start_date.append(country_start_date[index_lowest_phi_alph[j]])
                    c_med_plot_cluster.append(country_cluster[index_lowest_phi_alph[j]])

            plot_countries(c_plot_valid,
                           c_plot_Noft,
                           c_plot_start_date,
                           c_plot_cluster,
                           seq_tuple[0])

            with open(output_countries_filename,"a") as countries_file:
                for c_tuple in zip(c_plot_valid,'abcdefghij'):
                    countries_file.write('countries_daily_med_'+c_tuple[1]+'_'+seq_tuple[0]+' = countries_daily_'+c_tuple[0]+seq_tuple[0]+'\n')

            with open(output_countries_filename,"a") as countries_file:
                countries_file.write(str_lowest_phi_IDs+'\n')
                countries_file.write(str_lowest_psi_IDs+'\n')
                countries_file.write(str_lowest_phi_below_pointone_IDs+'\n')
                countries_file.write(str_lowest_phi_below_half_IDs+'\n')
                countries_file.write(str_lowest_phi_below_three_IDs+'\n')
            countries_file.close()

            print("reminder: N_countries_OK = ",N_countries_OK)

            if(seq_tuple[0] == "full"):                    
                # Akaike + Bayesian Information Criteria
                # fiducial model
                AIC_fid = 2.0*N_countries_OK
                BIC_fid = np.log(countries_total_days) * N_countries_OK
                # logmedian
                AIC_logmedian = 2.0*N_countries_OK
                BIC_logmedian = np.log(countries_total_days) * N_countries_OK
                # negbin
                AIC_negbin = 2.0*N_countries_OK
                BIC_negbin = np.log(countries_total_days) * N_countries_OK
                # bidays
                AIC_bidays = 2.0*N_countries_OK_bidays
                BIC_bidays = np.log(countries_total_bidays) * N_countries_OK_bidays
                # tridays
                AIC_tridays = 2.0*N_countries_OK_tridays
                BIC_tridays = np.log(countries_total_tridays) * N_countries_OK_tridays

                AIC_fid += np.sum(-2.0*np.log(country_p_cluster))
                BIC_fid += np.sum(-2.0*np.log(country_p_cluster))
                AIC_logmedian += np.sum(-2.0*np.log(country_p_cluster_logmedian))
                BIC_logmedian += np.sum(-2.0*np.log(country_p_cluster_logmedian))
                AIC_negbin += np.sum(-2.0*np.log(country_p_cluster_negbin))
                BIC_negbin += np.sum(-2.0*np.log(country_p_cluster_negbin))

                for c in country_p_cluster_bidays:
                    if(c > dummy_value):
                        AIC_bidays += -2.0*np.log(c)
                        BIC_bidays += -2.0*np.log(c)
                for c in country_p_cluster_tridays:
                    if(c > dummy_value):
                        AIC_tridays += -2.0*np.log(c)
                        BIC_tridays += -2.0*np.log(c)
                    
                with open(output_AIC_BIC_filename,"w+") as AIC_BIC_file:
                    AIC_BIC_file.write("# Akaike and Bayesian Information Criteria of full SARS-CoV-2 daily infection counts\n")
                    AIC_BIC_file.write("# Columns 1,2: AIC and BIC for default model\n")
                    AIC_BIC_file.write("# Columns 3,4: AIC and BIC for logmedian model\n")
                    AIC_BIC_file.write("# Columns 5,6: AIC and BIC for negative binomial model\n")
                    AIC_BIC_file.write("# Columns 7,8: AIC and BIC for 2-day grouping\n")
                    AIC_BIC_file.write("# Columns 9,10: AIC and BIC for 3-day grouping\n")
                    AIC_BIC_file.write("%7.3f %7.3f  %7.3f %7.3f  %7.3f %7.3f  %7.3f %7.3f  %7.3f %7.3f\n" %(AIC_fid, BIC_fid,
                                                                                                             AIC_logmedian, BIC_logmedian,
                                                                                                             AIC_negbin, BIC_negbin,
                                                                                                             AIC_bidays, BIC_bidays,
                                                                                                             AIC_tridays, BIC_tridays))
                AIC_BIC_file.close
                

            ## Weekly dip histogram
            if(seq_tuple[0] == "full"):            
                plt.figure()
                print('type(country_dip),country_dip = ',type(country_dip),country_dip)
                plt.hist(country_dip, bins=20, color=['#aaaaaa'])
                plt.xlabel(r'$w$')
                plt.ylabel(r'$N$')
                plt.tight_layout()
                plt.savefig('weeklydip_'+seq_tuple[0]+'.eps')
                plt.close()

                # print a few statistics
                dip_mean = np.mean(country_dip)
                dip_std = np.std(country_dip)
                dip_serr_mean = dip_std/np.sqrt(N_countries_OK)
                dip_median = np.median(country_dip)
                dip_IQR = np.percentile(country_dip,75.0) - np.percentile(country_dip,25.0)
                dip_shapirowilk_r, dip_shapirowilk_p = stats.shapiro(country_dip)
                with open(output_weeklydip_filename,"w+") as wdip_file:
                    wdip_file.write("dip_mean = %.3f\n" % dip_mean)
                    wdip_file.write("dip_serr_mean = %.3f\n" % dip_serr_mean)
                    wdip_file.write("dip_std = %.3f\n" % dip_std)
                    wdip_file.write("dip_median = %.3f\n" % dip_median)
                    wdip_file.write("dip_IQR = %.3f\n" % dip_IQR)
                    wdip_file.write("dip_shapirowilk_r = %.3f\n" % dip_shapirowilk_r)
                    wdip_file.write("dip_shapirowilk_p = %8.3g\n" % dip_shapirowilk_p)
                wdip_file.close
                
            with open(output_countries_filename,"a") as countries_file:
                countries_file.write("N_countries_OK = %d\n" % N_countries_OK)
            countries_file.close

        #end:   if(seq_tuple[1] > 0):
    #end: for seq in sequence_types:

    plot_countries(c_med_plot_valid,
                   c_med_plot_Noft,
                   c_med_plot_start_date,
                   c_med_plot_cluster,
                   'median_cases',
                   join_plots=True)

    sys.exit(pass_val)

#end: __main__
