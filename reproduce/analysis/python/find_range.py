#!/usr/bin/env python

# find_range - find a subsequence of SARS-CoV-2 daily counts given a threshold
# (C) 2020 Boud Roukema GPL-3+
#
# This script is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This script is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.

import numpy as np
from numpy.random import default_rng
import sys

def generate_Noft_test(valid,definite,threshold_start,threshold_stop,min_days):

    """
    Generate a simple dummy *Noft* time sequence.
    If *definite* is True, then:
      if *valid* is True, then generate a time sequence that is above
      *threshold_start* for at least *min_days*; otherwise generate
      an invalid sequence.
    otherwise: generate a sequence that might or might not be
    valid, depending on the Poisson error.
    """

    rng = default_rng() # this should work for arbitrary seeds

    # iniitialise the output array
    Noft=np.zeros(min_days + 10)
    if(definite):
        # add the threshold value + 1
        Noft[5:5+min_days] = threshold_start + 1
        if(valid):
            # add non-negative noise to make things slightly more realistic
            Noft[5:5+min_days] += rng.poisson(threshold_start,min_days)
        else:
            # invalidate two days; this assumes that min_days is big enough
            Noft[5+int(min_days/2):5+int(min_days/2)+2] = threshold_stop-1
    else:
        Noft[5] = threshold_start
        # One standard deviation of Poisson noise
        one_sigma = np.sqrt(threshold_stop)
        # Generate the cutoff value plus one std dev of noise
        Noft[6:5+min_days] = one_sigma + rng.poisson(threshold_stop,min_days-1)
    return Noft


def find_high_enough_subrange(Noft,threshold_start,threshold_stop,
                              min_days_init,min_days,stop_days):
    """
    Given an array *Noft*, return a list of integer indices
    *t_valid* for the first continuous period of time
    starting at the first day when a value is above
    *threshold_start*, and continuing until the value drops
    below *threshold_stop* for at least *stop_days* consecutive days.
    A period shorter than *min_days* is rejected as invalid.
    An initial period of *min_days_init* once the threshold
    has been obtained is accepted without checking for a drop.
    The indicator *OK* is returned as either True or False
    depending on whether a valid range was found or not.

    returns: OK, i_valid
    """

    N_Noft = np.array(Noft).size # assume 1D array

    debug = False

    i_above_threshold = np.array(np.where(Noft >= threshold_start))[0]
    N_above = (i_above_threshold.shape)[0]
    if(debug):
        print('N_above = ',N_above,' N_Noft = ',N_Noft)

    if(N_above < min_days): # finished if this is already too short
        OK = False
        i_valid = np.array([])
    else:
        i_just_before = (i_above_threshold)[0]-1 # just before the selected area starts

        # Add a min_days initial period to get sufficiently above the
        # initial threshold, so that noise fluctuations do not drop below
        # it:
        if(i_just_before+min_days < N_Noft):
            i_just_before += min_days_init

        # Create a copy of the full input array in new memory location
        N_copy = 1*Noft
        # Set artificial values above threshold_start at the beginning of
        # the array
        N_copy[0:(i_just_before+1)] = (threshold_start + 1)

        # Find the first series of stop_days invalid values just after the
        # first consecutive subarray
        N_invalid = 0
        i_anti_inf = 0 # anti-infinite-loop check
        N_inf = 10958 # assume less than 30 years of data
        while (N_invalid < stop_days and i_anti_inf < N_inf):
            i_invalid = (np.where(N_copy < threshold_stop))[0]
            if(debug):
                print('i_invalid = ', i_invalid)
            i_anti_inf += 1
            if(isinstance(i_invalid,tuple)):
                i_invalid=np.array(i_invalid)
            if((i_invalid.shape)[0] >= stop_days):
                i_just_after = i_invalid[0]
                if(debug):
                    print('i_just_after = ', i_just_after)
                # Are there stop_days consecutive invalid values?
                i_consecutive = (np.where(
                    N_copy[i_just_after:i_just_after+stop_days] < threshold_stop))[0]
                if(isinstance(i_consecutive,tuple)):
                    i_consecutive=np.array(i_consecutive)
                if(debug):
                    print('N_copy = ',N_copy)
                    print('N_copy[i_just_after:i_just_after+stop_days] = ',N_copy[i_just_after:i_just_after+stop_days])
                    print('i_consecutive = ', i_consecutive)

                N_invalid = (i_consecutive.shape)[0]
                if(debug):
                    print('N_invalid = ', N_invalid)
                # If this is too few invalid values, then validate the
                # first so that it is no longer a candidate for a
                # "stopping" day.
                if(N_invalid < stop_days):
                    N_copy[i_just_after] = threshold_stop +1
                if(debug):
                    print('validated N_copy = ', N_copy)
                # Now let the 'while' loop continue.
            else:
                i_just_after = N_Noft # indices are from 0 to N_Noft-1
                if(debug):
                    print('i_just_after = ',i_just_after)
                break

        i_valid = np.arange(i_just_before+1, i_just_after) # excludes i_just_after
        N_above = (i_valid.shape)[0]
        if(N_above < min_days):
            OK = False
            i_valid = np.array([])
        else:
            OK = True

    return (OK, i_valid)

# end def find_high_enough_subrange(...)


if __name__ == '__main__':

    N_thresh_start = 30
    N_thresh_stop = 10
    stop_days = 2
    pass_val = 0 # pass_val value of test: 0 = OK, 1 = fail

    print("N_thresh_start = ",N_thresh_start)
    print("N_thresh_stop = ",N_thresh_stop)
    print("stop_days = ",stop_days)

    print("Check valid array:")
    Noft = generate_Noft_test(True, True, N_thresh_start, N_thresh_stop, 3)
    print("Noft = ",Noft)
    OK, i_valid = find_high_enough_subrange(Noft, N_thresh_start, N_thresh_stop,
                                            0, 3, stop_days)
    print("OK = ",OK)
    if(OK):
        print("Noft[i_valid] = ",Noft[i_valid])
    else:
        pass_val += 1 # non-zero value for false positive
    print("")

    print("Check invalid array:")
    Noft = generate_Noft_test(False, True, N_thresh_start, N_thresh_stop, 3)
    print("Noft = ",Noft)
    OK, i_valid = find_high_enough_subrange(Noft, N_thresh_start, N_thresh_stop,
                                            0, 3, stop_days)
    print("OK = ",OK)
    if(OK):
        print("Noft(i_valid) = ",Noft[i_valid])
        pass_val += 2 # non-zero value for false negative
    print("")

    print("Check possibly valid array:")
    Noft = generate_Noft_test(False, False, N_thresh_start, N_thresh_stop, 3)
    print("Noft = ",Noft)
    OK, i_valid = find_high_enough_subrange(Noft, N_thresh_start, N_thresh_stop,
                                            0, 3, stop_days)
    print("OK = ",OK)
    if(OK):
        print("Noft(i_valid) = ",Noft[i_valid])
    print("")

    print("pass_val value = ",pass_val)
    sys.exit(pass_val)

#end __main__
