#!/usr/bin/env python

# get_noise - analyse noise in SARS-CoV-2 daily counts
# (C) 2020 Boud Roukema GPL-3+
#
# This script is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This script is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.

import sys
import os
import numpy as np
from numpy.random import SeedSequence, default_rng
from scipy import stats

import multiprocessing as mp

def generate_Noft_positive(N):

    debug = True

    seeds = SeedSequence(2039)
    seed = seeds.spawn(2) # one seed is ignored
    #rng = default_rng(seed[0])
    rng = default_rng()

    # Use a concave-down parabola that is positive on (0 < x < N) as a simple
    # test case.

    # First generate the smooth curve with only rounding noise:
    x = np.arange(N)
    Noft_smooth = N*N/2 - 0.5*(x - N/2)**2
    if(debug):
        print('Noft_smooth = ',Noft_smooth)

    Noft_smooth = rng.poisson(Noft_smooth)
    Noft_smooth = np.array(Noft_smooth, dtype=int) # convert to int
    if(debug):
        print('Noft_smooth = ',Noft_smooth)

    return Noft_smooth


def stat_poisson_cdf_function(i_t, Noft_normed, Noft_model_normed):
    p = stats.poisson.cdf(Noft_normed[i_t],
                          Noft_model_normed[i_t])
    return p


def get_noise_statistics(Noft,fixed_rng_seed):
    # Given a time sequence of positive only values that are not too low to
    # be unreasonably noisy, calculate their individual Poisson cdf
    # (cumulative distribution function) probabilities and see how
    # these relate to an expected uniform distribution.

    debug = False
    debug2 = False

    n = os.environ.get('N_MEDIAN_MODEL')
    if(n):
        N_median_model = np.int(n)
    else:
        N_median_model = 4

    n = os.environ.get('N_STDERR_CLUSTER')
    if(n):
        N_stderr_cluster = np.int(n)
    else:
        N_stderr_cluster = 10

    n = os.environ.get('DELTA_LOG10_CLUSTER')
    if(n):
        delta_log10_cluster = np.float(n)
    else:
        delta_log10_cluster = 0.5

    n = os.environ.get('DELTA_LOG10_REFINE')
    if(n):
        delta_log10_refine = np.float(n)
    else:
        delta_log10_refine = 0.3

    ## This is a development option for fast, inaccurate calculations,
    ## for checking the software validity.
    n = os.environ.get('ENABLE_DEV_OVERRIDE')
    if(n):
        delta_log10_cluster = 0.5
        delta_log10_refine = 0.5


    refine = True

    seeds = SeedSequence(4639)
    seed = seeds.spawn(2) # one seed is ignored
    if(fixed_rng_seed):
        #print('Fixed rng seed.')
        rng = default_rng(seed[0])
    else:
        #print('Numpy automatic rng seed.')
        rng = default_rng()


    N_Noft = Noft.size # assume numpy array
    if(N_Noft < 5):
        valid = False
    else:
        if(4 == N_median_model):
            Noft_neighbours = np.array([Noft[0:N_Noft-4],
                                        Noft[1:N_Noft-3],
                                        Noft[3:N_Noft-1],
                                        Noft[4:N_Noft]
            ])
        else:
            raise RuntimeError('get_noise.py: get_noise_statistics: N_median_model = ',
                               N_median_model,' is not programmed.')
        if(debug):
            print(Noft_neighbours)
        # simple model of the middle value as the median of the neighbours
        Noft_model_middle = np.median(Noft_neighbours, axis=0)
        if(debug):
            print('Noft_model_middle = \n',Noft_model_middle)
        Noft_model = np.zeros(N_Noft)
        ## Copy the middle model into the middle of Noft_model
        Noft_model[2:N_Noft-2] = Noft_model_middle
        ## Pad the ends with constants
        Noft_model[0:2] = Noft_model[2]
        Noft_model[N_Noft-2:N_Noft] = Noft_model[N_Noft-3]
        if(debug):
            print('Noft_model = \n',Noft_model)

        # Calculate probabilities of Noft given the Noft_model
        cluster = 10.0**np.arange(-1,3,delta_log10_cluster,dtype=np.float)

        # First check the probability of a zero-parameter Poisson model.
        # Convert the individual Poisson realisations with differing mean values
        # to what should be a uniform distribution in cumulative probability:
        prob_zeroparam = np.zeros(N_Noft)
        for i_t in np.arange(N_Noft):
            prob_zeroparam[i_t] = stats.poisson.cdf(Noft[i_t], Noft_model[i_t])
        # Kolmogorov-Smirnov test: how probable is this a realisation of
        # a uniform distribution (on (0,1))?
        D_KS_zeroparam, p_KS_zeroparam = stats.kstest(prob_zeroparam,'uniform',N=N_Noft)

        N_cluster = cluster.size
        prob = np.zeros((N_cluster,N_Noft))
        p_KS = np.zeros(N_cluster)
        p_max = -9.9
        for i_cluster in np.arange(N_cluster):
            # Convert individual Poisson realisations with differing mean values
            # to what should be a uniform distribution in cumulative probability:
            #pool = mp.Pool(6)
            Noft_normed = Noft/cluster[i_cluster]
            Noft_model_normed = Noft_model/cluster[i_cluster]
            prob[i_cluster,:] = stats.poisson.cdf(Noft_normed, Noft_model_normed)

            # Kolmogorov-Smirnov test: how probable is this a realisation of
            # a uniform distribution (on (0,1))?
            D_KS, p_KS[i_cluster] = stats.kstest(prob[i_cluster,:],'uniform',N=N_Noft)
            if(debug):
                print('cluster, D_KS, p_KS = ',cluster[i_cluster],D_KS, p_KS[i_cluster])
            # store a more likely cluster value if we've found a better one
            if(p_KS[i_cluster] > p_max):
                cluster_best = cluster[i_cluster]
                p_max = p_KS[i_cluster]

        if(refine):
            # Inelegant refinement: TODO - do this more elegantly
            c_best_log10 = np.log10(cluster_best)
            cluster_fine = 10.0**np.arange(c_best_log10-3*delta_log10_cluster,
                                           c_best_log10+3*delta_log10_cluster,
                                           delta_log10_refine*delta_log10_cluster,dtype=np.float)
            N_cluster_fine = cluster_fine.size
            prob_fine = np.zeros((N_cluster_fine,N_Noft))
            p_KS_fine = np.zeros(N_cluster_fine)
            p_max_fine = -9.9
            for i_cluster in np.arange(N_cluster_fine):
                # Convert individual Poisson realisations with differing mean values
                # to what should be a uniform distribution in cumulative probability:
                Noft_normed = Noft/cluster_fine[i_cluster]
                Noft_model_normed = Noft_model/cluster_fine[i_cluster]
                prob_fine[i_cluster,:] = stats.poisson.cdf(Noft_normed, Noft_model_normed)

                # Kolmogorov-Smirnov test: how probable is this a realisation of
                # a uniform distribution (on (0,1))?
                D_KS_fine, p_KS_fine[i_cluster] = stats.kstest(prob_fine[i_cluster,:],'uniform',N=N_Noft)
                if(debug):
                    print('fine: cluster, D_KS, p_KS = ',cluster_fine[i_cluster],D_KS_fine, p_KS_fine[i_cluster])
                # store a more likely cluster value if we've found a better one
                if(p_KS_fine[i_cluster] > p_max_fine):
                    cluster_best_fine = cluster_fine[i_cluster]
                    p_max_fine = p_KS_fine[i_cluster]


            cluster_best = cluster_best_fine
            p_max = p_max_fine

        # Estimate the standard error in cluster_best by making
        # Poisson simulations of Noft/cluster_best.
        cluster_best_sim = np.zeros(N_stderr_cluster)
        for i_sim in np.arange(N_stderr_cluster):
            # Realisation assuming that Noft_model and cluster_best are true
            Noft_sim = cluster_best * (rng.poisson(Noft_model/cluster_best))
            if(debug2):
                print('Noft_sim=',Noft_sim)
            prob_sim = np.zeros((N_cluster,N_Noft))
            p_KS_sim = np.zeros(N_cluster)
            p_max_sim = -9.9
            for i_cluster in np.arange(N_cluster):
                # Convert individual Poisson realisations with differing mean values
                # to what should be a uniform distribution in cumulative probability:
                Noft_sim_normed = Noft_sim/cluster[i_cluster]
                Noft_model_normed = Noft_model/cluster[i_cluster]
                prob_sim[i_cluster,:] = stats.poisson.cdf(Noft_sim_normed, Noft_model_normed)

                # Kolmogorov-Smirnov test: how probable is this a realisation of
                # a uniform distribution (on (0,1))?
                D_KS_sim, p_KS_sim[i_cluster] = stats.kstest(prob_sim[i_cluster,:],'uniform',N=N_Noft)
                if(debug2):
                    print('cluster, D_KS_sim, p_KS_sim = ',cluster[i_cluster],D_KS_sim, p_KS_sim[i_cluster])
                # store a more likely cluster value if we've found a better one
                if(p_KS_sim[i_cluster] > p_max_sim):
                    cluster_best_sim[i_sim] = cluster[i_cluster]
                    p_max_sim = p_KS_sim[i_cluster]

        #print('cluster_best_sim = ',cluster_best_sim)
        stderr_cluster = np.std(cluster_best_sim)
        stderrlog10_cluster = np.std(np.log10(cluster_best_sim))

        if(debug):
            print('prob = ',prob)

        valid = True

    return (valid, p_KS_zeroparam,
            cluster_best, p_max,
            stderr_cluster, stderrlog10_cluster)


def get_subseq_noise_statistics(Noft,N_days,fixed_rng_seed):
    # Given a time sequence of infection counts, find the consecutive subset
    # of length *N_days* that minimises the value of *cluster*.

    # Assumption: Noft is a numpy 1D array.

    debug = False

    N_Noft = Noft.size
    if(debug):
        print('get_subseq_noise_statistics: N_Noft = ',N_Noft,'N_days = ',N_days)
    if(N_Noft <= N_days):
        # Use only the original sequence if the number of days is too low.
        (valid, p_KS_zeroparam_subseq,
         cluster_best_subseq, p_max_best_subseq,
         stderr_cluster, stderrlog10_cluster) = get_noise_statistics(Noft,fixed_rng_seed)
        i_start_best_subseq = 0
    else:
        ## starting index of any subsequence to try
        i_start_range = np.arange(N_Noft-N_days+1)
        cluster_best_subseq = 9e9
        i_start_best_subseq = 0
        p_max_best_subseq = -9.9
        p_KS_zeroparam_subseq = -9.9
        for i_start in i_start_range:
            (valid, p_KS_zeroparam,
             cluster_best, p_max,
             stderr_cluster, stderrlog10_cluster) = get_noise_statistics(Noft[i_start:i_start+N_days],fixed_rng_seed)
            if(debug):
                print('i_start, cluster_best = ',i_start, cluster_best)
            if(valid and cluster_best < cluster_best_subseq):
                cluster_best_subseq = cluster_best
                i_start_best_subseq = i_start
                p_max_best_subseq = p_max
                p_KS_zeroparam_subseq = p_KS_zeroparam
                if(debug):
                    print('better: i_start_best_subseq, cluster_best_subseq = ',
                          i_start_best_subseq, cluster_best_subseq)

    return (valid, p_KS_zeroparam_subseq,
            cluster_best_subseq, p_max_best_subseq,
            stderrlog10_cluster,
            i_start_best_subseq)


if __name__ == '__main__':

    pass_val = 0 # test parameter 0 = pass, !0 = fail


    n = os.environ.get('FIXED_RNG_SEED')
    if(n):
        fixed_rng_seed = np.bool(n)
    else:
        fixed_rng_seed = False


    # Generate a test sequence of infection counts
    Noft = generate_Noft_positive(50)

    # Get some statistics to test if it's Poissonian
    (valid, p_KS_zeroparam,
     cluster, p_cluster,
     stderr_cluster, stderrlog10_cluster) = get_noise_statistics(Noft,fixed_rng_seed)
    if(valid):
        print('p_KS_zeroparam = ',p_KS_zeroparam)
        print('Best: cluster, p_cluster, stderr = ',
              cluster,p_cluster,stderr_cluster)
        print('Best log10: ',np.log10(cluster),' pm ',stderrlog10_cluster)
    else:
        print('Too few values')

    print('pass_val = ',pass_val)
    sys.exit(pass_val)

#end __main__
