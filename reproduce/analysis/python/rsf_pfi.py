#!/usr/bin/env python

# rsf_pfi - compare the RSF Press Freedom Index to SARS-CoV-2 clustering
# (C) 2021 Boud Roukema GPL-3+
#
# This script is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This script is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.

import sys
import os
import warnings

import numpy as np
from numpy.random import SeedSequence, default_rng
import matplotlib
import matplotlib.pyplot as plt
from scipy import stats

## Plot the RSF Press Freedom Index as a function of a psi_i or phi_i^{X}
## parameter; calculate and return the non-parametric correlation.

def do_rsf_plot(country_total_count,
                normalise,
                country_cluster,
                country_cluster_stderrlog10,
                pfi,
                x_axis_label,
                y_axis_label,
                graph_label,
                fig,ax,sublabel=''):
                        
    if(normalise):
        country_cluster_normed = country_cluster / np.sqrt(country_total_count)
    else:
        country_cluster_normed = country_cluster
    # log10 error bars
    c_cluster_plus = 10.0**(np.log10(country_cluster_normed) + country_cluster_stderrlog10) - country_cluster_normed
    c_cluster_minus = country_cluster_normed - 10.0**(np.log10(country_cluster_normed) - country_cluster_stderrlog10)

    #plt.figure()
    ax.errorbar(pfi,
                country_cluster_normed,
                yerr=[c_cluster_minus,c_cluster_plus],
                fmt='xk', markersize=8,
                elinewidth=0.5, capsize=1.5,capthick=0.5)
    #ylim1=3.e-3
    #ylim2=3.0e0
    xlim1=0.0
    xlim2=100.0
    #plt.xscale('log')
    ax.set_yscale('log')
    (ylim1,ylim2) = ax.get_ylim()

    y_text = ylim1*np.power((ylim2/ylim1),0.1)
    ax.text(90.0, y_text, sublabel)
    print('pfi_plot: y_text, sublabel = ', y_text,' ',sublabel)


    #plt.axis('square')
    ax.set_xlim(xlim1,xlim2)
    #plt.ylim(ylim1,ylim2)

    ax.set_xlabel(x_axis_label)
    ax.set_ylabel(y_axis_label)
    #plt.tight_layout()
    #plt.savefig('pfi_'+graph_label+'.eps')
    #plt.close()

    print('PFI plotted against: ',graph_label)
    tau_kendall, p_kendall = stats.kendalltau(country_cluster_normed, pfi)
    print('kendalltau: tau p = ',tau_kendall,p_kendall)

    return (tau_kendall, p_kendall)


if __name__ == '__main__':

    pass_val = 0 # test parameter 0 = pass, !0 = fail

    n = os.environ.get('SUFFIX')
    if(n):
        suffix = n
    else:
        suffix = ""

    rsf_file = "../inputs/RSF_press_freedom_index_2020.dat"
    pfi_correlations_vars = "pfi_correlations_vars"+suffix+".dat"
    pfi_correlations_table = "pfi_correlations_table"+suffix+".dat"
    # RSF PFI data
    country_pfi_all = np.loadtxt(rsf_file, usecols=0, dtype=np.str)
    #print('country_pfi_all = ',country_pfi_all)
    pfi_all = np.loadtxt(rsf_file, usecols=1, dtype=np.float)

    #count = 0
    phi_vars_text = ''
    psi_vars_text = ''

    phi_table_line = ''
    psi_table_line = ''

    plt.rcParams.update({'font.size': 12,
                         'font.family': 'serif'})

    with open(pfi_correlations_vars,"w+") as pfi_vars_file:
        with open(pfi_correlations_table,"w+") as pfi_table_file:
            pfi_table_file.write("# Kendall tau correlation coefficient and significance versus RSF Press Freedom Index\n")
            pfi_table_file.write("# Columns 1-8: pairs (tau, p) for: full, 28-day, 14-day, and 7-day subsequences; \n")
            pfi_table_file.write("# Row 1: phi_i\n")
            pfi_table_file.write("# Row 2: psi_i (i.e. phi_i normalised by the sqrt of the total infection count)\n")

            for model_choice in zip(["../data-to-publish/phi_N_full"+suffix+".dat",
                                     "../data-to-publish/phi_N_full"+suffix+".dat",
                                     "../data-to-publish/phi_N_28days"+suffix+".dat",
                                     "../data-to-publish/phi_N_28days"+suffix+".dat",
                                     "../data-to-publish/phi_N_14days"+suffix+".dat",
                                     "../data-to-publish/phi_N_14days"+suffix+".dat",
                                     "../data-to-publish/phi_N_07days"+suffix+".dat",
                                     "../data-to-publish/phi_N_07days"+suffix+".dat"],
                                    ["phi_full", "psi_full", "phi_28d", "psi_28d",
                                     "phi_14d", "psi_14d", "phi_07d", "psi_07d"],
                                    [False,     True,     False,     True,
                                     False,     True,     False,      True],
                                    ["$\phi_i$", "$\psi_i$", "$\phi_i^{28}$", "$\psi_i^{28}$",
                                     "$\phi_i^{14}$", "$\psi_i^{14}$", "$\phi_i^{7}$", "$\psi_i^{7}$"]):

                ## Import data: \phi_i, N_i => \psi_i
                country_phi_all = np.loadtxt(model_choice[0], usecols=0, dtype=np.str)
                (country_total_count_all,
                 country_cluster_all,
                 country_cluster_stderrlog10_all) = np.loadtxt(model_choice[0],
                                                               usecols=(1, 6,7),
                                                               dtype=np.float,
                                                               unpack=True)
                #print('country_phi_all = ',country_phi_all)

                # Match the countries between the two data sets by their ISO 3166-1 IDs.
                # TODO: This uses brute force matching; switch to a more efficient
                # algorithm before extending this to big data sets!
                country_total_count = np.array([],dtype=np.float)
                country_cluster = np.array([],dtype=np.float)
                country_cluster_stderrlog10 = np.array([],dtype=np.float)
                pfi = np.array([],dtype=np.float)
                for c1 in zip(country_phi_all,
                              country_total_count_all,country_cluster_all,country_cluster_stderrlog10_all):
                    for c2 in zip(country_pfi_all,pfi_all):
                        if(c1[0] == c2[0]):
                            country_total_count = np.append(country_total_count, c1[1])
                            country_cluster = np.append(country_cluster, c1[2])
                            country_cluster_stderrlog10 = np.append(country_cluster_stderrlog10, c1[3])
                            pfi = np.append(pfi, c2[1])

                graph_label = model_choice[1]
                x_axis_label = "PFI"
                normalise = model_choice[2]
                y_axis_label = model_choice[3]

                # If this is a new graph, then start the fig, ax objects.
                if(not(model_choice[2])):
                    fig, (ax1,ax2) = plt.subplots(1,2,figsize=(6.4,2.8))

                if(not(model_choice[2])):
                    ax=ax1
                    sublabel='A'
                else:
                    ax=ax2
                    sublabel='B'
                (tau_kendall, p_kendall) = do_rsf_plot(country_total_count,
                                                       normalise,
                                                       country_cluster,
                                                       country_cluster_stderrlog10,
                                                       pfi,
                                                       x_axis_label,
                                                       y_axis_label,
                                                       graph_label,
                                                       fig,ax,sublabel)
                if(model_choice[2]):
                    fig.tight_layout()
                    fig.savefig('pfi_phi_'+graph_label+'.eps')
                    plt.close()

                print('model_choice = ',model_choice)

                #pfi_table_file.write("%#10.3f %#10.3g " % (tau_kendall, p_kendall))
                if(not(model_choice[2])):
                    phi_table_line += ("%#10.3f %#10.3g " % (tau_kendall, p_kendall))
                    phi_vars_text += ("tau_Kendall_%s = %#.3f\n" % (graph_label, tau_kendall))
                    phi_vars_text += ("p_Kendall_%s = %#.3g\n" % (graph_label, p_kendall))
                else:
                    psi_table_line += ("%#10.3f %#10.3g " % (tau_kendall, p_kendall))
                    psi_vars_text += ("tau_Kendall_%s = %#.3f\n" % (graph_label, tau_kendall))
                    psi_vars_text += ("p_Kendall_%s = %#.3g\n" % (graph_label, p_kendall))
                #count+=1
                #if(np.mod(count,4)==0):
                #pfi_table_file.write("\n")

            #pfi_vars_file.write("p_Kendall_%s = %#.3g\n" % (graph_label, p_kendall))

            pfi_vars_file.write(phi_vars_text)
            pfi_vars_file.write(psi_vars_text)

            pfi_table_file.write(phi_table_line+'\n')
            pfi_table_file.write(psi_table_line+'\n')

    pfi_vars_file.close
    pfi_table_file.close

    print('pass_val = ',pass_val)
    sys.exit(pass_val)

#end __main__
            
