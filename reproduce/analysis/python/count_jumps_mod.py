#!/usr/bin/env python

# count_jumps_mod - count the number of jump/drop pairs in a counts sequence
# (C) 2020 Boud Roukema GPL-3+
#
# This script is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This script is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.

import numpy as np
from numpy.random import SeedSequence, default_rng
import sys
import os

def count_jumps(Noft_in, threshold_start):
    """
    Given a sequence (numpy array) of counts *Noft*, starting from the
    first value above *N_thresh_start*, count the number of pairs
    of which the absolute difference is greater than *jump_threshold*
    (hardwired) times the mean of the pair.

    Return: N_jumps
    """

    debug = False

    jump_threshold = 1.0  # hardwired

    # Full number of values
    N_initial = (Noft_in.shape)[0]
    # Find the values above threshold_start
    i_above_threshold = np.array(np.where( Noft_in >= threshold_start ))[0]

    if(debug):
        # Jump/drop pairs will be copied to this array for manual checking
        N_copy = np.zeros(N_initial,dtype=int)

    if(0 == i_above_threshold.size):
        N_jumps = 0
    else:
        # Extract the subsequence starting at the first value above the threshold
        Noft = Noft_in[i_above_threshold[0]:N_initial]
        N_Noft = Noft.size

        # Calculate the mean of each pair; should be N_Noft-1 values
        mean_per_pair = (Noft[0:N_Noft-1] +
                         Noft[1:N_Noft]) * 0.5
        mean_per_pair = np.maximum(1.0, mean_per_pair) # avoid division by zero

        # Calculate the difference of each pair and divide by the mean
        diff_per_pair = np.abs(Noft[0:N_Noft-1] -
                               Noft[1:N_Noft])
        diff_over_mean = diff_per_pair/mean_per_pair

        N_jumps = (np.array(np.where( (diff_over_mean > jump_threshold)
                                      & (mean_per_pair > threshold_start) )))[0].size

        if(debug):
            ## Copy jump/drop pairs into N_copy, adding 1 so that the values
            ## (including zero) can be distinguished from the originals.
            jumps_array = (np.array(np.where(diff_over_mean > jump_threshold)))[0]
            N_copy[ i_above_threshold[0]+jumps_array ] = 1+Noft[jumps_array]
            N_copy[ i_above_threshold[0]+jumps_array+1 ] = 1+Noft[jumps_array+1]
            print('jumps_array = ',jumps_array)
            print('Noft_in   = ',Noft_in)
            print('N_copy   = ',N_copy)


    return N_jumps


if __name__ == '__main__':

    pass_val = 0 # pass_val value of test: 0 = OK, 1 = fail

    n = os.environ.get('FIXED_RNG_SEED')
    if(n):
        fixed_rng_seed = np.bool(n)
    else:
        fixed_rng_seed = False

    seeds = SeedSequence(4597)
    seed = seeds.spawn(2)
    if(fixed_rng_seed):
        print('Fixed rng seed.')
        rng = default_rng(seed[0])
    else:
        print('Numpy automatic rng seed.')
        rng = default_rng()

    # Set up an array with Poisson random values which will often
    # be zero, to see how well the jumps are counted.
    Noft = 10 * rng.poisson(3,14) # 30 * rng.poisson(1,14)
    threshold_start = 5

    N_jumps = count_jumps(Noft, threshold_start)

    print("Noft     = ",Noft)
    print("N_jumps = ",N_jumps)

    print("pass_val value = ",pass_val)
    sys.exit(pass_val)

#end __main__
