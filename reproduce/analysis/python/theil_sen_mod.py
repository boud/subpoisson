#!/usr/bin/env python

# theil_sen - Theil-Sen linear regression - robust linear fit
# (C) 2018-2020 Boud Roukema GPL-3+
#
# This script is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This script is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.

import sys
import os
import numpy as np
from numpy.random import SeedSequence, default_rng
import matplotlib
import matplotlib.pyplot as plt

def theil_sen(x_in, y_in):
    """
    Theil-Sen linear regression - robust linear fit
    e.g. https://en.wikipedia.org/w/index.php?title=Theil%E2%80%93Sen_estimator&oldid=865140889
    input parameters: numpy arrays x (independent), y (dependent)
    """

    ## TODO: error feedback for cases with two few points
    N_x = x_in.size;
    N_y = y_in.size;
    if (N_x != N_y):
        raise RuntimeError("theil_sen: Error: N_x != N_y:", N_x,N_y)

    x_local = np.tile(x_in, (N_x,1))
    y_local = np.tile(y_in, (N_x,1))

    ## prepare to select the upper right triangle where i_col > i_row
    i_col, i_row = np.meshgrid(np.arange(N_x),np.arange(N_x))

    ## calculate x and y differences; and slopes
    dx = np.transpose(x_local) - x_local
    dy = np.transpose(y_local) - y_local
    indices = np.array(np.where(i_col > i_row))
    #print('indices = ',indices)
    #slopes_all = dy/dx
    #print('slopes_all = ',slopes_all)
    indices_zip = zip(indices[0],indices[1])
    slopes = np.array([])
    for ij in indices_zip:
        # ignore nans or \pm Inf from doubled x values
        #print('ij=',ij,'slopes_all[ij]= ',slopes_all[ij])
        #slope = dy[ij]/dx[ij]
        #if np.isfinite(slope):
        if(np.abs(dx[ij]) > 1e-30):
            slope = dy[ij]/dx[ij]
            slopes = np.append(slopes,slope)

    #print('slopes=',slopes)
    pslope = np.median(slopes)

    ## median intercept
    pzeropoint = np.median(y_in - pslope* x_in)

    return (pslope, pzeropoint)

def theil_sen_robust_stderr(x_in, y_in, ths_fixed_rng_seed_value=4733):
    """
    bootstrap estimate of uncertainty in Theil-Sen zeropoint
    """

    n = os.environ.get('FIXED_RNG_SEED')
    if(n):
        fixed_rng_seed = np.bool(n)
    else:
        fixed_rng_seed = False

    ## TODO: error feedback for cases with two few points

    ## sigma is 1.4826 * median absolute deviation for Gaussian
    sig_from_MAD = 1.4826;

    N_boot = 100;
    ps = np.zeros(N_boot)
    pz = np.zeros(N_boot)
    N_x = x_in.size

    seeds = SeedSequence(ths_fixed_rng_seed_value)
    seed = seeds.spawn(2)
    if(fixed_rng_seed):
        print('theil_sen_robust_stderr: Fixed rng seed.')
        print('theil_sen_robust_stderr: ths_fixed_rng_seed_value = ',
              ths_fixed_rng_seed_value)
        rng = default_rng(seed[0])
    else:
        print('theil_sen_robust_stderr: Numpy automatic rng seed.')
        rng = default_rng()

    for i in np.arange(N_boot):
        ## bootstrap indices - indices allowing resampling
        j = rng.integers(N_x, size=N_x)
        #print('theil_sen_robust_stderr: j = ',j)
        (ps[i], pz[i]) = theil_sen(x_in[j], y_in[j])
        #print('theil_sen_robust_stderr: i,ps[i],pz[i] = ',i,ps[i],pz[i])

    ps_median = np.median(ps)
    pz_median = np.median(pz)
    #print('theil_sen_robust_stderr: ps_median,pz_median = ',ps_median,pz_median)
    ## robust estimate of variation
    sig_pslope = np.median(np.abs(ps-ps_median)) * sig_from_MAD;
    sig_pzeropoint = np.median(np.abs(pz-pz_median)) * sig_from_MAD;

    return sig_pslope, sig_pzeropoint


def test_theil_sen():
    ## interactive test parameters
    show_plot = False

    N_x_gauss = 50
    noise_amp = 1.0
    N_x_outlier = 10
    outl_noise_amp = 2.0

    N_x = N_x_gauss + N_x_outlier

    true_slope = np.pi
    true_zeropoint = np.e
    outlier_slope = -2.0
    outlier_zeropoint = 3.0

    ## gaussian scatter
    seeds = SeedSequence(4547)
    #print('seeds=',seeds)
    seed = seeds.spawn(2)
    #print('seed=',seed)
    rng = default_rng(seed[0])
    #rng = default_rng()

    # Choose some x values randomly in the range from 0 to 1
    x = rng.uniform(size=N_x)

    ## Create a linear relation with a little Gaussian noise
    ii_gauss = np.arange(N_x_gauss)
    y = np.zeros(N_x)
    y[ii_gauss] = (true_slope * x[ii_gauss] +
                   true_zeropoint +
                   rng.normal(size=N_x_gauss)*noise_amp)

    ## Add some outliers - with a different slope, zeropoint, noise, and add
    ## a square component:
    ii_outl = np.arange(N_x_gauss,N_x)
    y[ii_outl] = (outlier_slope*x[ii_outl] +
                  outlier_zeropoint -
                  (x[ii_outl]**2) +
                  rng.normal(size=N_x_outlier) * outl_noise_amp)

    if(show_plot):
        matplotlib.use('Agg')
        plt.figure()
        plt.scatter(x[ii_gauss],y[ii_gauss],marker='o',c='g',label='good')
        plt.scatter(x[ii_outl],y[ii_outl],marker='x',c='r',label='bad')
        plt.savefig('theil_sen_test.eps')
        plt.close()

    print("\n\ntrue: %g %g\n" % (true_slope,true_zeropoint))
    pp = np.polyfit(x, y, 1)
    print("polyfit: %g %g\n" % (pp[0], pp[1]))
    pslope, pzeropoint = theil_sen(x, y)
    print("theil-sen: %g %g\n" % (pslope, pzeropoint))

    sig_pslope, sig_pzeropoint = (
        theil_sen_robust_stderr(x,y, ths_fixed_rng_seed_value=12354) )
    print("sigma theil-sen: %g %g\n" % (sig_pslope, sig_pzeropoint))

    #plot([0 1], pslope.*[0 1] .+ pzeropoint,'-k;Theil--Sen;','linewidth',5)
    #plot([0 1], pp(1).*[0 1] .+ pp(2),'-b;polyfit;','linewidth',5)

    return


if __name__ == '__main__':

    pass_val = 0 # pass_val value of test: 0 = OK, 1 = fail

    test_theil_sen() # interactive

    print("pass_val value = ",pass_val)
    sys.exit(pass_val)

#end __main__
