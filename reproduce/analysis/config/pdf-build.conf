# Make the final PDF?
# -------------------
#
# During the project's early phases, it is usually not necessary to build
# the PDF file (which makes a lot of output lines on the command-line and
# can make it hard to find the analysis commands and possible errors (and
# their outputs). Also, in some cases, only the produced results may be of
# interest and not the final PDF, so LaTeX (and its necessary packages) may
# not be installed at all.
#
# If 'pdf-build-final' is given the value 'yes', a PDF will be made with
# LaTeX. Otherwise, a notice will just printed that, no PDF will be
# created.
#
# Copyright (C) 2018-2021 Mohammad Akhlaghi <mohammad@akhlaghi.org>
#
# Copying and distribution of this file, with or without modification, are
# permitted in any medium without royalty provided the copyright notice and
# this notice are preserved.  This file is offered as-is, without any
# warranty.

# This parameter is a list of bibitem keys for highlighting new references
# in the produced file 'paper-full.tex'. WARNING: the algorithm is very
# crude and likely to be quite specific to the choice of .bst file. Do not
# use this without change the rules in 'paper.mk'.
new-reference-keys = Chowdhury2020 Huang2020covid19 HuangZhang2020covid19incubation Rouabah2020DZ Akhlaghi2020maneage JiangZhaoShao2020covid19 \
            Fokianos2011 Agosto2021 HarveyKattuman2020


old-reference-keys = Akaike74 Billah2020R0about3 CrouxDehon2010 \
            Endo2020overdisp He2020lowdisp matplotlib2007 JohnsonKempKotz05 \
            JustelPenaZamar97 Kendall38 Kendall70 Lauer2020incub LloydSmith2005Nature \
            Marsaglia03 scipy2011 MunckVerkuilen02 scipy2007 PapoulisPillai02 \
            RSFmethod2021 Schwarz78 ShapiroWilk65 Yang2020incub numpy2011

# The following are old and could, in principle, be listed as "removed", but
# that would require even more hacking...
very-old-reference-keys = ElMoudjahid20200530DZ200 TSA20200720DZfulllockdown


pdf-build-final = yes
