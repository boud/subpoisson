# Input files necessary for this project, the variables defined in this
# file are primarily used in 'reproduce/analysis/make/download.mk'. See
# there for precise usage of the variables. But comments are also provided
# here.
#
# Necessary variables for each input dataset are listed below. It's best
# if all the variables of each file have the same base-name (in the
# example below 'WHOCOVID19') with descriptive suffixes, also put a short
# comment above each group of variables for each dataset, shortly
# explaining what it is.
#
#  1) Local file name ('WHOCOVID19_FILENAME' below): this is the name of the dataset
#     on the local system (in 'INDIR', given at configuration time). It is
#     recommended that it be the same name as the online version of the
#     file like the case here (note how this variable is used in 'WHOCOVID19_URL'
#     for the dataset's full URL). However, this is not always possible, so
#     the local and server filenames may be different. Ultimately, the file
#     name is irrelevant, we check the integrity with the checksum.
#
#  2) The MD5 checksum of the file ('WHOCOVID19_MD5' below): this is very
#     important for an automatic verification of the file. You can
#     calculate it by running 'md5sum' on your desired file. You can also
#     use any other checksum tool that you prefer, just be sure to correct
#     the respective command in 'reproduce/analysis/make/download.mk'.
#
#  3) The human-readable size of the file ('WHOCOVID19_SIZE' below): this is an
#     optional feature which you can use for in the script that is loaded
#     at configure time ('reproduce/software/shell/configure.sh'). When
#     asking for the input-data directory, you can print some basic
#     information of the files for users to get a better feeling of the
#     volume. See that script for an example using this demo dataset.
#
#  4) The full dataset URL ('WHOCOVID19_URL' below): this is the full URL
#     (including the file-name) that can be used to download the dataset
#     when necessary. Also, see the description above on local filename.
#
# Copyright (C) 2018-2021 Mohammad Akhlaghi <mohammad@akhlaghi.org>
# Copyright (C) 2020-2021 Boud Roukema
#
# Copying and distribution of this file, with or without modification, are
# permitted in any medium without royalty provided the copyright notice and
# this notice are preserved.  This file is offered as-is, without any
# warranty.

## Input data on daily infection counts
WHOCOVID19_SRCURL = https://covid19.who.int/WHO-COVID-19-global-data.csv
WHOCOVID19_FILENAME = WHO-COVID-19-global-data.csv

# archived on 2021-05-06:
# Use the backup copy in reproduce/inputs/  if the archive copy fails.
WHOCOVID19_ARCHIVEURL = https://web.archive.org/web/20210506113321/https://covid19.who.int/WHO-COVID-19-global-data.csv
#https://web.archive.org/web/20220307024146/https://covid19.who.int/WHO-COVID-19-global-data.csv
WHOCOVID19_CHECKSUM = 3e29cd272898a715aa0dec3776bcacb08a42d7c2ac92a6e53dd642bb426e46b1e40fd8f477d1f1d27beabf3ed8bb17eead79a7080dd0dff03e3d4bfa5b0bef95
#2022-03-08 updated
#WHOCOVID19_CHECKSUM = cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e

## archived on 2020-07-22:
## 2020-07-22 attempted downloads:
##    - the 20200722024239 snapshot redirects to the 20200715231206 snapshot:
##    - this may become available later when Wayback has stabilised its storage;
#WHOCOVID19_ARCHIVEURL = https://web.archive.org/web/20200722024239/https://covid19.who.int/WHO-COVID-19-global-data.csv
#WHOCOVID19_CHECKSUM = e52499a292d8e9d4a61d2ffb2d8eeb7c6416c8b17d822f66e5be7a58bacdb505c3dd7686e0986a24a9c645b5471295082bc435853e1e80a60fb31a82cb0e279a

## archived on 2020-07-21:
## 2020-07-22 attempted downloads;
##    - the 20200721142549 snapshot redirects to the 20200715231206 snapshot;
##    - this may become available later when Wayback has stabilised its storage;
#WHOCOVID19_ARCHIVEURL = https://web.archive.org/web/20200721142549/https://covid19.who.int/WHO-COVID-19-global-data.csv
#WHOCOVID19_CHECKSUM = 8a9a0e9e52a12c4980284ae2e203a1ba509143239a20c0d07058aa8438e3a97f0d0d8cbfe769b9e8ee8addf33f8c38e93e7b72deac42cbe7c7f2c957defdfabb

# 2020-07-15
#WHOCOVID19_ARCHIVEURL = https://web.archive.org/web/20200715231206/https://covid19.who.int/WHO-COVID-19-global-data.csv
#WHOCOVID19_ARCHIVE_CHECKSUM = 5c6e635e7c6e7a62429bc4bb9a9d849c5e368e5b6a8bd45aad1945367ee202b90085eb052516651f1390be6b53209b65cf44174339d4cf14f05cdcff438324be

# 2020-06-29
#WHOCOVID19_ARCHIVEURL = https://web.archive.org/web/20200629212040/https://covid19.who.int/WHO-COVID-19-global-data.csv
#WHOCOVID19_CHECKSUM = 3f3970aadcf3764ba1032092fe2b48241b0f827976a01113ddd2cfe5dce2dfa1a6f3ea75f78eb1f8bbd453217eae5f24c801a5fa61d605b10e3f81c97856dd50

## See reproduce/analysis/bash/download-wikipedia-SARS-CoV-2-charts.sh for
## a fresh download of the 137 or so source files.
WIKIPEDIA_CHARTS_FILENAME = Wikipedia_SARSCoV2_charts.dat
#2022-03-08 updated
WIKIPEDIA_CHARTS_CHECKSUM = ba02e2da3818048afbefe028646de01b64767c7239d88b1075e14e462a31390203cf725dd392e2676c278b3514ac31c78f0ab177617354a00af7186cdde15bc7
#WIKIPEDIA_CHARTS_CHECKSUM = 3bfd264e8a3a63c59feaf0fbc5aa7f6af9a15b5310ce93ad8cfeadffd9b83575c80265f0cde668f95e190f3872e12cb03623825b2694b3b05f6a488aece25310
#WIKIPEDIA_CHARTS_CHECKSUM = 3d92f6ffa88c53c14c9d392e5b2ffafbc776877e05cec59d834fb7b18cd1d56b433aa9a0accf1531eeb29d1950ae876f93dbab154341605f45e093bdeb1c0bac
#WIKIPEDIA_CHARTS_CHECKSUM = e38ee19bd53ca245199b4ca11c9ce30479a10165d9bba86dd2d78aec2067393ee79007647c89d941d9fd0c8c3585b4ee9c6c2caf904d65bf06e045632137273b
WIKIPEDIA_CHARTS_SCRIPT = download-wikipedia-SARS-CoV-2-charts.sh

## See reproduce/analysis/bash/download-JHU-CSSEGIS-SARS-CoV-2.sh for a fresh download
## of JHU CSSEGIS data; the script includes a hardwired clean to remove annoying non-separating commas.
##
#JHU_URL = https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_confirmed_global.csv
JHU_URL = https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_deaths_global.csv
#JHU_DATE = 2020-08-15
#JHU_DATE = 2021-05-06
JHU_DATE = 2022-03-09
JHU_FILENAME = JHU_CSSEGIS_SARSCoV2.dat
#JHU_FILENAME = JHU_CSSEGIS_SARSCoV2_deaths.dat
## The following commit ID and checksum should be updated if a new download is done:
#JHU_COMMIT = 51cb3ee
# TO BE UPDATED:
JHU_COMMIT = 7dce70d
## - the checksum is on the file cleaned of commas in country names
#JHU_CHECKSUM = 65364f377db8e0bfeebe9e16cc5070756cec6e1cd6fdd42155ddda8044096596575102cdf928219b614247459c54ecc5356f9db1f26bd581ca82e43b98f43df4
#JHU_CHECKSUM = 2bf75fbe2ee512f0407f41819c76ab3aad6aa05ac437061a846ced6e943d692cc5c9b0771b2f73e66d04426500a1c496bc7764b7f128e99faa3f56966a029cab
#JHU_CHECKSUM = 1d5f2c35fec331abaa7a6b37d59e9c36f1f191b1cc5d0dda2ee4858e952d92c246afe0a4f32c841da94fb6e0d5a0368c16a94c7077e1ea7047f038031b1a700d
JHU_CHECKSUM = 9aaf22058ec0e44d2539e08b6adcfb5e17ca260f348cca836fab54846089612d327e95c772c4c10319399e1576021dbae77e27e1c86bf469ac886affda5a6804

## Reporters Sans Frontieres = Reporters Without Borders Press Freedom Index
RSF_PRESSFREEDOM_URL = https://rsf.org/en/ranking/2020
RSF_PRESSFREEDOM_FILENAME = RSF_press_freedom_index_2020.dat
RSF_PRESSFREEDOM_CHECKSUM = 41ec39c5f442757dde34182c799c3e631dab06d2fb334228908530e9af8b87de395701cb6c196e0358d2273d3cfe8fa28acacbab7f59e21c8f4454e9c4359762
RSF_PRESSFREEDOM_DATE = 2021-05-04


#JOURNAL_LATEX_DIRNAME = 
#JOURNAL_LATEX_FILENAME =
#JOURNAL_LATEX_VERSION = 
#JOURNAL_LATEX_CHECKSUM =
## As of 2021-01-23, the Overleaf/PeerJ package requires a login for
## downloading the full CC-BY 4.0 template + class file.  This is not
## completely in the spirit of open-access: it creates an unnecessary
## obstacle.
#JOURNAL_LATEX_URL = https://www.overleaf.com/latex/templates/latex-template-for-peerj-journal-and-pre-print-submissions/ptdwfrqxqzbn
#JOURNAL_LATEX_ARCHIVEURL =
