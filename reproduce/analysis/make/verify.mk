# Verify the project outputs before building the paper.
#
# Copyright (C) 2020-2021 Mohammad Akhlaghi <mohammad@akhlaghi.org>
#
# This Makefile is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This Makefile is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this Makefile.  If not, see <http://www.gnu.org/licenses/>.





# Verification functions
# ----------------------
#
# These functions are used by the final rule in this Makefile
verify-print-error-start = \
  echo; \
  echo "VERIFICATION ERROR"; \
  echo "------------------"; \
  echo

verify-print-tips = \
  echo "If you are still developing your project, you can disable"; \
  echo "verification by removing the value of the variable in the"; \
  echo "following file (from the top project source directory):"; \
  echo "    reproduce/analysis/config/verify-outputs.conf"; \
  echo; \
  echo "If this is the final version of the file, you can just copy"; \
  echo "and paste the calculated checksum (above) for the file in"; \
  echo "the following project source file:"; \
  echo "    reproduce/analysis/make/verify.mk"

# Removes following components of a plain-text file, calculates checksum
# and compares with given checksum:
#   - All commented lines (starting with '#') are removed.
#   - All empty lines are removed.
#   - All space-characters in remaining lines are removed (so the width of
#     the printed columns won't invalidate the verification).
#
# It takes three arguments:
#   - First argument: Full address of file to check.
#   - Second argument: Expected checksum of the file to check.
#   - File name to write result.
verify-txt-no-comments-no-space = \
  infile=$(strip $(1)); \
  inchecksum=$(strip $(2)); \
  innobdir=$$(echo $$infile | sed -e's|$(BDIR)/||g'); \
  if ! [ -f "$$infile" ]; then \
    $(call verify-print-error-start); \
    echo "The following file (that should be verified) doesn't exist:"; \
    echo "    $$infile"; \
    echo; exit 1; \
  fi; \
  checksum=$$(sed -e 's/[[:space:]][[:space:]]*//g' \
                  -e 's/\#.*$$//' \
                  -e '/^$$/d' $$infile \
                  | md5sum \
                  | awk '{print $$1}'); \
  if [ x"$$inchecksum" = x"$$checksum" ]; then \
    echo "%% (VERIFIED) $$checksum $$innobdir" >> $(3); \
  else \
    $(call verify-print-error-start); \
    $(call verify-print-tips); \
    echo; \
    echo "Checked file (without empty or commented lines):"; \
    echo "    $$infile"; \
    echo "Expected MD5 checksum:   $$inchecksum"; \
    echo "Calculated MD5 checksum: $$checksum"; \
    echo; exit 1; \
  fi;





# Final verification TeX macro (can be empty)
# -------------------------------------------
#
# This is the FINAL analysis step (before going onto the paper. Please use
# this step to verify the contents of the figures/tables used in the paper
# and the LaTeX macros generated from all your processing. It should depend
# on all the LaTeX macro files that are generated (their contents will be
# checked), and any files that go into the tables/figures of the paper
# (generated in various stages of the analysis.
#
# Since each analysis step's data files are already prerequisites of their
# respective TeX macro file, its enough for `verify.tex' to depend on the
# final TeX macro.
#
# USEFUL TIP: during the early phases of your research (when you are
# developing your analysis, and the values aren't final), you can comment
# the respective lines.
#
# Here is a description of the variables defined here.
#
#   verify-dep: The major step dependencies of `verify.tex', this includes
#               all the steps that must be finished before it.
#
#   verify-changes: The files whose contents are important. This is
#               essentially the same as `verify-dep', but it has removed
#               the `initialize' step (which is information about the
#               pipeline, not the results).
verify-dep = $(subst verify,,$(subst paper,,$(makesrc)))
verify-check = $(subst initialize,,$(verify-dep))
$(mtexdir)/verify.tex: $(foreach s, $(verify-dep), $(mtexdir)/$(s).tex)

        # Make sure that verification is actually requested, the '@' at the
        # start of the recipe is added so Make doesn't print the commands
        # on the standard output because this recipe is run on every call
        # to the project and can be annoying (get mixed in the middle of
        # the analysis outputs or the LaTeX outputs).
	@if [ x"$(verify-outputs)" = xyes ]; then

          # Make sure the temporary output doesn't exist (because we want
          # to append to it). We are making a temporary output target so if
          # there is a crash in the middle, Make will not continue. If we
          # write in the final target progressively, the file will exist,
          # and its date will be more recent than all prerequisites, so
          # next time the project is run, Make will continue and ignore the
          # rest of the checks.
	  rm -f $@.tmp

          # Verify the figure datasets.
	  if [ "x$(enable_dev_override)" = "xTrue" ]; then
	    printf "\nVerify.mk: Warning: you are running in *enable_dev_override* mode;\n"
	    printf "the calculations will be inaccurate (but faster than for the real calculation).\n"
	    sleep 5
	    $(call verify-txt-no-comments-no-space, \
	         $(outdir)/WHO_vs_WP.dat, 6b5d818852b97e9c6aa6a4ad0ff0347b, $@.tmp)
	    $(call verify-txt-no-comments-no-space, \
	         $(data-publish-dir)/phi_N_full.dat, 99fb7500917ec2b33fcd22fc293269bc, $@.tmp)
	    $(call verify-txt-no-comments-no-space, \
	         $(data-publish-dir)/phi_N_28days.dat, 57658592f24f5d9f5c0b2051785ef0ca, $@.tmp)
	    $(call verify-txt-no-comments-no-space, \
	         $(data-publish-dir)/phi_N_14days.dat, 7dd2f40c9dcea878f8e19837e0aea064, $@.tmp)
	    $(call verify-txt-no-comments-no-space, \
	         $(data-publish-dir)/phi_N_07days.dat, bec2cd1571b7c69a07382c1c22f475aa, $@.tmp)
	  else # production run
            # Why WP C19CCTF data is better than WHO
	    $(call verify-txt-no-comments-no-space, \
	         $(outdir)/WHO_vs_WP.dat, 43ab06f4f5df0ee3132d5ca3b36f74da, $@.tmp)
            # main results
	    $(call verify-txt-no-comments-no-space, \
	         $(data-publish-dir)/phi_N_full.dat, 8b97a679220310ad311d7ff05f52c0e4, $@.tmp)
	    $(call verify-txt-no-comments-no-space, \
	         $(data-publish-dir)/phi_N_28days.dat, 0503c3b73768b68a00aa2f3411fd8c6c, $@.tmp)
	    $(call verify-txt-no-comments-no-space, \
	         $(data-publish-dir)/phi_N_14days.dat, 22fdb7cc9a744b7f714a3d17031510b6, $@.tmp)
	    $(call verify-txt-no-comments-no-space, \
	         $(data-publish-dir)/phi_N_07days.dat, fbd4ed04f7c19082f2052c11b7d87061, $@.tmp)
	    $(call verify-txt-no-comments-no-space, \
	         $(data-publish-dir)/AIC_BIC_full.dat, 465f992e0cafcdcdaf2c9e3fe5b3f93d, $@.tmp)
	    $(call verify-txt-no-comments-no-space, \
	         $(data-publish-dir)/pfi_correlations_table.dat, bb6bb4366bbf4d6351db57219bce00e7, $@.tmp)
            # JHU supplementary results
	    $(call verify-txt-no-comments-no-space, \
	         $(data-publish-dir)/phi_N_full_jhu.dat, 2e184de340fc1d7a5c38643df6684812, $@.tmp)
	    $(call verify-txt-no-comments-no-space, \
	         $(data-publish-dir)/phi_N_28days_jhu.dat, 83a0462c6001b919da55e341901acd56, $@.tmp)
	    $(call verify-txt-no-comments-no-space, \
	         $(data-publish-dir)/phi_N_14days_jhu.dat, 9342c7f2ec67f780ae576d4b4efc70a2, $@.tmp)
	    $(call verify-txt-no-comments-no-space, \
	         $(data-publish-dir)/phi_N_07days_jhu.dat, 30ddae4e49df838fd4c118dfd60ea88e, $@.tmp)
	    $(call verify-txt-no-comments-no-space, \
	         $(data-publish-dir)/AIC_BIC_full_jhu.dat, a785a8e7c751e2b3fd40096768e3c343, $@.tmp)
	    $(call verify-txt-no-comments-no-space, \
	         $(data-publish-dir)/pfi_correlations_table_jhu.dat, 8e51b12c4b09c363f4009a2dc36527e9, $@.tmp)
	  fi

          # Verify TeX macros (the values that go into the PDF text).
	  for m in $(verify-check); do
	    file=$(mtexdir)/$$m.tex
	    printf "Will try verifying $${file}...\n"
	    if   [ $$m == download  ]; then s=2f9f7a9deee68c43be94ab92fe7a8b15
	    elif [ $$m == poisson ]; then
	        if [ "x$(enable_dev_override)" = "xTrue" ]; then
	            s=73da4729ead3102301bce929fbeb4c36
	        else
	            s=022b65a6512e81fa4d76d747d16eefc7
	        fi
	    else echo; echo "'$$m' not recognized."; exit 1
	    fi
	    $(call verify-txt-no-comments-no-space, $$file, $$s, $@.tmp)
	  done

          # Move temporary file to final target.
	  mv $@.tmp $@
	else
	  echo "% Verification was DISABLED!" > $@
	fi
