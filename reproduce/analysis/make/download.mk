# Download all the necessary inputs if they are not already present.
#
# Since most systems only have one input/connection into the network,
# downloading is essentially a serial (not parallel) operation. so the
# recipes in this Makefile all use a single file lock to have one download
# script running at every instant.
#
# Copyright (C) 2018-2021 Mohammad Akhlaghi <mohammad@akhlaghi.org>
#
# This Makefile is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This Makefile is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this Makefile.  If not, see <http://www.gnu.org/licenses/>.





# Download input data
# --------------------
#
# The input dataset properties are defined in
# `$(pconfdir)/INPUTS.conf'. For this template we only have one dataset to
# enable easy processing, so all the extra checks in this rule may seem
# redundant.
#
# In a real project, you will need more than one dataset. In that case,
# just add them to the target list and add an `elif' statement to define it
# in the recipe.
#
# Files in a server usually have very long names, which are mainly designed
# for helping in data-base management and being generic. Since Make uses
# file names to identify which rule to execute, and the scope of this
# research project is much less than the generic survey/dataset, it is
# easier to have a simple/short name for the input dataset and work with
# that. In the first condition of the recipe below, we connect the short
# name with the raw database name of the dataset.
#
# Download lock file: Most systems have a single connection to the
# internet, therefore downloading is inherently done in series. As a
# result, when more than one dataset is necessary for download, if they are
# done in parallel, the speed will be slower than downloading them in
# series. We thus use the `flock' program to tie/lock the downloading
# process with a file and make sure that only one downloading event is in
# progress at any moment.
$(indir):; mkdir $@
downloadwrapper = $(bashdir)/download-multi-try
inputdatasets = $(indir)/$(WHOCOVID19_FILENAME) \
                   $(indir)/$(WIKIPEDIA_CHARTS_FILENAME) \
                   $(indir)/$(JHU_FILENAME) \
                   $(indir)/$(RSF_PRESSFREEDOM_FILENAME)

#                   $(indir)/$(JOURNAL_LATEX_FILENAME)

inputs_export_file = $(data-publish-dir)/WP_C19CCTF_SARSCoV2.dat


inputs_checked = $(indir)/inputs-checked

## Shifted to paper.mk for hardwired files.
#journal-latex-cls-file = non-free/interact.cls
#journal-latex-bst-file = tfs_openaccess.bst
#journal-latex-files = $(journal-latex-cls-file) $(journal-latex-bst-file)

# The following function assumes that updates to the WP C19CCTF data file
# in the git repository will only be made after a fresh download of data.
get-WP-C19CCTF-downloaded-date = \
          $$(LANG=C git log --date=format:"%-d %B %Y" reproduce/inputs/Wikipedia_SARSCoV2_charts.dat | \
                grep Date | head -n 1 | sed -e 's/Date: *//')
get-WHO-downloaded-date = \
          $$(LANC=C date --date=$$(echo $(WHOCOVID19_ARCHIVEURL) |sed -e 's;^.*/web/\(2[0-9][0-9][0-9]\)\([01][0-9]\)\([0-3][0-9]\).*csv;\1-\2\-\3;') +"%-d %B %Y")

do-download: $(inputdatasets) $(inputs_checked)

.PHONY: do-download clean-download

# Download (or make the link to) the input dataset. If the file
# exists in `INDIR', it may be a symbolic link to some other place
# in the filesystem. To avoid too many links when using these files
# during processing, we'll use `readlink -f' so the link we make
 # here points to the final file directly (note that `readlink' is
# part of GNU Coreutils). If it's not a link, then the `readlink' part
# has no effect.
download-and-check = \
	localname="$(strip $(1))"; url="$(strip $(2))"; \
	checksum="$(strip $(3))"; \
	unchecked=$${localname}.unchecked \
	&& if [ -f $(INDIR)/$${localname} ]; then \
	  ln -fs $$(readlink -f $(INDIR)/$$localname) $$unchecked; \
	elif [ -f $(indir)/$${localname} ]; then \
	  mv -v $(indir)/$${localname} $$unchecked; \
	else \
	  touch $(lockdir)/download; \
	  $(downloadwrapper) "wget --no-use-server-timestamps -O" \
	                     $(lockdir)/download $$url $$unchecked; \
	fi; \
	\
	sum=$$(sha512sum $$unchecked | awk '{print $$1}') \
	&& if [ $$sum = $$checksum ]; then \
	  mv -v $${unchecked} $(indir)/$${localname}; \
	else \
	  echo; echo; \
	  echo "Wrong checksum for input file '$$localname':"; \
	  echo "  File location: $$(pwd)/$${unchecked}" ; \
	  echo "  Expected checksum:   $$checksum"; \
	  echo "  Calculated checksum: $$sum"; \
	  echo; exit 1; \
	fi

$(inputdatasets): $(inputs_checked)

$(inputs_checked): | $(indir) $(lockdir)
        # Download (or copy to the build area) and check the input files.
	$(call download-and-check, $(WHOCOVID19_FILENAME), \
	   $(WHOCOVID19_ARCHIVEURL), $(WHOCOVID19_CHECKSUM))
        #
	localname=$(WIKIPEDIA_CHARTS_FILENAME)
	unchecked=$(indir)/$${localname}.unchecked
	checksum=$(WIKIPEDIA_CHARTS_CHECKSUM)
	cp -pv $(repinputsdir)/$(WIKIPEDIA_CHARTS_FILENAME) $${unchecked}
	sum=$$(sha512sum $$unchecked | awk '{print $$1}')
	if [ $$sum = $$checksum ]; then
	    mv -v $${unchecked} $(indir)/$${localname}
	else
	    echo; echo
	    echo "Wrong checksum for input file '$$localname':"
	    echo "  Expected checksum:   $$checksum"
	    echo "  Calculated checksum: $$sum"
	    echo; exit 1
	fi
        #
	localname=$(JHU_FILENAME)
	unchecked=$(indir)/$${localname}.unchecked
	checksum=$(JHU_CHECKSUM)
	cp -pv $(repinputsdir)/$(JHU_FILENAME) $${unchecked}
	sum=$$(sha512sum $$unchecked | awk '{print $$1}')
	if [ $$sum = $$checksum ]; then
	    mv -v $${unchecked} $(indir)/$${localname}
	else
	    echo; echo
	    echo "Wrong checksum for input file '$$localname':"
	    echo "  Expected checksum:   $$checksum"
	    echo "  Calculated checksum: $$sum"
	    echo; exit 1
	fi
        #
	localname=$(RSF_PRESSFREEDOM_FILENAME)
	unchecked=$(indir)/$${localname}.unchecked
	checksum=$(RSF_PRESSFREEDOM_CHECKSUM)
	cp -pv $(repinputsdir)/$(RSF_PRESSFREEDOM_FILENAME) $${unchecked}
	sum=$$(sha512sum $$unchecked | awk '{print $$1}')
	if [ $$sum = $$checksum ]; then
	    mv -v $${unchecked} $(indir)/$${localname}
	else
	    echo; echo
	    echo "Wrong checksum for input file '$$localname':"
	    echo "  Expected checksum:   $$checksum"
	    echo "  Calculated checksum: $$sum"
	    echo; exit 1
	fi
	# $(call download-and-check, $(JOURNAL_LATEX_FILENAME), \
	#   $(JOURNAL_LATEX_URL), $(JOURNAL_LATEX_CHECKSUM)) &&
	#   cd $(indir)
	#   mkdir -p $(JOURNAL_LATEX_DIRNAME)
	# rm -f $(JOURNAL_LATEX_DIRNAME)/[a-z]*
	# rm -f $(JOURNAL_LATEX_DIRNAME)/[a-z]*/.log
	# rmdir $(JOURNAL_LATEX_DIRNAME)/[a-z]*
	# cd $(JOURNAL_LATEX_DIRNAME)
	# unzip $(JOURNAL_LATEX_FILENAME)
	# cd $(curdir)
        #
	touch $(inputs_checked)

$(inputs_export_file): $(inputs_checked) | $(data-publish-dir)
	WP_downloaded_date=$(call get-WP-C19CCTF-downloaded-date)
	$(call print-general-metadata, $@, new)
	printf "# Wikipedia WikiProject COVID-19 Case Count Task Force (C19CCTF)\n" >> $@
	printf "#   medical cases chart data extracted and collated on $${WP_downloaded_date}\n" >> $@
	printf "# Column 1: date         [yyyy-mm-dd]\n" >> $@
	printf "# Column 2: XX           [%%2s] country ISO 3166-1 alpha-2 code\n" >> $@
	printf "# Column 3: N_infect     [%%d] cumulative infection count\n" >> $@
	printf "# Column 4: N_deaths     [%%d] cumulative death count\n" >> $@
	printf "# Comment after # in first entry per country: article ID, revision ID, download URL\n" >> $@
	cat $(indir)/$(WIKIPEDIA_CHARTS_FILENAME) >> $@

# Final TeX macro
# ---------------
#
# It is very important to mention the address where the data were
# downloaded in the final report.
$(mtexdir)/download.tex: $(pconfdir)/INPUTS.conf \
                            $(inputs_export_file) \
                            | $(mtexdir)
	WP_downloaded_date=$(call get-WP-C19CCTF-downloaded-date)
	WHO_downloaded_date=$(call get-WHO-downloaded-date)
	printf "%% Automatically produced file.\n\n"
	printf "%% Automatically produced file.\n\n" > $@
	printf "\\\\newcommand{\\\\WPchartscript}{$(WIKIPEDIA_CHARTS_SCRIPT)}\n" >> $@
	printf "\\\\newcommand{\\\\WPchartfile}{$$(echo $(WIKIPEDIA_CHARTS_FILENAME) | sed -e 's/_/\\_/g')}\n" >> $@
	printf "\\\\newcommand{\\\\WHOsrcurl}{$(WHOCOVID19_SRCURL)}\n" >> $@
	printf "\\\\newcommand{\\\\WHOarchiveurl}{$(WHOCOVID19_ARCHIVEURL)}\n" >> $@
	printf "\\\\newcommand{\\\\WPCnineteenCCTFdownloadeddate}{$${WP_downloaded_date}}\n" >> $@
	printf "\\\\newcommand{\\\\WHOdownloadeddate}{$${WHO_downloaded_date}}\n" >> $@
	printf "\\\\newcommand{\\\\JHUurl}{\url{$(JHU_URL)}}\n" >> $@
	printf "\\\\newcommand{\\\\JHUdate}{$$(LANG=C date --date=$(JHU_DATE) +'%-d %B %Y')}\n" >> $@
	printf "\\\\newcommand{\\\\JHUcommit}{$(JHU_COMMIT)}\n" >> $@
	printf "\\\\newcommand{\\\\RSFPressFreedomurl}{\url{$(RSF_PRESSFREEDOM_URL)}}\n" >> $@
	printf "\\\\newcommand{\\\\RSFPressFreedomdate}{$$(LANG=C date --date=$(RSF_PRESSFREEDOM_DATE) +'%-d %B %Y')}\n" >> $@



clean-download:
	rm -fv $(inputs_checked)
