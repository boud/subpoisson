# Poisson statistics analysis
#
# Copyright (C) 2020 Boud Roukema
#
# This Makefile is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This Makefile is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this Makefile.  If not, see <http://www.gnu.org/licenses/>.

py_poisson_progs = $(pysrcdir)/find_range.py \
        $(pysrcdir)/count_jumps_mod.py \
        $(pysrcdir)/get_noise.py \
        $(pysrcdir)/theil_sen_mod.py \
        $(pysrcdir)/plot_countries_mod.py \
        $(pysrcdir)/subpoisson.py \
        $(pysrcdir)/compare_WHO_WP.py \
	$(pysrcdir)/read_counts_JHU.py \
	$(pysrcdir)/detect_weekly_dip.py \
	$(pysrcdir)/plot_alternatives.py \
	$(pysrcdir)/rsf_pfi.py

py_poisson_figs = $(outdir)/probability_vs_N_full.eps \
        $(outdir)/phi_N_full.eps \
        $(outdir)/psi_N_full.eps \
        $(outdir)/phi_N_a.eps \
        $(outdir)/phi_N_b.eps \
        $(outdir)/phi_N_c.eps \
	$(outdir)/weeklydip_full.eps

py_alternative_figs = \
        $(outdir)/psi_N_log_median_neg_binomial.eps \
        $(outdir)/psi_N_bidays_tridays.eps

py_rsf_figs = $(outdir)/pfi_phi_psi_07d.eps $(outdir)/pfi_phi_psi_14d.eps \
        $(outdir)/pfi_phi_psi_28d.eps $(outdir)/pfi_phi_psi_full.eps

py_compare_inputdata_figs = $(outdir)/WHO_vs_WP.eps

py_poisson_tables_dat = $(data-publish-dir)/phi_N_full.dat \
        $(data-publish-dir)/phi_N_28days.dat \
        $(data-publish-dir)/phi_N_14days.dat \
        $(data-publish-dir)/phi_N_07days.dat \
        $(data-publish-dir)/phi_N_full_jhu.dat \
        $(data-publish-dir)/phi_N_28days_jhu.dat \
        $(data-publish-dir)/phi_N_14days_jhu.dat \
        $(data-publish-dir)/phi_N_07days_jhu.dat

py_aic_tables_dat = $(data-publish-dir)/AIC_BIC_full.dat \
        $(data-publish-dir)/AIC_BIC_full_jhu.dat

py_compare_inputdata_tables = $(outdir)/WHO_vs_WP.dat

py_poisson_tables_tex = $(subst $(data-publish-dir)/phi_N,$(texbdir)/low_phi_table,\
        $(subst .dat,.tex,$(py_poisson_tables_dat)))

py_aic_tables_tex = $(subst $(data-publish-dir)/,$(texbdir)/,\
        $(subst .dat,.tex,$(py_aic_tables_dat)))



check-poisson: $(outdir)/done-check-poisson

$(outdir)/done-check-poisson: | $(outdir)
	cp -pv $(py_poisson_progs) $(installdir)/bin/ \
	  && cd $(outdir) \
	  && find_range.py \
	  && get_noise.py \
	  && touch $(outdir)/done-check-poisson

$(outdir):
	mkdir -p $(outdir)

poisson: $(outdir)/done-compare-datasets $(outdir)/done-poisson \
             $(mtexdir)/poisson.tex $(py_poisson_tables_tex) \
             $(outdir)/done-alternative-plots \
             $(outdir)/done-rsf-pfi

$(py_poisson_tables_dat): $(outdir)/done-poisson

$(outdir)/done-poisson: $(inputdatasets) \
                        $(pconfdir)/subpoisson.conf \
                        $(outdir)/done-check-poisson \
                        | $(outdir) $(data-publish-dir)
	printf "Will redo the main subpoisson calculations... \n"
	printf "Press ^C within 15 seconds if you don't want this.\n"
	sleep 15
	cd $(outdir) \
	  && sed -e 's/Bonaire, Sint/Bonaire; Sint/' \
	     -e 's/territory, including/territory; including/' \
	     -e 's/, ,Other/,XX,Other/' \
	     $(indir)/$(WHOCOVID19_FILENAME) \
	     > $(outdir)/$(WHOCOVID19_FILENAME).cleaned \
	  && numthreads=$(strip $(n_cpus)) \
	  && if [ 0$${numthreads} -eq 0 ]; then \
	       numthreads=$$($(installdir)/bin/nproc --all | \
	       LANG=C awk '{n=$$1-2; if(n<1)n=1; if(n>20)n=20; print n}'); fi \
	  && printf "numthreads=$${numthreads}\n" \
	  && for INPUT in JHU WP; do \
	       if [ "x$${INPUT}" = "xWP" ]; then \
	           INFILE=$(indir)/$(WIKIPEDIA_CHARTS_FILENAME); \
	           suffix=""; \
	       else \
	           INFILE=$(indir)/$(JHU_FILENAME); \
	           suffix=_jhu; \
	       fi
	       INPUT_FILE=$${INFILE} \
	       N_THRESHOLD_START=$(N_threshold) \
	       N_THRESHOLD_STOP=$(N_threshold) \
	       MIN_DAYS=$(min_days) \
	       STOP_DAYS=$(stop_days) \
	       N_MEDIAN_MODEL=$(N_median_model) \
	       N_LOWEST_PSI=$(N_lowest_psi) \
	       N_LOWEST_PHI=$(N_lowest_phi) \
	       N_PLOT_LOWEST_PHI=$(N_plot_lowest_phi) \
	       N_PLOT_MEDIAN_PHI=$(N_plot_median_phi) \
	       N_PLOT_N_MIN_LOWEST_PHI=$(N_plot_N_min_lowest_phi) \
	       N_STDERR_CLUSTER=$(N_stderr_cluster) \
	       DELTA_LOG10_CLUSTER=$(delta_log10_cluster) \
	       DELTA_LOG10_REFINE=$(delta_log10_refine) \
	       N_SUBSEQ_LENGTH_A=$(N_subseq_length_a) \
	       N_SUBSEQ_LENGTH_B=$(N_subseq_length_b) \
	       N_SUBSEQ_LENGTH_C=$(N_subseq_length_c) \
	       FIXED_RNG_SEED=$(fixed_rng_seed) \
	       FIXED_RNG_SEED_VALUE=$(fixed_rng_seed_value) \
	       ENABLE_DEV_OVERRIDE=$(enable_dev_override) \
	       N_CPUS=$${numthreads} \
	       subpoisson.py \
	    && if [ "x$${INPUT}" = "xWP" ]; then
	           cp -pv $(py_poisson_figs) $(texbdir)/ \
	           && countries_daily_figs=$$(ls $(outdir)/countries_daily*.eps 2>/dev/null) \
	           && if [ "x$${countries_daily_figs}" != "x" ]; then \
	                 cp -pv $${countries_daily_figs} $(texbdir)/; fi; \
	       fi \
	    && $(call print-general-metadata, $(data-publish-dir)/phi_N_full$${suffix}.dat, new) \
	    && cat $(outdir)/phi_N_full.dat >> \
	         $(data-publish-dir)/phi_N_full$${suffix}.dat \
	    && $(call print-general-metadata, $(data-publish-dir)/phi_N_28days$${suffix}.dat, new) \
	    && cat $(outdir)/phi_N_a.dat >> \
	         $(data-publish-dir)/phi_N_28days$${suffix}.dat \
	    && $(call print-general-metadata, $(data-publish-dir)/phi_N_14days$${suffix}.dat, new) \
	    && cat $(outdir)/phi_N_b.dat >> \
	         $(data-publish-dir)/phi_N_14days$${suffix}.dat \
	    && $(call print-general-metadata, $(data-publish-dir)/phi_N_07days$${suffix}.dat, new) \
	    && cat $(outdir)/phi_N_c.dat >> \
	         $(data-publish-dir)/phi_N_07days$${suffix}.dat \
	    && $(call print-general-metadata, $(data-publish-dir)/AIC_BIC_full$${suffix}.dat, new) \
	    && cat $(outdir)/AIC_BIC_full.dat >> \
	         $(data-publish-dir)/AIC_BIC_full$${suffix}.dat; \
	    done \
	  && touch $(outdir)/done-poisson

$(outdir)/done-compare-datasets: $(inputdatasets) | $(outdir) $(data-publish-dir)
	cd $(outdir) \
	  && N_THRESHOLD_START=$(N_threshold) \
	     INPUT_FILE_0=$(outdir)/$(WHOCOVID19_FILENAME).cleaned \
	     INPUT_FILE_1=$(indir)/$(WIKIPEDIA_CHARTS_FILENAME) \
	     compare_WHO_WP.py \
	  && cp -pv $(py_compare_inputdata_figs) $(texbdir)/ \
	  && $(call print-general-metadata, $(data-publish-dir)/WHO_vs_WP_jumps.dat, new) \
	  && cat $(py_compare_inputdata_tables) >> \
	       $(data-publish-dir)/WHO_vs_WP_jumps.dat \
	  && touch $(outdir)/done-compare-datasets



$(mtexdir)/poisson.tex: $(outdir)/done-poisson $(outdir)/done-compare-datasets \
                           $(py_poisson_tables_tex) $(py_aic_tables_tex) \
                           $(outdir)/done-alternative-plots \
                           $(outdir)/done-rsf-pfi
        # Produce a file with LaTeX macros for the analysis.
	printf "%% Automatically produced file.\n\n"
	printf "%% Automatically produced file.\n\n" > $@
	for paramname in N_threshold min_days stop_days \
	      N_median_model N_lowest_phi N_lowest_psi N_stderr_cluster \
	      N_plot_lowest_phi N_plot_median_phi N_plot_N_min_lowest_phi \
	      N_subseq_length_a N_subseq_length_b N_subseq_length_c; do
	   $(call create_tex_macro, $(pconfdir)/subpoisson.conf, $${paramname}, int, poiss); done
	for paramname in N_countries_all N_neg N_counts_all N_countries_OK; do
	   $(call create_tex_macro, $(outdir)/countries_out_full.dat, $${paramname}, int, poiss); done
	for paramname in phi_N_full_slope phi_N_full_sig_slope phi_N_full_zeropoint phi_N_full_sig_zeropoint; do
	   $(call create_tex_macro, $(outdir)/countries_out_full.dat, $${paramname}, float, poiss); done
	for paramname in phi_N_a_slope phi_N_a_sig_slope phi_N_a_zeropoint phi_N_a_sig_zeropoint; do
	   $(call create_tex_macro, $(outdir)/countries_out_a.dat, $${paramname}, float, poiss); done
	for paramname in phi_N_b_slope phi_N_b_sig_slope phi_N_b_zeropoint phi_N_b_sig_zeropoint; do
	   $(call create_tex_macro, $(outdir)/countries_out_b.dat, $${paramname}, float, poiss); done
	for paramname in phi_N_c_slope phi_N_c_sig_slope phi_N_c_zeropoint phi_N_c_sig_zeropoint; do
	   $(call create_tex_macro, $(outdir)/countries_out_c.dat, $${paramname}, float, poiss); done
	for paramname in N_lowest_phi_countries N_lowest_psi_countries \
	      $$(grep "^[^#]*countries_daily_" $(outdir)/countries_out_full.dat | awk '{print $$1}'); do
	   $(call create_tex_macro, $(outdir)/countries_out_full.dat, $${paramname}, strdeacronymise, poiss); done
	for paramname in N_lowest_phi_below_pointone_countries N_lowest_phi_below_half_countries N_lowest_phi_below_three_countries; do
	   $(call create_tex_macro, $(outdir)/countries_out_a.dat, $${paramname}, strdeacronymise, poissTwEightDay); done
	for paramname in \
	      $$(grep "^[^#]*countries_daily_" $(outdir)/countries_out_a.dat | awk '{print $$1}'); do
	   $(call create_tex_macro, $(outdir)/countries_out_a.dat, $${paramname}, str, poiss); done
	for paramname in \
	      $$(grep "^[^#]*countries_daily_" $(outdir)/countries_out_a.dat | awk '{print $$1}'); do
	        c_ISO=$$(grep $${paramname} $(outdir)/countries_out_a.dat |awk -F '=' '{print $$2}' | \
	          sed -e 's/countries_daily_\(..\)a/\1/' |tr -d ' ')
	        c_en=$$(echo $${c_ISO} | $(call ISO-3166-1-deacronymise))
	        paramname_tex=$$(echo $${paramname}|sed -e 's/[^_]*/\u&/g' | tr -d '_')
	        printf "\\\\newcommand{\\\\$${paramname_tex}Key}{$${c_ISO}: $${c_en}}\n" >> $(mtexdir)/poisson.tex
	done
	for paramname in $$(grep "^[^#]*countries_daily_" $(outdir)/countries_out_b.dat | awk '{print $$1}'); do
	   $(call create_tex_macro, $(outdir)/countries_out_b.dat, $${paramname}, str, poiss); done
	for paramname in $$(grep "^[^#]*countries_daily_" $(outdir)/countries_out_c.dat | awk '{print $$1}'); do
	   $(call create_tex_macro, $(outdir)/countries_out_c.dat, $${paramname}, str, poiss); done
	for paramname in \
	      $$(grep "^[^#]*countries_daily_" $(outdir)/countries_out_c.dat | awk '{print $$1}'); do
	        c_ISO=$$(grep $${paramname} $(outdir)/countries_out_c.dat |awk -F '=' '{print $$2}' | \
	          sed -e 's/countries_daily_\(..\)c/\1/' |tr -d ' ')
	        c_en=$$(echo $${c_ISO} | $(call ISO-3166-1-deacronymise))
	        paramname_tex=$$(echo $${paramname}|sed -e 's/[^_]*/\u&/g' | tr -d '_')
	        printf "\\\\newcommand{\\\\$${paramname_tex}Key}{$${c_ISO}: $${c_en}}\n" >> $(mtexdir)/poisson.tex
	done

	for paramname in N_WHO_countries N_WPCCTF_countries N_common_countries; do
	     $(call create_tex_macro, $(outdir)/WHO_vs_WP_countries.dat, $${paramname}, int, poiss); done
	for paramname in $$(grep "^[^#]*dip_" $(outdir)/weeklydip_full.dat |grep -v shapirowilk_p | awk '{print $$1}'); do
	     $(call create_tex_macro, $(outdir)/weeklydip_full.dat, $${paramname}, float.3, weekly); done
	for paramname in dip_shapirowilk_p ; do
	     $(call create_tex_macro, $(outdir)/weeklydip_full.dat, $${paramname}, exp, weekly); done
	for country in BR US DZ BY RU IN FI PL GH TR CN; do \
	      for subseq in full a b c; do
	         paramname=PPoiss$${country}Seq$${subseq}
	         prob_value=$$(grep "^$${country}" $(outdir)/phi_N_$${subseq}.dat | awk '{printf "%.3f",$$5}')
	         printf "\\\\newcommand{\\\\$${paramname}}{$${prob_value}}\n" >> $(mtexdir)/poisson.tex
	         paramname=Phi$${country}Seq$${subseq}
	         phi_value=$$(grep "^$${country}" $(outdir)/phi_N_$${subseq}.dat | awk '{printf "%.2f",$$7}')
	         printf "\\\\newcommand{\\\\$${paramname}}{$${phi_value}}\n" >> $(mtexdir)/poisson.tex
	         paramname=Phi$${country}Seq$${subseq}SigLogTen
	         phi_value=$$(grep "^$${country}" $(outdir)/phi_N_$${subseq}.dat | awk '{printf "%.2g",$$8}')
	         printf "\\\\newcommand{\\\\$${paramname}}{$${phi_value}}\n" >> $(mtexdir)/poisson.tex
	         paramname=Phi$${country}Seq$${subseq}ThreeSigFig
	         phi_value=$$(grep "^$${country}" $(outdir)/phi_N_$${subseq}.dat | awk '{printf "%.3g",$$7}')
	         printf "\\\\newcommand{\\\\$${paramname}}{$${phi_value}}\n" >> $(mtexdir)/poisson.tex
	      done
	      for subseq in full; do
	         paramname=PsiPrimary$${country}Seq$${subseq}
	         psi_value=$$(grep "^$${country}" $(outdir)/phi_N_$${subseq}.dat | awk '{printf "%.2f",$$7/sqrt($$2)}')
	         printf "\\\\newcommand{\\\\$${paramname}}{$${psi_value}}\n" >> $(mtexdir)/poisson.tex
	         paramname=PsiLogMedian$${country}Seq$${subseq}
	         psi_value=$$(grep "^$${country}" $(outdir)/phi_N_$${subseq}.dat | awk '{printf "%.2f",$$10/sqrt($$2)}')
	         printf "\\\\newcommand{\\\\$${paramname}}{$${psi_value}}\n" >> $(mtexdir)/poisson.tex
	         paramname=PsiNegBinomial$${country}Seq$${subseq}
	         psi_value=$$(grep "^$${country}" $(outdir)/phi_N_$${subseq}.dat | awk '{printf "%.2f",$$13/sqrt($$2)}')
	         printf "\\\\newcommand{\\\\$${paramname}}{$${psi_value}}\n" >> $(mtexdir)/poisson.tex
	         paramname=PsiPrimaryLog$${country}Seq$${subseq}
	         psi_value=$$(grep "^$${country}" $(outdir)/phi_N_$${subseq}.dat | awk '{printf "%.2f",log($$7/sqrt($$2))/log(10.0)}')
	         printf "\\\\newcommand{\\\\$${paramname}}{$${psi_value}}\n" >> $(mtexdir)/poisson.tex
	         paramname=PsiLogMedianLog$${country}Seq$${subseq}
	         psi_value=$$(grep "^$${country}" $(outdir)/phi_N_$${subseq}.dat | awk '{printf "%.2f",log($$10/sqrt($$2))/log(10.0)}')
	         printf "\\\\newcommand{\\\\$${paramname}}{$${psi_value}}\n" >> $(mtexdir)/poisson.tex
	         paramname=PsiNegBinomialLog$${country}Seq$${subseq}
	         psi_value=$$(grep "^$${country}" $(outdir)/phi_N_$${subseq}.dat | awk '{printf "%.2f",log($$13/sqrt($$2))/log(10.0)}')
	         printf "\\\\newcommand{\\\\$${paramname}}{$${psi_value}}\n" >> $(mtexdir)/poisson.tex
	         paramname=PsiPrimaryStderrLog$${country}Seq$${subseq}
	         psi_value=$$(grep "^$${country}" $(outdir)/phi_N_$${subseq}.dat | awk '{printf "%.2f",$$8}')
	         printf "\\\\newcommand{\\\\$${paramname}}{$${psi_value}}\n" >> $(mtexdir)/poisson.tex
	         paramname=PsiLogMedianStderrLog$${country}Seq$${subseq}
	         psi_value=$$(grep "^$${country}" $(outdir)/phi_N_$${subseq}.dat | awk '{printf "%.2f",$$11}')
	         printf "\\\\newcommand{\\\\$${paramname}}{$${psi_value}}\n" >> $(mtexdir)/poisson.tex
	         paramname=PsiNegBinomialStderrLog$${country}Seq$${subseq}
	         psi_value=$$(grep "^$${country}" $(outdir)/phi_N_$${subseq}.dat | awk '{printf "%.2f",$$14}')
	         printf "\\\\newcommand{\\\\$${paramname}}{$${psi_value}}\n" >> $(mtexdir)/poisson.tex
	      done
	    done
	for subseq in full; do
	       paramname=PPoissLowerOnePercentSeq$${subseq}
	       paramnamep=PPoissLowerOnePercentSeq$${subseq}Percent
	       N_value=$$(grep -v "^#" $(outdir)/phi_N_$${subseq}.dat | awk '{all+=1; if($$5 < $(Poisson_lower_onepercent)){count+=1}} END {print count}')
	       N_percent=$$(grep -v "^#" $(outdir)/phi_N_$${subseq}.dat | awk '{all+=1; if($$5 < $(Poisson_lower_onepercent)){count+=1}} END {printf "%.1f\n", 100*count/all}')
	       printf "\\\\newcommand{\\\\$${paramname}}{$${N_value}}\n" >> $(mtexdir)/poisson.tex
	       printf "\\\\newcommand{\\\\$${paramnamep}}{$${N_percent}}\n" >> $(mtexdir)/poisson.tex
	       paramname=PPoissLowerFivePercentSeq$${subseq}
	       paramnamep=PPoissLowerFivePercentSeq$${subseq}Percent
	       N_value=$$(grep -v "^#" $(outdir)/phi_N_$${subseq}.dat | awk '{all+=1; if($$5 < $(Poisson_lower_fivepercent)){count+=1}} END {print count}')
	       N_percent=$$(grep -v "^#" $(outdir)/phi_N_$${subseq}.dat | awk '{all+=1; if($$5 < $(Poisson_lower_fivepercent)){count+=1}} END {printf "%.1f\n", 100*count/all}')
	       printf "\\\\newcommand{\\\\$${paramname}}{$${N_value}}\n" >> $(mtexdir)/poisson.tex
	       printf "\\\\newcommand{\\\\$${paramnamep}}{$${N_percent}}\n" >> $(mtexdir)/poisson.tex
	       paramname=PKSPhiLowestSeq$${subseq}Country
	       paramnamep=PKSPhiLowestSeq$${subseq}Prob
	       N_value=$$(grep -v "^#" $(outdir)/phi_N_$${subseq}.dat | awk '{if($$7 < $(clustering_threshold))print $$1,$$6}' |LANG=C sort -g -k2,2|head -n1 | \
	             awk '{print $$1}' | $(call ISO-3166-1-deacronymise))
	       N_valuep=$$(grep -v "^#" $(outdir)/phi_N_$${subseq}.dat | awk '{if($$7 < $(clustering_threshold))print $$1,$$6}' |LANG=C sort -g -k2,2|head -n1 |awk '{printf "%.2f", $$2}')
	       printf "\\\\newcommand{\\\\$${paramname}}{$${N_value}}\n" >> $(mtexdir)/poisson.tex
	       printf "\\\\newcommand{\\\\$${paramnamep}}{$${N_valuep}}\n" >> $(mtexdir)/poisson.tex
	       paramname=PKSPhiNBLowestSeq$${subseq}Country
	       paramnamep=PKSPhiNBLowestSeq$${subseq}Prob
	       N_value=$$(grep -v "^#" $(outdir)/phi_N_$${subseq}.dat | awk '{if($$13 < $(clustering_threshold))print $$1,$$12}' |LANG=C sort -g -k2,2|head -n1 | \
	             awk '{print $$1}' | $(call ISO-3166-1-deacronymise))
	       N_valuep=$$(grep -v "^#" $(outdir)/phi_N_$${subseq}.dat | awk '{if($$13 < $(clustering_threshold))print $$1,$$12}' |LANG=C sort -g -k2,2|head -n1 |awk '{printf "%.2f", $$2}')
	       printf "\\\\newcommand{\\\\$${paramname}}{$${N_value}}\n" >> $(mtexdir)/poisson.tex
	       printf "\\\\newcommand{\\\\$${paramnamep}}{$${N_valuep}}\n" >> $(mtexdir)/poisson.tex
	       printf "\\\\newcommand{\\\\ClusteringThreshold}{$(clustering_threshold)}\n" >> $(mtexdir)/poisson.tex
	done

        # Write LaTeX macros for the Kendall tau statistic and significance
	infile=$(outdir)/pfi_correlations_vars.dat
	for param_name in $$(grep -v "^#" $${infile} |awk -F "=" '{print $$1}'); do
	  param_name_tex=$$(echo $${param_name} | sed -e 's/[^_-]*/\u&/g' |tr -d '_'|tr -d '-' | \
	      sed -e 's/28/TwEight/' -e 's/14/Fourteen/' -e 's/07/Seven/')
	  param_value=$$(grep -v ^# $${infile} |grep $${param_name} |awk -F "=" '{print $$2}')
	  printf "\\\\newcommand{\\\\$${param_name_tex}}{$${param_value}}\n" >> $(mtexdir)/poisson.tex
	done
        # median of psi_full, phi28, phi14, phi07
	param_name_tex=PKendallMedian
	param_list="p_Kendall_psi_full|p_Kendall_phi_(28|14|07)d"
	param_value=$$(grep -v ^# $${infile} |egrep "$${param_list}" |awk -F "=" '{print $$2}'| \
	   LANG=C sort -g |head -n 3|tail -n2 | tr '\n' ' '|awk '{printf "%.4f", ($$1+$$2)*0.5}')
	printf "\\\\newcommand{\\\\$${param_name_tex}}{$${param_value}}\n" >> $(mtexdir)/poisson.tex
        # individual PFI values
	for param_name in CN; do
	  param_name_tex=PFITwentyTwenty$${param_name}
	  param_value=$$(grep $${param_name} $(indir)/RSF_press_freedom_index_2020.dat| awk '{print $$2}')
	  printf "\\\\newcommand{\\\\$${param_name_tex}}{$${param_value}}\n" >> $(mtexdir)/poisson.tex
	done
	printf "\nThe LaTeX macro file $(mtexdir)/poisson.tex was created.\n\n"

$(outdir)/done-alternative-plots: $(py_alternative_figs)

$(py_alternative_figs): $(py_poisson_tables_dat) $(outdir)/done-poisson | $(outdir)
	cp -pv $(pysrcdir)/plot_alternatives.py $(installdir)/bin/
	cd $(outdir)
	plot_alternatives.py
	cp -pv $(py_alternative_figs) $(texbdir)/
	touch $(outdir)/done-alternative-plots

$(py_rsf_figs): $(outdir)/done-rsf-pfi

$(outdir)/done-rsf-pfi: $(py_poisson_tables_dat) $(outdir)/done-poisson \
                 | $(outdir)
	cp -pv $(pysrcdir)/rsf_pfi.py $(installdir)/bin/
	cd $(outdir)
	for INPUT in JHU WP; do
	   if [ "x$${INPUT}" = "xWP" ]; then
	     suffix=""
	   else
	     suffix="_jhu"
	   fi
	   SUFFIX=$${suffix} rsf_pfi.py
	   if [ "x$${INPUT}" = "xWP" ]; then
	      cp -pv $(py_rsf_figs) $(texbdir)/
	   fi

           # Write table with values to $(data-publish-dir) directory:
	   infile=pfi_correlations_table$${suffix}.dat
	   outfile=$(data-publish-dir)/$${infile}
	   $(call print-general-metadata, $${outfile}, new)
	   cat $${infile} >> $${outfile}

           # Write table with values as LaTeX macros:
	   outfile=$(texbdir)/pfi_correlations_table$${suffix}.tex
	   printf '\\rule{0ex}{2.6ex}%%\n' > $${outfile}
	   grep -v "^ *#" pfi_correlations_table$${suffix}.dat | \
	     head -n 1| \
	     sed -e 's/^ */$$\\phi_i$$ /' -e 's/  */ \& /g' -e "s/\& *\'/ \\\\\\\\/" >> $(texbdir)/pfi_correlations_table$${suffix}.tex
	   grep -v "^ *#" pfi_correlations_table$${suffix}.dat | \
	     head -n 2| tail -n 1| \
	     sed -e 's/^ */$$\\psi_i$$ /' -e 's/  */ \& /g' -e "s/\& *\'/ \\\\\\\\/" >> $(texbdir)/pfi_correlations_table$${suffix}.tex
	   printf '\\hline\n' >> $(texbdir)/pfi_correlations_table$${suffix}.tex
	done
	touch $(outdir)/done-rsf-pfi


$(py_poisson_tables_tex): $(py_poisson_tables_dat) $(outdir)/done-poisson
	data_file=$(strip $(subst $(texbdir)/low_phi_table,$(data-publish-dir)/phi_N,$(subst .tex,.dat,$@)))
	printf "DEBUG poisson.mk; @ = $@  data_file=$${data_file}\n"
        ## phi
	printf '\\rule{0ex}{2.6ex}%%\n' > $@
	if (echo $@ |grep full); then \
	  grep -v "^ *#" $${data_file} | \
	    LANG=C sort -g -k7,7|head -n$(N_lowest_phi) | \
	    LANG=C awk '{printf "%2s %7d %.2f %.2f %.2f %.3f %.2f %.2f %.2f %.2f\n", $$1,$$2,$$5,$$6,$$7,int(1000*($$7/sqrt($$2)))/1000,$$9,$$10,$$12,$$13}' | \
	    sed -e 's/  */ \& /g' -e "s/\'/ \\\\\\\\/" | \
	    $(call ISO-3166-1-deacronymise) >> $@; \
	else \
	  grep -v "^ *#" $${data_file} | \
	    LANG=C sort -g -k7,7|head -n$(N_lowest_phi) | \
	    LANG=C awk '{printf "%2s %7d %.1f %.2f %.2f %.2f %10s\n", $$1,$$2,$$4,$$5,$$6,$$7,$$9}' | \
	    sed -e 's/  */ \& /g' -e "s/\'/ \\\\\\\\/" | \
	    $(call ISO-3166-1-deacronymise) >> $@; \
	fi
        ## psi
	if (echo $@ |grep full); then \
	  printf '\\hline\\rule{0ex}{2.6ex}%%\n' >> $@
	  grep -v "^ *#" $${data_file} | \
	  LANG=C awk '{printf "%2s %7d %.2f %.2f %.2f %.3f %.2f %.2f %.2f %.2f\n", $$1,$$2,$$5,$$6,$$7,int(1000*($$7/sqrt($$2)))/1000,$$9,$$10,$$12,$$13}' | \
	    LANG=C sort -g -k6,6|head -n$(N_lowest_psi) | \
	    sed -e 's/  */ \& /g' -e "s/\'/ \\\\\\\\/" | \
	    $(call ISO-3166-1-deacronymise) >> $@; \
	else \
	  #printf '\\hline\\rule{0ex}{2.6ex}\n' >> $@
	  # grep -v "^ *#" $${data_file} | \
	  #LANG=C awk '{printf "%2s %7d  %.2f %.2f %.5f %10s\n", $$1,$$2,$$4,$$5,int(100000*($$5/sqrt($$2)))/100000,$$7}' $${data_file} | \
	  #  LANG=C sort -g -k5,5|head -n$(N_lowest_psi) | \
	  #  sed -e 's/  */ \& /g' -e "s/\'/ \\\\\\\\/" >> $@;
	  printf "Unjustified. Do not add to table.\n"
	fi
	printf '\\hline\n' >> $@
	printf "Have written out $@ .\n"

$(py_aic_tables_tex): $(py_aic_tables_dat) $(outdir)/done-poisson
	data_file=$(strip $(subst $(texbdir)/,$(data-publish-dir)/,$(subst .tex,.dat,$@)))
	printf "DEBUG poisson.mk; @ = $@  data_file=$${data_file}\n"
	printf '\\rule{0ex}{2.6ex}%%\n' > $@
	  grep -v "^ *#" $${data_file} | \
	  LANG=C awk '{printf "  %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f\n", $$1,$$2,$$3,$$4,$$5,$$6,$$7,$$8,$$9,$$10}' | \
	    sed -e 's/  */ \& /g' -e "s/\'/ \\\\\\\\/" >> $@; \
	printf '\\hline\n' >> $@
	printf "Have written out $@ .\n"

clean-poisson:
	rm -f $(outdir)/[a-z]* $(mtexdir)/poisson.tex $(texbdir)/low_phi_table.tex $(data-publish-dir)/*



.PHONY: check-poisson poisson clean-poisson
