# Build the final PDF paper/report.
#
# Copyright (C) 2018-2021 Mohammad Akhlaghi <mohammad@akhlaghi.org>
#
# This Makefile is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This Makefile is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this Makefile.  If not, see <http://www.gnu.org/licenses/>.


journal-latex-cls-file = wlpeerj.cls
journal-latex-bst-file = mnras_openaccess_titles_100authors.bst
journal-latex-files = $(journal-latex-cls-file) $(journal-latex-bst-file)


# LaTeX macros for paper
# ----------------------
#
# To report the input settings and results, the final report's PDF (final
# target of this project) uses macros generated from various steps of the
# project. All these macros are defined through `$(mtexdir)/project.tex'.
#
# `$(mtexdir)/project.tex' is actually just a combination of separate files
# that keep the LaTeX macros related to each workhorse Makefile (in
# `reproduce/src/make/*.mk'). Those individual macros are pre-requisites to
# `$(mtexdir)/verify.tex' which will check them before starting to build
# the paper. The only workhorse Makefile that doesn't need to produce LaTeX
# macros is this Makefile (`reproduce/src/make/paper.mk').
#
# This file is thus the interface between the analysis/processing steps and
# the final PDF: when we get to this point, all the processing has been
# completed.
#
# Note that if you don't want the final PDF and just want the processing
# and file outputs, you can remove the value of `pdf-build-final' in
# `reproduce/analysis/config/pdf-build.conf'.
$(mtexdir)/project.tex: $(mtexdir)/verify.tex \
               $(inputdatasets)

        # If no PDF is requested, or if LaTeX isn't available, don't
        # continue to building the final PDF. Otherwise, merge all the TeX
        # macros into one for building the PDF.
	@if [ -f .local/bin/pdflatex ] && [ x"$(pdf-build-final)" = xyes ]; then

          # Put a LaTeX input command for all the necessary macro files.
          # 'hardware-parameters.tex' is created in 'configure.sh'.
	  projecttex=$(mtexdir)/project.tex
	  rm -f $$projecttex
	  for t in $(subst paper,,$(makesrc)) hardware-parameters; do
	    echo "\input{tex/build/macros/$$t.tex}" >> $$projecttex
	  done

	  # Highlight changes after the first review: disabled.
	  echo "\togglefalse{postreferee}" >> $$projecttex
          # Possibly highlight the '\new' parts of the text after the second review:
	  if [ x"$(highlightnew)" = x1 ]; then
	    echo "\newcommand{\highlightnew}{}" >> $$projecttex # default maneage style
	    echo "\toggletrue{postrefereeB}" >> $$projecttex # boud's style
	  else
	    echo "\togglefalse{postrefereeB}" >> $$projecttex
	  fi


          # Possibly show the text within '\tonote'.
	  if [ x"$(highlightnotes)" = x1 ]; then
	    echo "\newcommand{\highlightnotes}{}" >> $$projecttex
	  fi

        # The paper shouldn't be built.
	else
	  echo
	  echo "-----"
	  echo "The processing has COMPLETED SUCCESSFULLY! But the final "
	  echo "LaTeX-built PDF paper will not be built."
	  echo
	  if [ x$(more-on-building-pdf) = x1 ]; then
	    echo "To build the PDF, make sure that the 'pdf-build-final' "
	    echo "variable has a value of 'yes' (it is defined in this file)"
	    echo "    reproduce/analysis/config/pdf-build.conf"
	    echo
	    echo "If you still see this message, there was a problem with "
	    echo "building LaTeX within the project. You can re-try building"
	    echo "it when you have internet access with the two commands below:"
	    echo "    $ rm .local/version-info/tex/texlive*"
	    echo "    $./project configure -e"
	  else
	    echo "For more, run './project make more-on-building-pdf=1'"
	  fi
	  echo
	  echo "" > $@
	fi





# The bibliography
# ----------------
#
# We need to run the `biber' program on the output of LaTeX to generate the
# necessary bibliography before making the final paper. So we'll first have
# one run of LaTeX (similar to the `paper.pdf' recipe), then `biber'.
#
# NOTE: `$(mtexdir)/project.tex' is an order-only-prerequisite for
# `paper.bbl'. This is because we need to run LaTeX in both the `paper.bbl'
# recipe and the `paper.pdf' recipe. But if `tex/src/references.tex' hasn't
# been modified, we don't want to re-build the bibliography, only the final
# PDF.
$(texbdir)/paper.bbl $(texbdir)/paper-full.tex: \
	              tex/src/references.tex $(mtexdir)/dependencies-bib.tex \
                      paper.tex \
                      | $(mtexdir)/project.tex
        # If `$(mtexdir)/project.tex' is empty, don't build PDF.
	@macros=$$(cat $(mtexdir)/project.tex)
	if [ x"$$macros" != x ]; then

	  # Copy journal class/bst files into place. (See also download.mk)
	  for file in $(journal-latex-files); do \
	     cp -pv $(texsrcdir)/$${file} $(texbdir)/ ; \
	  done


          # We'll run LaTeX first to generate the `.bcf' file (necessary
          # for `biber') and then run `biber' to generate the `.bbl' file.
	  p=$$(pwd)
	  export TEXINPUTS=$$p:
	  export BIBINPUTS=$$p:
	  #cp -v tex/src/mnras_openaccess.bst $(texbdir)/
	  ## Do some tidying to handle the MNRAS .bst file:
	  cat tex/src/references.tex $(mtexdir)/dependencies-bib.tex \
	    > ${texbdir}/references_tmp.bib
	  cd $(texbdir)
	  latex $$p/paper.tex
	  bibtex paper
	  latex $$p/paper.tex

          # Prepare the latex full source:
	  printf "\nWill create paper-full ...\n"
	  sed -e 's/\({http[^ ]*\)\(%\)/\1\\%/g' paper.bbl > paper.bbl.bak \
          && latexpand --expand-bbl paper.bbl.bak $$p/paper.tex | \
            sed -e 's/\\bibliographystyle[^ }]*}//' > paper-full.tex
	  printf "\n... have created paper-full.tex .\n"

          # Hack for highlighting new references
	  if [ x"$(highlightnew)" = x1 ]; then
	    for key in $(new-reference-keys); do
	      mv -v paper-full.tex paper-full.tex.bak
	      cat paper-full.tex.bak | \
	        sed -e 's/\(\\bibitem\)/NEWBIBITEM\1/' \
	            -e 's/@/THE_AT_SYMBOL/g' \
	            -e "s/\'/@/" | \
	        tr -d '\n' | \
	        sed -e 's/;/THE_SEMICOLON/g' | \
	        sed -e 's/@@/;/g' | \
	        sed -e "s/\($${key}}@\)\([^;]*\)\(;NEWBIBITEM\)/\1\\\\postrefereeBchangesplain{}{\2}\3/" | \
	        sed -e 's/;/@@/g' -e 's/THE_SEMICOLON/;/g' -e 's/@/\n/g' -e 's/THE_AT_SYMBOL/@/g' -e 's/NEWBIBITEM//g' \
	        > paper-full.tex
	    done
	  fi
	fi




# The final paper
# ---------------
#
# Run LaTeX in the `$(texbdir)' directory so all the intermediate and
# auxiliary files stay there and keep the top directory clean. To be able
# to run everything cleanly from there, it is necessary to add the current
# directory (top project directory) to the `TEXINPUTS' environment
# variable.
paper.pdf: $(mtexdir)/project.tex paper.tex $(texbdir)/paper.bbl

        # If `$(mtexdir)/project.tex' is empty, don't build the PDF.
	@macros=$$(cat $(mtexdir)/project.tex)
	if [ x"$$macros" != x ]; then

          # Go into the top TeX build directory and make the paper.
	  p=$$(pwd)
	  export TEXINPUTS=$$p:
	  cd $(texbdir)
	  printf "Check typesetting program availability:\n" ;
	  which latex; which dvips; which ps2pdf;
	  pwd
	  if [ x"$(highlightnew)" = x1 ]; then
	    latex paper-full.tex
	    latex paper-full.tex
	    latex paper-full.tex
	    mv -v paper-full.dvi paper.dvi
	  else
	    latex $$p/paper.tex
	    latex $$p/paper.tex
	    latex $$p/paper.tex
	  fi
	  dvips paper -o paper-tmp.eps
	  ps2pdf paper-tmp.eps $@
          # Come back to the top project directory and copy the built PDF
          # file here.
	  cd "$$p"
	  cp -pv $(texbdir)/$@ $(final-paper)

	fi

dist-pdf: paper.pdf
	cp -pv $(final-paper) $(project-package-name).pdf


clean-full: clean-download clean-poisson clean-paper

clean-paper:
	rm -f $(texbdir)/*.aux $(texbdir)/*.bbl $(texbdir)/*.blg
        # Remove LaTeX macros that are automatically extracted from
        # galaxy formation pipeline configuration files. We first
        # add the `extglob` option to bash, so that `!` can be used
        # to exclude the `dependencies*` files, which are generated
        # during the configure step when installing software. Normally,
        # `!` has a different special meaning in bash.
	shopt -s extglob
	rm -f $(texdir)/macros/!(dependencies.tex|dependencies-bib.tex|hardware-parameters.tex)

.PHONY: clean-paper clean-full
