#!/bin/bash

# download-RSF-press-freedom-index - download RSF press freedom index data

# (C) Boud Roukema 2020-2021 GPL-3+ or CC-BY-SA, at your choosing

# GPL-3+
# This script is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This script is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.

# CC-BY-SA
# https://creativecommons.org/licenses/by-sa/4.0/
# https://creativecommons.org/2015/10/08/cc-by-sa-4-0-now-one-way-compatible-with-gplv3/

# Download Reporters Without Borders (RSF in French) categorisation of press freedom per "country"
# Input: year
# Store:
# * the country 2-letter ID
# * the rating (0 = best, 100 = worst;
#      see https://rsf.org/en/detailed-methodology (archive: https://archive.ph/zaFgE)


RSF_PRESSFREEDOM_URL=https://rsf.org/en/ranking
RSF_PRESSFREEDOM_URL2021_CSV=https://rsf.org/sites/default/files/index_2021_pour_import_-_index_2020_-_pour_import_1_1_-_index_2020_-_pour_import_1_1.csv
# 2021-05-04 - both URLs give the exact same set of ratings (unsurprisingly).


if ([ $# -gt 2 ] || [ "x$1" = "x-h" ] || [ "x$1" = "x--help" ] || [ "x$1" = "x--usage" ] || [ $# -eq 0 ] ); then
    printf "Usage: ./download-RSF-press-freedom YEAR ENABLE_DOWNLOAD\n"
    printf "  YEAR   Initially only tested for 2020. Updates may be needed for earlier or later years.\n"
    printf "  ENABLE_DOWNLOAD     Set to YES for a fresh download of data.\n"
    printf "Default values for both are NO.\n"
    exit 0
fi

YEAR=$1

## Set this to YES if you really want to download. The alternative is to parse
## existing files that you have already downloaded.
# argument 2 on the command line (possibly empty)
enable_download=$2
if [ "x${enable_download}" = "x" ]; then
    enable_download=NO
fi



COUNTRIES_WP=(Algeria Angola Burkina_Faso Burundi Cameroon Egypt Eswatini Ethiopia Ghana Ivory_Coast Kenya Malawi Mali Mauritania Mauritius Morocco Nigeria Senegal South_Africa Tanzania Togo Tunisia Argentina Bolivia Brazil Canada Chile Colombia Costa_Rica Cuba Dominican_Republic Ecuador El_Salvador Guatemala Haiti Honduras Mexico Nicaragua Panama Paraguay Peru United_States Venezuela Afghanistan Armenia Azerbaijan Bahrain Bangladesh Bhutan Brunei Cambodia Mainland_China Georgia Hong_Kong India Indonesia Iran Iraq Israel Japan Jordan Kazakhstan South_Korea Kuwait Kyrgyzstan Laos Lebanon Malaysia Myanmar Nepal Oman Pakistan Palestine Philippines Qatar Saudi_Arabia Singapore Sri_Lanka Syria Taiwan Thailand United_Arab_Emirates Vietnam Yemen Albania Austria Belarus Belgium Bosnia_and_Herzegovina Bulgaria Croatia Cyprus Czech_Republic Denmark Estonia Finland France Germany Greece Hungary Iceland Republic_of_Ireland Italy Kosovo Latvia Lithuania Luxembourg Malta Moldova Monaco Montenegro Netherlands North_Macedonia Norway Poland Portugal Romania Russia San_Marino Serbia Slovakia Slovenia Spain Sweden Switzerland Turkey Ukraine United_Kingdom Vatican_City Australia Fiji French_Polynesia New_Caledonia New_Zealand Papua_New_Guinea)

COUNTRIES_WP_ISO=(DZ AO BF BI CM EG SZ ET GH CI KE MW ML MR MU MA NG SN ZA TZ TG TN AR BO BR CA CL CO CR CU DO EC SV GT HT HN MX NI PA PY PE US VE AF AM AZ BH BD BT BN KH CN GE HK IN ID IR IQ IL JP JO KZ KR KW KG LA LB MY MM NP OM PK PS PH QA SA SG LK SY TW TH AE VN YE AL AT BY BE BA BG HR CY CZ DK EE FI FR DE GR HU IS IE IT XK LV LT LU MT MD MC ME NE MK NO PL PT RO RU SM RS SK SI ES SE CH TR UA GB VA AU FJ PF NC NZ PG)

COUNTRIES_RSF=(Afghanistan Albania Algeria Andorra Angola Argentina Armenia Australia Austria Azerbaijan Bahrain Bangladesh Belarus Belgium Belize Benin Bhutan Bolivia Bosnia_Herzegovina Botswana Brazil Brunei Bulgaria Burkina_Faso Burundi Cabo_Verde Cambodia Cameroon Canada Central_African_Republic Chad Chile China Colombia Comoros Congo_Brazzaville Costa_Rica Croatia Cuba Cyprus Czech_Republic Cote_d_Ivoire Democratic_Republic_of_Congo Denmark Djibouti Dominican_Republic Ecuador Egypt El_Salvador Equatorial_Guinea Eritrea Estonia Eswatini Ethiopia Fiji Finland France Gabon Gambia Georgia Germany Ghana Greece Guatemala Guinea Guinea_Bissau Guyana Haiti Honduras Hong_Kong Hungary Iceland India Indonesia Iran Iraq Ireland Israel Italy Jamaica Japan Jordan Kazakhstan Kenya Kosovo Kuwait Kyrgyzstan Laos Latvia Lebanon Lesotho Liberia Libya Liechtenstein Lithuania Luxembourg Madagascar Malawi Malaysia Maldives Mali Malta Mauritania Mauritius Mexico Moldova Mongolia Montenegro Morocco___Western_Sahara Mozambique Myanmar Namibia Nepal Netherlands New_Zealand Nicaragua Niger Nigeria North_Korea North_Macedonia Northern_Cyprus Norway OECS Oman Pakistan Palestine Panama Papua_New_Guinea Paraguay Peru Philippines Poland Portugal Qatar Romania Russia Rwanda Samoa Saudi_Arabia Senegal Serbia Seychelles Sierra_Leone Singapore Slovakia Slovenia Somalia South_Africa South_Korea South_Sudan Spain Sri_Lanka Sudan Suriname Sweden Switzerland Syria Taiwan Tajikistan Tanzania Thailand Timor_Leste Togo Tonga Trinidad_and_Tobago Tunisia Turkey Turkmenistan Uganda Ukraine United_Arab_Emirates United_Kingdom United_States Uruguay Uzbekistan Venezuela Vietnam Yemen Zambia Zimbabwe)

# User-assigned codes; see https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2
# XK = Kosovo (used by some international organisations for Kosovo)
# XC = Northern_Cyprus (arbitrary choice)
# XO = OECS = Organization of East Caribbean States (arbitrary choice)

COUNTRIES_RSF_ISO=(AF AL DZ AD AO AR AM AU AT AZ BH BD BY BE BZ BJ BT BO BA BW BR BN BG BF BI CV KH CM CA CF TD CL CN CO KM CG CR HR CU CY CZ CI CD DK DJ DO EC EG SV GQ ER EE SZ ET FJ FI FR GA GM GE DE GH GR GT GN GW GY HT HN HK HU IS IN ID IR IQ IE IL IT JM JP JO KZ KE XK JW KG LA LV LB LS LR LY LI LT LU MG MW MY MV ML MT MR MU MX MD MN ME MA MZ MM NA NP NL NZ NI NE NG KP MK XC NO XO OM PK PS PA PG PY PE PH PL PT QA RO RU RW WS SA SN RS SC SL SG SK SI SO ZA KR SS ES LK SD SR SE CH SY TW TJ TZ TH TL TG TO TT TN TR TM UG UA AE GB US UY UZ VE VN YE ZM ZW)
    

N="${#COUNTRIES_RSF[@]}"

printf "Country list:\n"
for ((i=0; i<${N}; i++)); do
    printf "${COUNTRIES_RSF_ISO[$i]} = ${COUNTRIES_RSF[$i]}\n"
done

# XK Kosovo https://ec.europa.eu/consularprotection/countries/kosovo-0_en


## How to update the country list:

## Download the template listing the charts:
# wget -O WP_Template_COVID_19_pandemic.src 'https://en.wikipedia.org/w/index.php?title=Template:COVID-19_pandemic&action=edit'
## Extract the country entries (ignore continents; ignore sub-regions of countries):
# grep "^\*\* .*chart" WP_Template_COVID_19_pandemic.src |grep -o "/.* medical"|sed -e 's/ medical//' -e 's,/,,'|tr ' ' '_' > available_countries
## Use the strings in 'available_countries' to update COUNTRIES_WP above; add the 2-letter acronyms by hand


#URL_PL="https://en.wikipedia.org/w/index.php?title=Template:COVID-19_pandemic_data/Poland_medical_cases_chart&action=edit"

## All ouput files will be created in the current directory or subdirectories.
RSF_PRESSFREEDOM_DIR=rsf_pressfreedom_data
mkdir -p ${RSF_PRESSFREEDOM_DIR} # make the directory if needed
TMP_RSF=${RSF_PRESSFREEDOM_DIR}/tmp_rsf.dat
TMP_TMP=${RSF_PRESSFREEDOM_DIR}/tmp_tmp.dat
TMP_OUT=RSF_press_freedom_index_${YEAR}.dat
rm -fv ${TMP_OUT}

function sleep_a_bit {
   ## We do not want to overload the RSF server(s).
    sleep $(shuf -i 5-10|tail -n1) # wait 5 to 10 seconds
}

function do_download {
    TMP_FILE=$1
    URL=$2
    wget -O ${TMP_FILE} "${URL}"
    sleep_a_bit
}

function extract_data {
    TMP_XX=$1
    TMP_OUT=$2

    ## Check if the data file has already been downloaded.
    if !([ -e ${TMP_XX} ]); then
        printf "Error: Could not find file ${TMP_XX}.\n"
        printf "You may need to do a download first. Try the option '--help'.\n"
        exit 1
    fi

    grep -o "panel-name.*panel-score.*/span" ${TMP_XX} | \
        sed -e 's;panel-name..;;' -e 's;</span.*panel-score..; ;' -e 's;</span;;' | \
        sed -e 's; \([^0-9]\);_\1;g' -e 's;ô;o;g' -e 's;ï;i;g' -e 's;’;_;g' -e 's;[/-];_;g' > ${TMP_TMP}

    N="${#COUNTRIES_RSF_ISO[@]}" # count the number of countries by ISO 3166-1 code

    for ((i=0; i<${N}; i++)); do
        # See if the country is present in the downloaded + cleaned temporary file, and store the grade.
        grade=$(grep "^${COUNTRIES_RSF[${i}]} " ${TMP_TMP} | awk '{print $2}')
        # If the grade is non-empty, then print the country ISO 3166-1 code and the grade
        if [ "x${grade}" != "x" ]; then
            printf "${COUNTRIES_RSF_ISO[${i}]} ${grade}\n" >> ${TMP_OUT}
        fi
    done
}


#N="${#COUNTRIES_ISO[@]}"

URL="${RSF_PRESSFREEDOM_URL}/${YEAR}"
TMP_XX=${TMP_RSF}

echo TMP_XX=${TMP_XX}
echo URL="${URL}"

if [ "x${enable_download}" = "xYES" ]; then
    do_download ${TMP_XX} "${URL}"
fi

extract_data ${TMP_XX} ${TMP_OUT}
