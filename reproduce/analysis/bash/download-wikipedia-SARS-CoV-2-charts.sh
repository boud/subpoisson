#!/bin/bash

# download-wikipedia-SARS-CoV-2-charts - download national SARS-CoV-2 pandemic data

# (C) Boud Roukema 2020-2022 GPL-3+ or CC-BY-SA, at your choosing

# GPL-3+
# This script is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This script is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.

# CC-BY-SA
# https://creativecommons.org/licenses/by-sa/4.0/
# https://creativecommons.org/2015/10/08/cc-by-sa-4-0-now-one-way-compatible-with-gplv3/

# Download Wikipedia 'medical_cases_chart' for each country for which a
# valid article exists.  Store:
# * the ISO format date %Y-%m-%d = yyyy-mm-dd
# * the country 2-letter ID
# * the cumulative infection count
# * the cumulative death count
# * the permanent URL of the data file as a comment to the first line for a country


if ([ $# -gt 2 ] || [ "x$1" = "x-h" ] || [ "x$1" = "x--help" ] || [ "x$1" = "x--usage" ]); then
    printf "Usage: ./download-wikipedia-SARS-CoV-2-charts.sh FULL_COUNTRY_LIST ENABLE_DOWNLOAD\n"
    printf "  FULL_COUNTRY_LIST   Set to YES to extract data for all countries in the list.\n"
    printf "  ENABLE_DOWNLOAD     Set to YES for a fresh download of data.\n"
    printf "Default values for both are NO.\n"
    exit 0
fi

## Set this to YES to download all countries. This is slow to avoid overpressuring
## Wikimedia servers.
# argument 1 on the command line (possibly empty)
full_country_list=$1
if [ "x${full_country_list}" = "x" ]; then
    full_country_list=NO
fi


## Set this to YES if you really want to download. The alternative is to parse
## existing files that you have already downloaded.
# argument 2 on the command line (possibly empty)
enable_download=$2
if [ "x${enable_download}" = "x" ]; then
    enable_download=NO
fi


if [ "x${full_country_list}" = "xYES" ]; then
    COUNTRIES_WP=(Algeria Angola Burkina_Faso Burundi Cameroon Egypt Eswatini Ethiopia Ghana Ivory_Coast Kenya Malawi Mali Mauritania Mauritius Morocco Nigeria Senegal South_Africa Tanzania Togo Tunisia Argentina Bolivia Brazil Canada Chile Colombia Costa_Rica Cuba Dominican_Republic Ecuador El_Salvador Guatemala Haiti Honduras Mexico Nicaragua Panama Paraguay Peru United_States Venezuela Afghanistan Armenia Azerbaijan Bahrain Bangladesh Bhutan Brunei Cambodia Mainland_China Georgia Hong_Kong India Indonesia Iran Iraq Israel Japan Jordan Kazakhstan South_Korea Kuwait Kyrgyzstan Laos Lebanon Malaysia Myanmar Nepal Oman Pakistan Palestine Philippines Qatar Saudi_Arabia Singapore Sri_Lanka Syria Taiwan Thailand United_Arab_Emirates Vietnam Yemen Albania Austria Belarus Belgium Bosnia_and_Herzegovina Bulgaria Croatia Cyprus Czech_Republic Denmark Estonia Finland France Germany Greece Hungary Iceland Republic_of_Ireland Italy Kosovo Latvia Lithuania Luxembourg Malta Moldova Monaco Montenegro Netherlands North_Macedonia Norway Poland Portugal Romania Russia San_Marino Serbia Slovakia Slovenia Spain Sweden Switzerland Turkey Ukraine United_Kingdom Vatican_City Australia Fiji French_Polynesia New_Caledonia New_Zealand Papua_New_Guinea Libya Sierra_Leone South_Sudan Sudan Tajikistan Uruguay Uzbekistan)

    COUNTRIES_ISO=(DZ AO BF BI CM EG SZ ET GH CI KE MW ML MR MU MA NG SN ZA TZ TG TN AR BO BR CA CL CO CR CU DO EC SV GT HT HN MX NI PA PY PE US VE AF AM AZ BH BD BT BN KH CN GE HK IN ID IR IQ IL JP JO KZ KR KW KG LA LB MY MM NP OM PK PS PH QA SA SG LK SY TW TH AE VN YE AL AT BY BE BA BG HR CY CZ DK EE FI FR DE GR HU IS IE IT XK LV LT LU MT MD MC ME NE MK NO PL PT RO RU SM RS SK SI ES SE CH TR UA GB VA AU FJ PF NC NZ PG LY SL SS SD TJ UY UZ)

    sleep_delay=10
else
    COUNTRIES_ISO=(PL DE LT)
    COUNTRIES_WP=(Poland Germany Lithuania)
    sleep_delay=3
fi


N="${#COUNTRIES_WP[@]}"

printf "Country list:\n"
for ((i=0; i<${N}; i++)); do
    printf "${COUNTRIES_ISO[$i]} = ${COUNTRIES_WP[$i]}\n"
done

if [ "x${enable_download}" = "xYES" ]; then
    printf "Data for the above ${N} countries will be downloaded"
    printf " after waiting for ${sleep_delay} seconds.\n"
    printf "Double check ISO-3166-1 codes (+ proposed codes).\n\n"
    printf "Use Ctrl-C to cancel.\n"
    sleep ${sleep_delay}
fi


# XK Kosovo https://ec.europa.eu/consularprotection/countries/kosovo-0_en


## How to update the country list:

## Download the template listing the charts:
# wget -O WP_Template_COVID_19_pandemic.src 'https://en.wikipedia.org/w/index.php?title=Template:COVID-19_pandemic&action=edit'
## Extract the country entries (ignore continents; ignore sub-regions of countries):

## 2022-03-08: The 2021-05-06 change did not generalise the string "2020" for extraction. This meant that despite the
# claim in the paper, the data was only analysed for 2020. The string 2020 has now been changed to 202[0-5], which
# should be valid through to 2025-12-31.

## 2021-05-06:
# grep "^\*\* .*chart" WP_Template_COVID_19_pandemic.src |grep -v vaccination|sed -e 's/^.*pandemic in //' -e 's;^.*pandemic data/;;' -e "s/\#.*\'//" -e 's/^the //' -e 's/ (country)//' -e "s/ medical cases.*\'//"|grep -v "State of Palestine" |tr ' ' '_' > available_countries
## * The parsing is more complex than 10 months ago due to choices in the style of presenting the template and links through articles.
## * Palestine is excluded because although the link exists, the daily data are not provided as of 2021-05-06
#
## 2020-07-12:
# grep "^\*\* .*chart" WP_Template_COVID_19_pandemic.src |grep -o "/.* medical"|sed -e 's/ medical//' -e 's,/,,'|tr ' ' '_' > available_countries
#
## Use the strings in 'available_countries' to update COUNTRIES_WP above; add the 2-letter acronyms by hand
## For comparison, try something like, keeping in mind that Russia and Turkey are listed twice as being "in two continents".
# sort available_countries available_countries_old |uniq -c |sort -n -k1,1 > diff



#URL_PL="https://en.wikipedia.org/w/index.php?title=Template:COVID-19_pandemic_data/Poland_medical_cases_chart&action=edit"

## All ouput files will be created in the current directory or subdirectories.
WP_CHART_DIR=wp_chart_data
mkdir -p ${WP_CHART_DIR} # make the directory if needed
TMP_WP=${WP_CHART_DIR}/tmp_wp.dat
TMP_OUT=Wikipedia_SARSCoV2_charts.dat

function sleep_a_bit {
    ## We do not want to overload Wikimedia Foundation servers.
    sleep $(shuf -i 5-10|tail -n1) # wait 5 to 10 seconds
}

function do_download {
    TMP_FILE=$1
    URL=$2
    wget -O ${TMP_FILE} "${URL}"; sleep_a_bit
}

function extract_data_xx {
    XX=$1
    XX_NAME=$2
    TMP_XX=$3
    TMP_OUT=$4

    ## Check if the data file has already been downloaded.
    if !([ -e ${TMP_XX} ]); then
        printf "Error: Could not find file ${TMP_XX}.\n"
        printf "You may need to do a download first. Try the option '--help'.\n"
        exit 1
    fi

    ## Extract the OLDID of the Wikipedia medical cases chart
    ARTICLEID=$(grep -o "wgArticleId.:[0-9]*,"  ${TMP_XX} |grep -o "[0-9]*")
    OLDID=$(grep -o "wgCurRevisionId.:[0-9]*,"  ${TMP_XX} |grep -o "[0-9]*")

    URL="https://en.wikipedia.org/w/index.php?title=Template:COVID-19_pandemic_data/${XX_NAME}_medical_cases_chart&oldid=${OLDID}&action=edit"

    LEN=$(grep "^202[0-5]-[01][0-9]" ${TMP_XX} | wc -l |awk '{print $1}')
    if [ ${LEN} -le 10 ]; then
        printf "Warning: very few data in extract_data_xx;"
        printf " only ${LEN} entries. TMP_XX = ${TMP_XX}.\n"
    fi

    grep "^202[0-5]-[01][0-9]" ${TMP_XX} | \
        tr -d ' ' | \
        tr -d '\t' | \
        sed -e 's/,//g' | \
        awk -F ';' -v xx=${XX} -v url=${URL} -v articleid=${ARTICLEID} \
            -v oldid=${OLDID} \
            '{print $1,xx,0+$4,0+$2,(1==NR)?("# wgArticleId="articleid" wgCurRevisionId="oldid" url="url):"" }' | \
        sed -e 's/ /,/' -e 's/ /,/' -e 's/ /,/' \
            >> ${TMP_OUT}
}


N="${#COUNTRIES_ISO[@]}"
rm -fv ${TMP_OUT}
for ((i=0; i<${N}; i++)); do
#for ((i=0; i<2; i++)); do # debug
    URL="https://en.wikipedia.org/w/index.php?title=Template:COVID-19_pandemic_data/${COUNTRIES_WP[${i}]}_medical_cases_chart&action=edit"
    TMP_XX=${TMP_WP}_${COUNTRIES_ISO[${i}]}

    if [ "x${enable_download}" = "xYES" ]; then
        do_download ${TMP_XX} "${URL}"
    fi
    extract_data_xx ${COUNTRIES_ISO[${i}]} ${COUNTRIES_WP[${i}]} ${TMP_XX} ${TMP_OUT}
done
