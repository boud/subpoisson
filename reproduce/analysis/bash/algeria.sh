#!/bin/bash
# Manual check for differences between https://github.com/Taha-Rouabah/COVID-19 Algerian
# data and the WP C19CCTF and JHU CSSE versions.

## Download Rouabah versions of Algerian data
if ! [ -e Algeria_data_2.csv ] ; then
    wget https://raw.githubusercontent.com/Taha-Rouabah/COVID-19/master/Data/Algeria_data_2.csv
    checksum=$(sha512sum Algeria_data_2.csv | awk '{print $1}')
    expected_checksum=0c2ccf79ce068c703d05f35b2377918e722fc38a640c7db5d2d04364390e909e5e44c54f8340e17c0e66e0c3ed75ccef91602abbedbfa82eca290766b644b2f6
    if [ "${checksum}" != "${expected_checksum}" ] ; then
        printf "Warning: sha512sum of Algeria_data_2.csv is \n"
        printf "${checksum}\n"
        printf "but\n"
        printf "${expected_checksum} was expected.\n"
        printf "Please double check if the file is really the one you wish to analyse.\n"
    fi
fi

## Rouabah data for comparison with WP C19CCTF
tail -n +8 Algeria_data_2.csv |awk -F , '{print $2}' > dz.rouabah.2020.03.02

## Rouabah data for comparison with JHU
tail -n +2 Algeria_data_2.csv |awk -F , '{print $2}' > dz.rouabah.2020.02.25

## WP C19CCTF
grep DZ reproduce/inputs/Wikipedia_SARSCoV2_charts.dat |head -n 79 | awk -F , '{print $3}' > dz.wp

## JHU
grep Algeria .build/inputs/JHU_CSSEGIS_SARSCoV2.dat | tr ',' '\n'|tail -n +39 |head -n 90 > dz.jhu

## compare
printf "List of differences to change from -Rouabah- to +WP_C19CCTF+ :\n"
removals=$(diff -ub dz.rouabah.2020.03.02 dz.wp | grep -v "\-\-\-" | \
                  grep "^\-" |wc -l)
additions=$(diff -ub dz.rouabah.2020.03.02 dz.wp | grep -v "\-\-\-" | \
                  grep "^\+" |wc -l)
printf "${removals} removals; ${additions} additions;\n\n"
diff -ub dz.rouabah.2020.03.02 dz.wp
printf "\n"

printf "List of differences to change from -Rouabah- to +JHU_CSSE+ :\n"
removals=$(diff -ub dz.rouabah.2020.02.25 dz.jhu | grep -v "\-\-\-" | \
                  grep "^\-" |wc -l)
additions=$(diff -ub dz.rouabah.2020.02.25 dz.jhu | grep -v "\-\-\-" | \
                  grep "^\+" |wc -l)
printf "${removals} removals; ${additions} additions;\n\n"

diff -ub dz.rouabah.2020.02.25 dz.jhu
