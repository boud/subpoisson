#!/bin/bash

# download-JHU-CSSEGIS-SARS-CoV-2 - download national SARS-CoV-2 pandemic data

# (C) Boud Roukema 2020 GPL-3+ or CC-BY-SA, at your choosing

# GPL-3+
# This script is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This script is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.

# CC-BY-SA
# https://creativecommons.org/licenses/by-sa/4.0/
# https://creativecommons.org/2015/10/08/cc-by-sa-4-0-now-one-way-compatible-with-gplv3/

URL=https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_confirmed_global.csv

wget -O JHU.raw ${URL}

## Clean up known territorial names that include commas and double quotes:
cat JHU.raw | \
    sed -e 's/"Korea, South"/South Korea/' \
        -e 's/"Bonaire, Sint Eustatius and Saba"/Bonaire Sint Eustatius and Saba/' \
        -e 's/"Saint Helena, Ascension and Tristan da Cunha"/Saint Helena Ascension and Tristan da Cunha/' \
        > ../../../reproduce/inputs/JHU_CSSEGIS_SARSCoV2.dat
